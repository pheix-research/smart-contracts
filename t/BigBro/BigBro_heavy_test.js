var nodepass  = "node0";
var db_size   = 21;
var trtimeout = 600;
var test_iter = 3;
var id_disp   = 100000;
var gasqty    = 3000000;
var debug_log = true;
var from      = {from: personal.listAccounts[0]};
var sendtx    = {from: personal.listAccounts[0], gas: gasqty};
var domains   = [
    "google.com",
    "facebook.com",
    "doubleclick.net",
    "google-analytics.com",
    "akamaihd.net",
    "googlesyndication.com",
    "googleapis.com",
    "googleadservices.com",
    "facebook.net",
    "youtube.com",
    "twitter.com",
    "scorecardresearch.com",
    "microsoft.com",
    "ytimg.com",
    "googleusercontent.com",
    "apple.com",
    "msftncsi.com",
    "2mdn.net",
    "googletagservices.com",
    "adnxs.com",
    "yahoo.com",
    "serving-sys.com",
    "akadns.net",
    "bluekai.com",
    "ggpht.com",
    "rubiconproject.com",
    "verisign.com",
    "addthis.com",
    "crashlytics.com",
    "amazonaws.com"
];
var ip_addrs  = [
    "5.135.164.72",
    "212.112.124.163",
    "147.135.206.233",
    "177.234.19.50",
    "31.145.50.34",
    "185.136.151.20",
    "191.239.243.156",
    "202.93.128.98",
    "77.95.132.41",
    "193.117.138.126",
    "182.52.51.10",
    "89.249.65.140",
    "190.214.12.82",
    "91.215.195.143",
    "178.128.88.35",
    "118.172.211.155",
    "87.244.182.181",
    "84.52.69.94",
    "1.2.169.4",
    "43.245.131.205",
    "173.249.51.179",
    "180.180.218.153",
    "91.143.54.254",
    "158.69.243.155",
    "35.185.201.225",
    "82.99.228.90",
    "185.81.99.205",
    "186.215.133.170",
    "31.130.91.37",
    "178.128.21.47"
];
var browsers  = [
    "Mozilla/5.0 (Amiga; U; AmigaOS 1.3; en; rv:1.8.1.19) Gecko/20081204 SeaMonkey/1.1.14",
    "Mozilla/5.0 (AmigaOS; U; AmigaOS 1.3; en-US; rv:1.8.1.21) Gecko/20090303 SeaMonkey/1.1.15",
    "Mozilla/5.0 (AmigaOS; U; AmigaOS 1.3; en; rv:1.8.1.19) Gecko/20081204 SeaMonkey/1.1.14",
    "Mozilla/5.0 (Android 2.2; Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4",
    "Mozilla/5.0 (BeOS; U; BeOS BeBox; fr; rv:1.9) Gecko/2008052906 BonEcho/2.0",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.1) Gecko/20061220 BonEcho/2.0.0.1",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.10) Gecko/20071128 BonEcho/2.0.0.10",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.17) Gecko/20080831 BonEcho/2.0.0.17",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.6) Gecko/20070731 BonEcho/2.0.0.6",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.7) Gecko/20070917 BonEcho/2.0.0.7",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1b2) Gecko/20060901 Firefox/2.0b2",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20051002 Firefox/1.6a1",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20060702 SeaMonkey/1.5a",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.10pre) Gecko/20080112 SeaMonkey/1.1.7pre",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.14) Gecko/20080429 BonEcho/2.0.0.14",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.17) Gecko/20080831 BonEcho/2.0.0.17",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.18) Gecko/20081114 BonEcho/2.0.0.18",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.21pre) Gecko/20090218 BonEcho/2.0.0.21pre",
    "Mozilla/5.0 (Darwin; FreeBSD 5.6; en-GB; rv:1.8.1.17pre) Gecko/20080716 K-Meleon/1.5.0",
    "Mozilla/5.0 (Darwin; FreeBSD 5.6; en-GB; rv:1.9.1b3pre)Gecko/20081211 K-Meleon/1.5.2",
    "Mozilla/5.0 (Future Star Technologies Corp.; Star-Blade OS; x86_64; U; en-US) iNet Browser 4.7",
    "Mozilla/5.0 (Linux 2.4.18-18.7.x i686; U) Opera 6.03 [en]",
    "Mozilla/5.0 (Linux 2.4.18-ltsp-1 i686; U) Opera 6.1 [en]",
    "Mozilla/5.0 (Linux 2.4.19-16mdk i686; U) Opera 6.11 [en]",
    "Mozilla/5.0 (Linux 2.4.21-0.13mdk i686; U) Opera 7.11 [en]",
    "Mozilla/5.0 (Linux X86; U; Debian SID; it; rv:1.9.0.1) Gecko/2008070208 Debian IceWeasel/3.0.1",
    "Mozilla/5.0 (Linux i686 ; U; en; rv:1.8.1) Gecko/20061208 Firefox/2.0.0 Opera 9.70",
    "Mozilla/5.0 (Linux i686; U; en; rv:1.8.1) Gecko/20061208 Firefox/2.0.0",
    "Mozilla/5.0 (Linux i686; U; en; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 Opera 10.51",
    "Mozilla/5.0 (Linux) Gecko Iceweasel (Debian) Mnenhy"
];
var resolut   = [
    "960x640",
    "800x480",
    "1152x768",
    "1024x800",
    "1024x640",
    "1280x1024",
    "1280x960",
    "1440x1024",
    "1600x1024",
    "1600x1200",
    "1920x1080",
    "1920x1200",
    "2048x1152",
    "2048x1536"
];
var pages     = [
    "/index.php",
    "/license.txt",
    "/readme.html",
    "/wp-activate.php",
    "/wp-admin/about.php",
    "/wp-admin/admin-ajax.php",
    "/wp-admin/admin-footer.php",
    "/wp-admin/admin-functions.php",
    "/wp-admin/admin-header.php",
    "/wp-admin/admin-post.php",
    "/wp-admin/admin.php",
    "/wp-admin/async-upload.php",
    "/wp-admin/comment.php",
    "/wp-admin/credits.php",
    "/wp-admin/custom-background.php",
    "/wp-admin/custom-header.php",
    "/wp-admin/edit-comments.php",
    "/wp-admin/edit-form-advanced.php",
    "/wp-admin/edit-form-comment.php",
    "/wp-admin/edit-link-form.php",
    "/wp-admin/edit-tag-form.php",
    "/wp-admin/edit-tags.php",
    "/wp-admin/edit.php",
    "/wp-admin/export.php",
    "/wp-admin/freedoms.php",
    "/wp-admin/gears-manifest.php"
];
var countries = [
    "ES",
    "ET",
    "FI",
    "FJ",
    "FK",
    "FM",
    "FO",
    "FR",
    "FX",
    "GA",
    "GB",
    "GD",
    "GE",
    "GF",
    "GG",
    "GH",
    "GI",
    "GL",
    "GM",
    "GN",
    "GP",
    "GQ",
    "GR",
    "GS",
    "GT",
    "GU",
    "GW",
    "GY",
    "HK",
    "HN",
    "HR",
    "HT",
    "HU"
];

// unlock account at Parity CLI
personal.unlockAccount(personal.listAccounts[0], nodepass);

while (test_iter > 0) {
    var dropped = drop_table(debug_log);
    if (dropped) {
        var test_create = false;
        var test_insert = false;
        var test_slct_1 = false;
        var test_slct_2 = false;
        var test_slct_3 = false;
        var test_slct_4 = false;
        var test_remove = false;
        var test_set    = false;
        var table = generate_tab((Math.floor(Math.random() * db_size) + 1), debug_log);
        //console.log("table: " + key + "," + table);
        test_create = true; //create_table( debug_log );
        test_insert = insert_table(table, debug_log);
        test_slct_1 = select_table(table, debug_log);

        var selected_table = select_all();
        test_slct_2 = compare_mem_tables(table, selected_table);

        var indexes_to_remove = indexes_list(Math.floor(table.length / 4) || 1, table.length, debug_log);
        var updated_table     = remove_from_tab(table, indexes_to_remove);
        test_remove = remove_table(table, indexes_to_remove, debug_log);
        test_slct_3 = select_table(updated_table, debug_log);

        updated_table  = generate_tab(updated_table.length, debug_log);
        test_set       = set_table(updated_table, debug_log);
        selected_table = select_all();
        test_slct_4    = compare_mem_tables(updated_table, selected_table );
        database = updated_table;

        // summary
        if (
            test_create &&
            test_insert &&
            test_slct_1 &&
            test_remove &&
            test_slct_2 &&
            test_slct_3 &&
            test_slct_4 &&
            test_set
        ) {
            console.log("Test #" + test_iter + " is COMPLETED");
        }
        else {
            console.log("Test #" + test_iter + " is FAILED");
        }
        test_iter--;
    }
    else {
        break;
    }
}

/* Utility test functions */

// wait for transactions by hash-array
function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc      = true;
    var index   = 0;
    var hashes  = tr_hash_arr;
    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length);
    while (hashes.length > 0) {
        var updated_hashes = new Array;
        for(var i = 0; i < hashes.length; i++) {
            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                if (!(eth.getTransactionReceipt(hashes[i]).status === "0x1")) {
                    rc = false;
                    console.log("\tFAILURE: transaction status=" + eth.getTransactionReceipt(hashes[i]).status);
                }
            }
        }
        hashes = updated_hashes;
        if (index == trtimeout) {
            rc = false;
            break;
        }
        else {
            if (logging == 1 && (index > 0 && index % 15 == 0)) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length) + "/" + tr_hash_arr.length);
            }
            index++;
            storage.sleep(1);
        }
    }
    return rc;
}

// generate table
function generate_tab(tab_size, logging) {
    var tab = new Array;
    for(var i = 0; i < tab_size; i++) {
        var id       = i + id_disp;
        var d_index  = Math.floor(Math.random() * (domains.length));
        var ip_index = Math.floor(Math.random() * (ip_addrs.length));
        var ua_index = Math.floor(Math.random() * (browsers.length));
        var r_index  = Math.floor(Math.random() * (resolut.length));
        var p_index  = Math.floor(Math.random() * (pages.length));
        var c_index  = Math.floor(Math.random() * (countries.length));
        tab.push(
            domains[d_index] + '|' +
            ip_addrs[ip_index] + '|' +
            browsers[ua_index] + '|' +
            resolut[r_index] + '|' +
            pages[p_index] + '|' +
            countries[c_index]
        );
    }
    if (logging) {
        console.log("table with " + tab_size + " rows is successfully generated");
    }
    return tab;
}

// get rowid for indexed from table
function rowids_list(indexes, logging) {
    var rowids = new Array;
    for( var i = 0; i < indexes.length; i++) {
        var tuple = storage.getByIndex.call(indexes[i], from);
        var rowid = tuple[0];
        if (rowid > -1) {
            rowids.push(rowid);
        }
    }
    if (logging) {
        console.log("\tremove plan: " + rowids.length + " rows, ids=[" + rowids + "]");
    }
    return rowids;
}

// generate indexes for remove
function indexes_list(list_size, database_size, logging) {
    var indexes = new Array;
    for(var i = 0; i < list_size; i++) {
        var index = -1;
        var flag  = true;
        while (flag) {
            flag  = false;
            index = Math.floor(Math.random() * (database_size - i));
            for(var k = 0; k < indexes.length; k++) {
                if (indexes[k] == index) {
                    flag = true;
                    break;
                }
            }
        }
        if (index > -1) {
            indexes.push(index);
        }
        else {
            if (logging) {
                console.log("Failure white generating index for i=" + i);
            }
        }
    }
    if (logging) {
        console.log("\tremove plan: " + indexes.length + " elements, indexes=[" + indexes + "]");
    }
    return indexes;
}

// compare tables
function compare_mem_tables(tab1, tab2) {
    var rc = true;
    if (tab1.length == tab2.length) {
        for(var i = 0; i < tab1.length; i++) {
            var fnd = false;
            for(var j = 0; j < tab2.length; j++) {
                if (tab2[j] === tab1[i]) {
                    fnd = true;
                    break;
                }
            }
            if (!fnd) {
                rc = false;
                console.log("data in tab1[" + i + "] is not found in tab2");
                break;
            }
        }
    }
    else {
        rc = false;
    }
    return rc;
}

// select all
function select_all() {
    var data = new Array;
    for(var i = 0; i < storage.getCount.call(from); i++) {
        var tuple = storage.getByIndex.call(i, from);
        var rowid = tuple[0];
        if (rowid > 0) {
            var row = tuple[1] + '|' + tuple[2] + '|' + tuple[3] + '|' + tuple[4] + '|' + tuple[5] + '|' + tuple[6];
            data.push(row);
        }
        else {
            console.log("\tFAILURE: table contains invalid id=" + rowid + " at index=" + i);
            data = [];
            break;
        }
    }
    return data;
}

// remove elements from memory table
function remove_from_tab(table, index_array) {
    var updated_db = new Array;
    for(var i = 0; i < table.length; i++) {
        var flag = true;
        for(var j = 0; j < index_array.length; j++) {
            if (index_array[j] > -1 && index_array[j] < table.length && index_array[j] == i) {
                flag = false;
                break;
            }
        }
        if (flag) {
            updated_db.push(table[i]);
        }
    }
    return updated_db;
}

// drop all tables before testings
function drop_table(logging) {
    var rc = true;
    var existed_rows_num = storage.getCount.call(from);
    var hashes = new Array;
    if (existed_rows_num > 0) {
        if (logging) {
            console.log( "PREPARE: drop " + existed_rows_num + " existed rows");
        }
        var existed_ids = new Array;
        for(var i = 0; i < existed_rows_num; i++) {
            var tuple = storage.getByIndex.call(i, from);
            var rowid = tuple[0];
            if (rowid > 0) {
                existed_ids.push(rowid);
            }
        }
        for(var i = 0; i < existed_ids.length; i++) {
            var rowid = existed_ids[i];
            if (rowid > 0) {
                var tuple = storage.getById.call(rowid, from);
                hashes.push( storage.removeById.sendTransaction(rowid, sendtx) );
            }
            else {
                if (logging) {
                    //rc = false;
                    console.log("\tFAILURE: existence row '" + rowid + "' at index=" + i);
                }
            }
        }
        rc = wait_transactions_byhash_array("drop_table for " + existed_rows_num + " rows", hashes, logging);
    }
    return rc;
}

// table create test
function create_table(logging) {
    var hashes = new Array;
    hashes.push(storage.init.sendTransaction(sendtx));
    var res  = wait_transactions_byhash_array("create_table with init_db()", hashes, logging);
    if (storage.getCount.call(from) != 5 || !res) {
        console.log("\tFAILURE: could not create table with init_db()");
        return false;
    }
    return true;
}

// table insert test
function insert_table(table, logging) {
    var hashes     = new Array;
    var initial_id =
        ( eth.getBlock(eth.getTransaction(storage.tx).blockNumber).timestamp -
        1 * id_disp );
    for(var i = 0; i < table.length; i++) {
        var row = table[i].split('|');
        hashes.push(storage.insert.sendTransaction((initial_id < 1 ? (i + 1) : initial_id + i), row[0], row[1], row[2], row[3], row[4], row[5], sendtx));
    }
    var res = wait_transactions_byhash_array("insert_table rows to table", hashes, logging);
    var tlen = storage.getCount.call(from);
    if (!(tlen == (table.length)) || !res) {
        console.log("\tFAILURE: could not insert to table, count_rows=" + tlen);
        return false;
    }
    return true;
}

// table set test
function set_table(table, logging) {
    var rc     = true;
    var hashes = new Array;
    var storage_tab_len = storage.getCount.call(from);
    if (storage_tab_len == table.length) {
        for(var i = 0; i < storage_tab_len; i++ ) {
            var tuple = storage.getByIndex.call(i, from);
            var rowid = tuple[0];
            if (rowid > 0) {
                var row  = table[i].split('|');
                hashes.push(storage.setById.sendTransaction(parseInt(rowid), row[0], row[1], row[2], row[3], row[4], row[5], sendtx));
            }
        }
        var res = wait_transactions_byhash_array("set_table rows", hashes, logging);
        var tlen = storage.getCount.call(from);
        if (!(tlen == (table.length)) || !res) {
            console.log("\tFAILURE: could not insert to table, count_rows=" + tlen);
            rc = false;
        }
    }
    else {
        console.log("\tFAILURE: tables have diff lengths blockchain_tab=" + storage.getCount.call(from) + ", memory_tab=" + table.length);
        rc = false;
    }
    return rc;
}

// table select test
function select_table(table, logging) {
    var tab_select = true;
    for(var i = 0; i < storage.getCount.call(from); i++) {
        var tuple1 = storage.getByIndex.call(i, from);
        var rowid  = tuple1[0];
        if (rowid > 0) {
            var fnd   = false;
            var tuple = storage.getById.call(rowid, from);
            var data  = tuple[0] + '|' + tuple[1] + '|' + tuple[2] + '|' + tuple[3] + '|' + tuple[4] + '|' + tuple[5];
            for(var j = 0; j < table.length; j++) {
                if (data === table[j]) {
                    fnd = true;
                    break;
                }
            }
            if (!fnd) {
                tab_select = false;
                if (logging) {
                    console.log("\tFAILURE: could not find data for id " + tuple1[0]);
                }
                //break;
            }
        }
    }
    return tab_select;
}

// table remove test
function remove_table(table, indexes_to_remove, logging) {
    var rc      = true;
    var hashes  = new Array;
    var rm_rows = indexes_to_remove.length;
    var rowids_to_remove  = rowids_list(indexes_to_remove, logging);
    for(var i = 0; i < rowids_to_remove.length; i++) {
        var rowid = rowids_to_remove[i];
        if (rowid > -1) {
            var hash = storage.removeById.sendTransaction(rowid, sendtx);
            hashes.push(hash);
            if (!hash) {
                if (logging) {
                    console.log("\tFAILURE: remove failure for id=" + rowid);
                }
                rc = false;
            }
        }
        else {
            if (logging) {
                console.log("\tFAILURE: invalid id=" + rowid + " for remove");
            }
            rc = false;
        }
    }
    var res = wait_transactions_byhash_array("remove_table", hashes, logging);
    if (storage.getCount.call(from) == (table.length - rm_rows) && res) {
        var check = true;
        for(var i = 0; i < rowids_to_remove.length; i++) {
            var rowid = rowids_to_remove[i];
            var tuple = storage.getById.call(rowid, from);
            if (
                !(
                    tuple[0] === "" &&
                    tuple[1] === "" &&
                    tuple[2] === "" &&
                    tuple[3] === "" &&
                    tuple[4] === "" &&
                    tuple[5] === ""
                )
            ) {
                check = false;
                console.log("\tFAILURE: remove table fails, found removed id=" + rowid);
            }
        }
        if (check) {
            if ( logging ) {
                console.log("\t    remove from table ok!");
            }
        }
        else {
            rc = false;
        }
    }
    else {
        console.log("\tFAILURE: failure while remove from table, count_rows=" + storage.getCount.call(from));
        rc = false;
    }
    return rc;
}
