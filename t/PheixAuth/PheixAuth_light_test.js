var nodepass  = 'node0';
var trtimeout = 600;
var gasqty    = 3000000;
var from      = {from: personal.listAccounts[0]};
var sendtx    = {from: personal.listAccounts[0], gas: gasqty};
var logging   = true;
var test_res  = false;
var staticval = "1653185297585255431507888012886439";

// wait for transactions by hash-array
function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc = true;
    var index = 0;
    var hashes = tr_hash_arr;
    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length );
    while(hashes.length > 0) {
        var updated_hashes = new Array;
        for(var i = 0; i < hashes.length; i++) {
            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                if (!(eth.getTransactionReceipt(hashes[i]).status === "0x1")) {
                    rc = false;
                    console.log("\tFAILURE: transaction status=" + eth.getTransactionReceipt(hashes[i]).status);
                }
            }
        }
        hashes = updated_hashes;
        if (index == trtimeout) {
            rc = false;
            break;
        }
        else {
            if (logging && (index > 0 && index % 15 == 0)) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length) + "/" + tr_hash_arr.length);
            }
            index++;
            storage.sleep(1);
        }
    }
    return rc;
}

/* account unlock */
personal.unlockAccount(personal.listAccounts[0], nodepass);

/* primary retrieval */
var initial      = web3.sha3(web3.padLeft(web3.toHex(web3.toBigNumber(staticval)), 64), { encoding : 'hex' });
var primary      = storage.getPkey.call(from);
var primary_diff = storage.getDiff.call(from);

if (logging) {
    console.log('Static value: ' + staticval);
    console.log('Initial pkey: ' + initial);
    console.log('Primary pkey: ' + primary);
}

/* update pkey */
var hashes = new Array;

hashes.push(storage.updateState.sendTransaction(0, sendtx));

if (wait_transactions_byhash_array('update pkey', hashes, logging)) {
    if (logging) {
        console.log("\tpkey is updated");
    }
}
else {
    if (logging) {
        console.log("FAILURE: update pkey: " + hashes.join(','));
    }
}

hashes.pop();

/* secondary retrieval */
var secondary      = storage.getPkey.call(from);
var secondary_diff = storage.getDiff.call(from);

if (logging) {
    console.log("Secondary pkey: " + secondary);
}

/* retrieve events */
var curr_filter;

web3.eth.filter({
    fromBlock: 'earliest',
    toBlock: 'latest',
    address: storage.address,
    topics: [ web3.sha3('PheixAuthCode(uint256)'), ["0x" + web3.padLeft('1', 64), "0x" + web3.padLeft('2', 64) ]]}
).get(function(error, result){ if (!error) curr_filter = result; else console.log(error); });

if (logging) {
    console.log("Got " + curr_filter.length + " events with trxs:");

    for(var i=0; i<curr_filter.length; i++) {
        console.log("\t" + curr_filter[i].transactionHash);
    }
}

/* setup regexprs */
var re1 = /0x[0-9A-Fa-f]{64}/g;
var re2 = /0x[0-9A-Fa-f]{64}/g;
var re3 = /0x[0-9A-Fa-f]{64}/g;

if (logging) {
    console.log("primary diff: " + primary_diff + ", secondary_diff: " + secondary_diff);
}

/* Tertiary retrieval */
hashes.push(storage.updateState.sendTransaction(web3.toHex(web3.toBigNumber(staticval)), sendtx));

if (wait_transactions_byhash_array('update pkey to static value', hashes, logging)) {
    if (logging) {
        console.log("\tpkey is updated to static value");
    }
}
else {
    if (logging) {
        console.log("FAILURE: update pkey to static value: " + hashes.join(','));
    }
}

hashes.pop();

var tertiary      = storage.getPkey.call(from);
var tertiary_diff = storage.getDiff.call(from);

if (
    initial === primary &&
    primary_diff < 0 &&
    secondary_diff < 0 &&
    tertiary_diff < 0 &&
    primary !== secondary &&
    primary === tertiary &&
    primary && secondary && tertiary &&
    re1.test(primary) &&
    re2.test(secondary) &&
    re3.test(tertiary) &&
    curr_filter.length >= 2
) {
    wait_transactions_byhash_array('close session', [storage.doExit.sendTransaction(sendtx)], logging)

    var re4       = /0x[0]{64}/g;
    var nullkey   = storage.getPkey.call(from);
    var exit_diff = storage.getDiff.call(from);

    if (exit_diff == 0 && nullkey && (nullkey === '0x') || re4.test(nullkey)) {
        test_res = true;
    }
    else {
        console.log("FAILURE: pkey is not null after self destruct: " + nullkey);
    }
}

if (test_res) {
    console.log("ALL TESTS COMPLETED!");
}
else {
    console.log("TESTS FAILED!");
}
