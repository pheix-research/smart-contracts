#!/bin/bash

CONTRACT=$1
DOTGZ=$2
NODEPASSW=$3
ROOT=$ENVREPOPATH

if [ -z "$ROOT" ]; then
    ROOT=..
fi

PATH=$ROOT/sol
COMPILEPATH=.solcoutput
DEPLOYGAS=8000000
PREFIX=retrieve
SLEEPIMPLEMENTATION="function(sec){var dt=new Date();dt.setTime(dt.getTime()+sec*1000);while(new Date().getTime()<dt.getTime());}"
SLEEPADMIN="admin.sleep"
INITIALPKEYNUM=1653185297585255431507888012886439

if [ -z "$NODEPASSW" ]; then
    NODEPASSW=node0
fi

echo "***HINT: ./compile-contract.sh <contract_name> [--tgz/--notgz] [<node_password>]"

if test -z "${CONTRACT}"
then
    echo "Please specify correct contact name: ./compile-contract.sh <contract_name>"
    exit 1
else
    if [ ! -d ${CONTRACT} ]; then
        echo "No target smart contract folder ${CONTRACT}"
        exit 1
    fi

    if [ -f ${PATH}/${CONTRACT}.sol ]; then
        /usr/local/bin/solc --evm-version paris --optimize --overwrite --abi --bin -o ${COMPILEPATH} ${PATH}/${CONTRACT}.sol

        if [ $? -ne 0 ]; then
            echo "***ERR: ${PATH}/${CONTRACT}.sol compilation failed"
            exit 1
        fi

        echo "var _sleep=${SLEEPIMPLEMENTATION}" > ${CONTRACT}/${CONTRACT}.js

        ABI=`/usr/bin/cat ${COMPILEPATH}/${CONTRACT}.abi`
        BIN=`/usr/bin/cat ${COMPILEPATH}/${CONTRACT}.bin`

        echo "var storageContractAbi = ${ABI};" >> ${CONTRACT}/${CONTRACT}.js
        echo "var storageContract = eth.contract(storageContractAbi);" >> ${CONTRACT}/${CONTRACT}.js
        echo "var storageBinCode = \"0x${BIN}\";" >> ${CONTRACT}/${CONTRACT}.js
        echo "if (typeof personal === 'object') {personal.unlockAccount(eth.accounts[0],'$NODEPASSW');}" >> ${CONTRACT}/${CONTRACT}.js;
        echo "var deployTransationObject = { from: eth.accounts[0], data: storageBinCode, gas: ${DEPLOYGAS} };" >> ${CONTRACT}/${CONTRACT}.js

        if [[ ${ABI} =~ "constructor" ]]; then
            if [ "${CONTRACT}" = "PheixAuth" ]; then
                echo "var storageInstance = storageContract.new(0, 0, 0, web3.toHex(web3.toBigNumber(\"${INITIALPKEYNUM}\")), deployTransationObject);" >> ${CONTRACT}/${CONTRACT}.js
            else
                echo "var storageInstance = storageContract.new(deployTransationObject);" >> ${CONTRACT}/${CONTRACT}.js
            fi
        else
            echo "var storageInstance = storageContract.new(deployTransationObject);" >> ${CONTRACT}/${CONTRACT}.js
        fi

        echo "console.log(storageContractAbi);" >> ${CONTRACT}/${CONTRACT}.js
        echo "console.log(storageBinCode);" >> ${CONTRACT}/${CONTRACT}.js
        echo "storageContract.abi.forEach(function(item){console.log(item.name)});" >> ${CONTRACT}/${CONTRACT}.js
        echo "storageInstance.abi.forEach(function(item){console.log(item.name)});" >> ${CONTRACT}/${CONTRACT}.js
        echo "console.log(storageInstance.transactionHash);" >> ${CONTRACT}/${CONTRACT}.js
        echo "var receipt = undefined; var timeout=60; while(timeout>0 && !receipt){receipt=eth.getTransactionReceipt(storageInstance.transactionHash);console.log(timeout);_sleep(1);timeout--;}" >> ${CONTRACT}/${CONTRACT}.js
        echo "var tx=eth.getTransaction(storageInstance.transactionHash);for (var prop in tx) {console.log(\"tx.\" + prop + \" = \" + tx[prop]);}" >> ${CONTRACT}/${CONTRACT}.js
        echo "if(receipt){for (var prop in receipt) {console.log(\"receipt.\" + prop + \" = \" + receipt[prop]);}}else{console.log('Tx Receipt is NULL!');}" >> ${CONTRACT}/${CONTRACT}.js
        echo "var storageAddress = eth.getTransactionReceipt(storageInstance.transactionHash).contractAddress;" >> ${CONTRACT}/${CONTRACT}.js
        echo "var storage = storageContract.at(storageAddress);" >> ${CONTRACT}/${CONTRACT}.js;
        echo "storage.sleep=_sleep;" >> ${CONTRACT}/${CONTRACT}.js;
        echo "storage.tx=storageInstance.transactionHash;" >> ${CONTRACT}/${CONTRACT}.js;

        echo "var _sleep=${SLEEPIMPLEMENTATION}" > ${CONTRACT}/${PREFIX}_${CONTRACT}.js
        echo "var storageContractAbi = ${ABI};" >> ${CONTRACT}/${PREFIX}_${CONTRACT}.js
        echo "var storageContract = eth.contract(storageContractAbi);" >> ${CONTRACT}/${PREFIX}_${CONTRACT}.js
        echo "var storageBinCode = \"0x${BIN}\";" >> ${CONTRACT}/${PREFIX}_${CONTRACT}.js
        echo "var storage; if (!(typeof _CONTRACT_TX === 'undefined')) {storage=storageContract.at(eth.getTransactionReceipt(_CONTRACT_TX).contractAddress); storage.sleep=_sleep; storage.tx=_CONTRACT_TX} else {console.log('Please specify contract transaction hash in _CONTRACT_TX variable!');}" >> ${CONTRACT}/${PREFIX}_${CONTRACT}.js

        if [ "$DOTGZ" == "--tgz" ]; then
            /bin/tar cf - ${CONTRACT}/${CONTRACT}.js ${CONTRACT}/${PREFIX}_${CONTRACT}.js ${COMPILEPATH}/${CONTRACT}.abi ${COMPILEPATH}/${CONTRACT}.bin | /bin/gzip -9 > ${ROOT}/${CONTRACT}.tar.gz

            if [ $? -ne 0 ]; then
                echo "***ERR: tar generation failed"

                exit 1
            else
                /bin/rm ${COMPILEPATH}/${CONTRACT}.*
            fi
        else
            echo "***WARN: Skip tgz output: no --tgz arg is given to command line"
        fi
    else
        echo "***ERR: contract ${PATH}/${CONTRACT}.sol not found"

        exit 1
    fi
fi

echo "Smart contract ${CONTRACT} is compiled successfully"

exit 0
