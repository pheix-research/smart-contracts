var nodepass  = 'node0';
var db_size   = 20;
var trtimeout = 600;
var gasqty    = 3000000;
var from      = {from: personal.listAccounts[0]};
var sendtx    = {from: personal.listAccounts[0], gas: gasqty};
var logging   = true;
var savedata  = '0x425A68363141592653592C4B15590000169FB140E70100002250003FEFDFA00020020010003000AC4354F4011A064C8D06801864604D30264313460354FD14D943D0A0D034309A145DBA653E28236D3DD2AF14C5F096ED6250562CEA3C24F935540D399309186AEC546E49D7B68A8A12706E1CF691DA03DE81B6C127BE190B9117E25EF3BDC20A8058747C8156359A2916E7229CB4366B094DE2E8120C18DA10052564254184DED3D695C690E0E55B16B0645916AC78B857D4151807902302EDEC60242AF1D4673639D7821E563EF91F8BB9229C284816258AAC80';


// wait for transactions by hash-array
function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc = true;
    var index = 0;
    var hashes = tr_hash_arr;
    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length );
    while(hashes.length > 0) {
        var updated_hashes = new Array;
        for(var i = 0; i < hashes.length; i++) {
            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                if (!(eth.getTransactionReceipt(hashes[i]).status === "0x1")) {
                    rc = false;
                    console.log("\tFAILURE: transaction status=" + eth.getTransactionReceipt(hashes[i]).status);
                }
            }
        }
        hashes = updated_hashes;
        if (index == trtimeout) {
            rc = false;
            break;
        }
        else {
            if (logging && (index > 0 && index % 15 == 0)) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length) + "/" + tr_hash_arr.length);
            }
            index++;
            storage.sleep(1);
        }
    }
    return rc;
}

/* clear data storage */
function cleanStorage(log) {
    var ret    = false;
    var hashes = new Array;
    var len    = storage.getLength.call(from);
    if (len > 0) {
        for(var i = 0; i < len; i++) {
            hashes.push(storage.popRecord.sendTransaction(sendtx));
        }
        if (wait_transactions_byhash_array('clean storage', hashes, log)) {
            if (log) {
                console.log('Storage cleared, removed ' + len + ' records');
            }
            ret = true;
        }
        else {
            if (log) {
                console.log('FAILURE: storage clear fails: ' + hashes.join(','));
            }
        }
    }
    else {
        if (log) {
            console.log("Clean storage, skip clearing");
        }
        ret = true;
    }
    return ret;
}

/* fill data storage */
function fillStorage(log) {
    var ret    = false;
    var hashes = new Array;
    for(var i = 0; i < db_size; i++) {
        hashes.push(storage.pushRecord.sendTransaction(savedata, sendtx));
    }
    if (wait_transactions_byhash_array('fill storage', hashes, log)) {
        if (log) {
            console.log('Storage filled with ' + db_size + ' records');
        }
        ret = true;
    }
    else {
        if (log) {
            console.log('FAILURE: storage fill fails: ' + hashes.join(','));
        }
    }
    return ret;
}

/* check records */
function checkStorage(log) {
    var ret = true;
    var len = storage.getLength.call(from);
    if (len > 0) {
        for(var i = 0; i < len; i++) {
            var storedata = storage.getRecord.call(i, from);
            if (storedata !== savedata.toLowerCase()) {
                if (log) {
                    console.log('FAILURE: storage check fails on record index=' + i + ', data=' + storedata);
                }
                ret = false;
            }
        }
    }
    else {
        ret = false;
    }
    return ret;
}

/* account unlock */
personal.unlockAccount(personal.listAccounts[0], nodepass);

/* initial storage clean */
var step01 = cleanStorage(logging);

/* write sample database */
var step02 = fillStorage(logging);

/* check sample database */
var step03 = checkStorage(logging);

/* revert database changes */
var step04 = cleanStorage(logging);

// summary
if (step01 && step02 && step03 && step04) {
    console.log("ALL TESTS COMPLETED!");
}
else {
    console.log("TESTS FAILED!");
}
