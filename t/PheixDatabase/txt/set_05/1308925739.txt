<h1>
	<span style="color:#800000;"><strong>СИСТЕМЫ ОТОПЛЕНИЯ, ВОДОСНАБЖЕНИЯ, КАНАЛИЗАЦИИ, ВЕНТИЛЯЦИИ&nbsp;</strong></span></h1>
<p style="text-align: justify">
	<span style="color:#800000;"><strong>(используем энергосберегающие технологии)</strong></span></p>
<p style="text-align: justify">
	<span style="color:#800000;"><strong><a href="/pricelist/pricelist_id1311596949.pdf">Прайс-лист на сантехнические материалы и оборудование</a>&nbsp;</strong></span></p>
<p style="text-align: justify">
	<span style="color:#800000;"><strong><a href="/pricelist/pricelist_id1311599817.pdf">Расценки на сантехнические услуги</a>&nbsp;</strong></span></p>
<p style="text-align: justify">
	&nbsp;</p>
<p style="text-align: justify">
	<span style="color:#800000;"><strong>ООО &laquo;АРХИСТРОЙ&raquo; </strong></span>предлагает комплекс услуг по поставке (обеспечению) и монтажу теплотехнического, вентиляционного, водопроводного и канализационного оборудования от ведущих российских и мировых производителей, производству электротехнических и сантехнических работ, связанных с установкой указанного оборудования.</p>
<p style="text-align: justify">
	Компания <span style="color:#800000;"><strong>ООО &laquo;АРХИСТРОЙ&raquo;</strong></span> осуществляет поставку, монтаж, наладку, ввод в эксплуатацию, гарантийное и послегарантийное обслуживание вышеуказанного оборудования. Производим работы согласно строительной документации как с крупными заказчиками, так и с частными лицами. Качество и скорость исполнения заказа осуществляется за счет профессиональной грамотности специалистов компании и отлаженной схемы поставок необходимого для монтажа оборудования и материалов, необходимых для исполнения заказа. Цель Компании <span style="color:#800000;"><strong>&laquo;АРХИСТРОЙ&raquo;</strong></span> быстрое и качественное исполнение поставленных задач.</p>
<p style="text-align: justify">
	Широкий ассортимент качественного оборудования,&nbsp; стройматериалов, необходимого инструмента, а также наличие многолетнего опыта работы в данной отрасли строительства, надежный, обученный и квалифицированный персонал, в обозначенные сроки обеспечит исполнение задач наших клиентов.</p>
<p style="text-align: justify">
	<span style="color:#800000;"><strong>Сроки исполнения</strong></span> &ndash; слаженный и сплоченный коллектив высококлассных специалистов компании, отлаженный механизм поставок оборудования дают нам возможность обеспечивать исполнение заказов в <span style="color:#800000;"><strong>согласованные сроки</strong></span>.</p>
<p style="text-align: justify">
	<span style="color:#800000;"><strong>Гибкие условия оплаты и система накопительных скидок</strong></span> - позволяют нашим партнерам более эффективно использовать свои материальные ресурсы.</p>
<p style="text-align: justify">
	Надеемся, что наше сотрудничество принесет Вам реальную <span style="color:#800000;"><strong>экономию затрат</strong></span> на производимые работы, а также поможет Вашему бизнесу быть более <span style="color:#800000;"><strong>успешным и стабильным</strong></span>, а отдых в построенном доме или коттедже будет <span style="color:#800000;"><strong>качественным и радостным</strong></span>.</p>
<p style="text-align: justify">
	Предлагаем Вам ознакомиться с <span style="color:#800000;"><strong>расценками</strong> <strong>на оборудование</strong></span>(прайс-лист) для систем отопления, водоснабжения, канализации, вентиляции.</p>
<table border="0" cellpadding="1" cellspacing="1" style="width: 200px">
	<tbody>
		<tr>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309362714_4.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309362714_1.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309362714_0.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309362714_3.jpg" style="width: 113px; height: 150px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309362714_2.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
		</tr>
	</tbody>
</table>
<p style="text-align: justify">
	<span style="color:#800000;"><strong>МОНТАЖ</strong></span></p>
<p style="text-align: justify">
	Мы имеем монтажное подразделение, специалисты&nbsp;&nbsp; которого:</p>
<ul>
	<li style="text-align: justify">
		Прошли обучение у ведущих поставщиков импортного оборудования;</li>
	<li style="text-align: justify">
		Имеют многолетний опыт работы с импортным котловым оборудованием, медными, пластиковыми и поливинилхлоридными трубами;</li>
	<li style="text-align: justify">
		Выполняют полный комплекс монтажных и пуско-наладочных работ по внутренним инженерным системам;</li>
	<li style="text-align: justify">
		Осуществляют монтаж инженерного оборудования индивидуальных и многоквартирных домов, бытовых и промышленных котельных.</li>
</ul>
<p style="text-align: justify">
	Компания <span style="color:#800000;"><strong>ООО &laquo;АРХИСТРОЙ&raquo; </strong></span>предлагает качественный монтаж инженерных систем квалифицированными специалистами в оговоренные с Заказчиком сроки и предоставляем гарантийные обязательства на выполненную нами работу.</p>
<p style="text-align: justify">
	Компания <span style="color:#800000;"><strong>ООО &laquo;АРХИСТРОЙ&raquo; </strong></span>предлагает следующие виды услуг:</p>
<ul>
	<li style="text-align: justify">
		Монтаж котельных установок и котельного оборудования промышленного и бытового назначения;</li>
	<li style="text-align: justify">
		Монтаж тепловых пунктов;</li>
	<li style="text-align: justify">
		Монтаж внутренних систем отопления различных видов и назначения;</li>
	<li style="text-align: justify">
		Монтаж воздушных и лучевых систем отопления;</li>
	<li style="text-align: justify">
		Монтаж систем вентиляции и кондиционирования;</li>
	<li style="text-align: justify">
		Оборудование скважин, монтаж систем химводоочистки, водоподготовки, повышения и контроля давления;</li>
	<li style="text-align: justify">
		Монтаж оборудования и систем напорной и безнапорной канализации;</li>
	<li style="text-align: justify">
		Монтаж сантехнических приборов;</li>
	<li style="text-align: justify">
		Пусконаладочные работы;</li>
</ul>
<p style="text-align: justify">
	<strong><span style="color:#800000;">ГАРАНТИИ</span>&nbsp;</strong></p>
<p style="text-align: justify">
	Выполняя свои обязательства, мы осуществляем гарантийную и послегарантийную поддержку поставленного оборудования.</p>
<p style="text-align: justify">
	<span style="color:#800000;"><strong>СЕРВИС</strong></span></p>
<p style="text-align: justify">
	Сертифицированные специалисты нашей сервисной службы выполняют работы по техническому сопровождению и обслуживанию смонтированного нашей компанией оборудования на объектах заказчика.</p>
<p style="text-align: justify">
	Нашими партнерами являются ведущие немецкие компании, занимающие лидирующие позиции на мировом рынке по производству теплотехнического оборудования:</p>
<p style="text-align: justify; margin-left: 40px">
	<span style="color:#800000;"><strong>Buderus</strong></span> отопительные настенные и напольные газовые и жидко топливные котлы, водонагреватели, бойлеры, автоматика управления, климат, каскад.</p>
<p style="text-align: justify; margin-left: 40px">
	<span style="color:#800000;"><strong>Viessmann </strong></span>отопительные настенные и напольные газовые и жидко топливные котлы, водонагреватели, бойлеры, автоматика управления, климат, каскад.</p>
<p style="text-align: justify; margin-left: 40px">
	<span style="color:#800000;"><strong>Junkers </strong></span>отопительные настенные и напольные газовые и жидкотопливные котлы, водонагреватели, бойлеры, автоматика управления, климат, каскад.</p>
<p style="text-align: justify; margin-left: 40px">
	<span style="color:#800000;"><strong>Grundfoss </strong></span>насосы скваженные, повысительные, дренажные, циркуляционные, бытовые и промышленные.</p>
<p>
	<strong><a href="/simplecontacts.html">КОНТАКТЫ</a></strong></p>
