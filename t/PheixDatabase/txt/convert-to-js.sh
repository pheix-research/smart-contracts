#!/bin/sh

OUTPUT=datasets.js

echo "// auto generated data sets in JavaScript strings array" > $OUTPUT;
echo "var datasets = new Array;" > $OUTPUT;

find `pwd` -maxdepth 2 -mindepth 2 -type f -name "*.txt" -printf '%h/%f\n' | while read file; do
#   COUNTER=$((COUNTER + 1))
    echo -n "datasets.push({file:\"${file}\", content:" >> $OUTPUT;
    while IFS='' read -r line; do
        line=$(echo "$line" | tr -d '\t\n\r' | sed -r 's/\"/\\\"/g')
        echo -n "\"$line\" + " >> $OUTPUT;
    done < "$file"
    echo " \"\"});" >> $OUTPUT;
done

iconv -f utf-8 -t utf-8 -c ${OUTPUT} -o clean-${OUTPUT}
rm ${OUTPUT}
mv clean-${OUTPUT} ${OUTPUT}