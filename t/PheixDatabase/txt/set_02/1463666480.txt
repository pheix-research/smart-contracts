<div class="head-delimiter"></div>

<div class="content indent">
    <!--content-->
    <div class="thumb-box9" style="margin-top:-30px;">
        <div class="container">
            <p class="title"><span>Объекты</span></p>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="thumb-pad2">
                        <div class="thumbnail">
                            <figure><a href="object-0.html"><img src="images/castiglion/objects/0.jpg" alt="" border=0></a></figure>
                            <div class="caption">
                                <a href="object-0.html">Загорская АЭС</a>
                                <hr>
                                <p>Загорская ГАЭС конструктивно разделяется на две очереди — собственно Загорскую ГАЭС (первая очередь) и строящуюся Загорскую ГАЭС-2 (вторая очередь).</p>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" data-wow-delay="0.1s">
                    <div class="thumb-pad2">
                        <div class="thumbnail">
                            <figure><a href="object-1.html"><img src="images/castiglion/objects/1.jpg" alt="" border=0></a></figure>
                            <div class="caption">
                                <a href="object-1.html">ЦПК</a>
                                <hr>
                                <p>В гидролаборатории ЦПК проводится отработка действий в условиях невесомости открытого космоса на полноразмерном макете орбитальной станции МКС.</p>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" data-wow-delay="0.2s">
                    <div class="thumb-pad2">
                        <div class="thumbnail">
                            <figure><a href="object-2.html"><img src="images/castiglion/objects/2.jpg" alt="" border=0></a></figure>
                            <div class="caption">
                                <a href="object-2.html">Щепкина, 42</a>
                                <hr>
                                <p>Выполнение строительных работ на территории Федерального космического агентства Роскосмос - ул. Щепкина, д. 42. </p>
                            </div>  
                        </div>
                    </div>
                </div>                 
            </div>
            <div class="row" style="margin-top:30px;">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="thumb-pad2">
                        <div class="thumbnail">
                            <figure><a href="object-3.html"><img src="images/castiglion/objects/3.jpg" alt="" border=0></a></figure>
                            <div class="caption">
                                <a href="object-3.html">Красная площадь</a>
                                <hr>
                                <p>Красная площадь дом 5 - Комплекс зданий музеев московского Кремля.</p>
                            </div>  
                        </div>
                    </div>
                </div>     
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" data-wow-delay="0.1s">
                    <div class="thumb-pad2">
                        <div class="thumbnail">
                            <figure><a href="object-4.html"><img src="images/castiglion/objects/4.jpg" alt="" border=0></a></figure>
                            <div class="caption">
                                <a href="object-4.html">ЦПК (объект №2)</a>
                                <hr>
                                <p>В Центре подготовки космонавтов участники экипажей ТПК «Союз МС» приступили к заключительной экзаменационной сессии</p>
                            </div>  
                        </div>
                    </div>
                </div>     
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" data-wow-delay="0.2s">
                    <div class="thumb-pad2">
                        <div class="thumbnail">
                            <figure><a href="object-5.html"><img src="images/castiglion/objects/5.jpg" alt="" border=0></a></figure>
                            <div class="caption">
                                <a href="object-5.html">ЦПК (объект №3)</a>
                                <hr>
                                <p>Руководство и сотрудники ЦПК имени Ю.А. Гагарина, отряд космонавтов поздравляют Валерия Полякова с днем рождения и желают ему крепкого здоровья.</p>
                            </div>  
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>

<section class="content_map">
      <div class="google-map-api"> 
        <div id="map-canvas" class="gmap"></div> 
      </div> 
</section>