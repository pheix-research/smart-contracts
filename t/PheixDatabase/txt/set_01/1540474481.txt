<h1>По Мещере на фэтбайках. Петушки-Шатура&nbsp; с осмотром руин храмов в урочищах Илкодино и Курилово. 7.10.2018 (85 км).</h1>

<p><br />
Продолжаю посты о путешествиях по Мещере. Целью этой поездки было посещение руин храмов в урочищах Иколдино и Курилово, ну и конечно золотая осень :-). Для&nbsp;На этот раз ко мне присоединилcя Евгений&nbsp;<a href="https://www.instagram.com/tugrik2k/" target="_self">https://www.instagram.com/tugrik2k/</a>, для него&nbsp;это был первый ПВД на фэтбайке и он прекрасно справился с маршрутом и сделал много отличных фото, которые и будут украшать этот отчет.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/29281/29281_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/29281/29281_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/29015/29015_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/29015/29015_800.jpg" /></a></p>

<p><em>Мост через реку Сеньга.</em></p>

<p>&nbsp;</p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/29728/29728_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/29728/29728_800.jpg" /></a></p>

<p><em>Мост через реку Сеньга.</em></p>

<p><br />
Мост через реку Сеньга. В конструкции моста использованы пеналы от ПВО-шных ракет с артилерийского полигона&nbsp;Костеревский. Благодаря организации этого полигона в 1953 г мы сейчас и катаем всей велобратией по Мещёре. На самом деле история этих мест трагична - много деревень были выселены во время расчистки места под этот артполигон. Сейчас природа забирает свое - урочища зарастают, мосты разметает водами...</p>

<p>&nbsp;</p>

<p><br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/29589/29589_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/29589/29589_800.jpg" /></a></p>

<p><em>Река Сеньга</em><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/29962/29962_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/29962/29962_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/30278/30278_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/30278/30278_800.jpg" /></a></p>

<p><em>Вереск.</em></p>

<p><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/40264/40264_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/40264/40264_800.jpg" /></a></p>

<p><em>Памятный крест на месте бывшей деревни Борисово.</em></p>

<p><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/40454/40454_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/40454/40454_800.jpg" /></a><br />
<em>Мещёра - рай для фэтбайкера :-)) Песок!!!</em><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/30636/30636_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/30636/30636_800.jpg" /></a></p>

<p><em>Мещерские сосны.</em></p>

<p><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/30757/30757_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/30757/30757_800.jpg" /></a></p>

<p><em>Песчаные дороги Мещеры.</em></p>

<p><br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/30987/30987_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/30987/30987_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/31266/31266_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/31266/31266_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/31713/31713_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/31713/31713_800.jpg" /></a></p>

<p><em>Сосны.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/31954/31954_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/31954/31954_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/32002/32002_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/32002/32002_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/32304/32304_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/32304/32304_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/32936/32936_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/32936/32936_800.jpg" /></a></p>

<p><em>Наши байки.</em></p>

<p><br />
<br />
<strong>Церковь Рождества Христова (1851 г. постройки) в Илкодино</strong><br />
Это первая достопримечательность нашего маршрута. Традиционно даю краткую справку по этим местам:<br />
<br />
<em>Некогда богатое и шумное селение Илкодино находилось в Шатуре в пятидесяти пяти верстах от губернского центра и примерно на таком же расстоянии от города уездного. Стояло село в живописнейшем месте, неподалеку от места слияния шатурских рек &nbsp;- Поли и Клязьмы. Через поселение проходил и известный Владимирский тракт, по которому в то время постоянно двигались торговые обозы и шли путники.<br />
<br />
Сейчас села нет, и на его месте вырос лес, но если судить по описаниям известного писателя Златовратского, то еще в 1864 году Илкодино было весьма живым и красивым селом. Писатель был очарован красотой природы в урочище и храмом, который стоял в селе. Действовала в селе школа, работала новенькая мельница.<br />
<br />
Особенно сильно Илкодино разрослось к концу девятнадцатого столетия, в селе регулярно проходили ярмарки, были открыты кабаки и трактиры, работал постоялый двор. Илкодино было единственным крупным населенным пунктом в местности, все близлежащие к селу территории оставались практически незаселенными. Согласно рассказам старожилов &nbsp;- в Илкодино царская власть разрешала селиться крестьянам, которые бежали из крепостной неволи, и потому население села было весьма разношерстным.<br />
<br />
ИЛКОДИНСКИЙ ПРИХОД<br />
<br />
Краеведы установили, что учрежден Илкодинский приход был примерно в первой половине девятнадцатого столетия. До 1843 года все земли, которые вошли в новый приход относились к храму поселения Николы-Пустого, но до этого села местным жителям добираться было трудно и потому-то в Илкодино и было решено построить свою церковь.<br />
<br />
Храм из камня начали возводить в 1841 году, и средства на строительство выделил местный помещик Поливанов, также расходы на стройку взяли на себя и владельцы Мишеронского стеклозавода, жертвовали на храм и жители села. После постройки храма деревня Илкодино получает статус села и в новой церкви начинаются службы, хотя стройка его была еще не окончена и продолжалась до 1851 года.<br />
<br />
Храм в Илкодино был возведен белокаменный и по архитектуре близок к позднему классицизму. У церкви был купольный свод, который венчала декоративная главка, а с правой стороны строения располагался алтарь, с запада располагались колокольня и трапезная. Убранство храма было сдержанным, но весьма красивым.<br />
<br />
В церкви располагались престолы &ndash; в честь Рождества, во славу иконы Богородицы и в честь Николая Чудотворца. Все престолы были освящены в разные года существования храма. В храме была достаточно богатая утварь и известные иконы, в том числе Нерукотворный Спас, Знамение Богородицы, Спасителя и Божьей Матери. Пожертвования на ризы для икон внес все тот же помещик Поливанов.<br />
<br />
В церкви была и уникальная картина, изображавшая сцену бичевания Спасителя. Эта картина была создана талантливыми руками одного крепостного крестьянина, которого &nbsp;в награду за труд Поливанов отпустил на волю. &nbsp;При храме был причт, состоявший из священника, дьякона и псаломщика.&nbsp;<br />
<br />
ЛЕГЕНДА О СОКРОВИЩАХ ИЛКОДИНСКОГО УРОЧИЩА<br />
<br />
Богатое село Илкодино просуществовало практически до середины века 20-го. Но в 1953 году власти дают распоряжение переселить всех жителей Илкодино в другие места, а земли урочища отвести под полигон Костеревский.<br />
<br />
Постепенно из села уехали все жители, кроме старой Анисьи Батуриной, которая жила на самой окраине села. Вскоре село Илкодино перестало существовать, в эту сторону почти никто и не ходил, кроме грибников и рыбаков, посещавших окрестности села в летнюю пору. &nbsp;Лишь Анисья Батурина продолжала жить в своем одиноком домишке, и редко забредали к ней странники и паломники. Выселить старушку власти так и не решились.<br />
<br />
Один из паломников, зашедших на огонек к Анисье, остался у нее, и потому последние годы своей жизни Батурина провела не в одиночестве. &nbsp;Жила Батурина долго, почти до конца 20-го века, а потом тихо умерла и ее постоялец некоторое время жил в ее доме вместе со старым псом. Но вот умер и он, &nbsp;и дом опустел, а потом в нем по неизвестным причинам случился пожар.<br />
<br />
Вскоре к пепелищу на месте дома Батуриной стали захаживать странные люди и поползли слухи, якобы где-то у дома старая Анисья зарыла церковные ценности, переданные ей на хранение в свое время последним церковным старостой храма в Илкодино. &nbsp;Но все попытки кладоискателей были напрасными &ndash; никаких сокровищ у дома Анисьи им найти не удалось.&nbsp;<br />
<br />
От стараний искателей кладов пострадала и колокольня закрытой церкви. &nbsp;Они раскопали весь ее фундамент экскаватором, и теперь на месте стройной и звонкой колокольни зияет черный провал. (материал с сайта&nbsp;</em><a href="https://xn----7sbba0ba9aoomhm0cr7h.xn--p1ai/%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D0%BD%D0%BE%D0%B5/6179-%D1%83%D1%80%D0%BE%D1%87%D0%B8%D1%89%D0%B5-%D0%B8%D0%BB%D0%BA%D0%BE%D0%B4%D0%B8%D0%BD%D0%BE-%D0%B2-%D1%88%D0%B0%D1%82%D1%83%D1%80%D0%B5" target="_self">https://типичная-шатура.рф/интересное/6179-урочище-илкодино-в-шатуре</a>)<br />
<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/39984/39984_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/39984/39984_800.jpg" /></a><br />
<em>Церковь Рождества Христова (1851 г. постройки) в Илкодино.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/39718/39718_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/39718/39718_800.jpg" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/32516/32516_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/32516/32516_800.jpg" /></a><br />
<em>Руины церкви&nbsp;Рождества Пресвятой Богородицы (постр около 1832-... г.).в селе Илкодино.</em><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/33157/33157_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/33157/33157_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/34518/34518_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/34518/34518_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/34117/34117_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/34117/34117_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/33397/33397_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/33397/33397_800.jpg" /></a></p>

<p><em>Церковь Рождества Христова. Интерьер. Импровизированный иконостас.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/33695/33695_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/33695/33695_800.jpg" /></a>&nbsp;<br />
<em>Стенные фрески почти стерлись под воздействием атмосферы.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/33952/33952_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/33952/33952_800.jpg" /></a><br />
<em>Застигнутая холодом в расплох гадюка. К вечеру воздух сильно стыл - осень.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/34712/34712_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/34712/34712_800.jpg" /></a><br />
<em>Снимаю кино.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/35394/35394_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/35394/35394_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/35626/35626_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/35626/35626_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/35974/35974_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/35974/35974_800.jpg" /></a></p>

<p><em>Привал.</em></p>

<p><br />
<strong>Курилово. Церковь рождества Пресвятой Богородицы (постр около 1832-... г.).</strong><br />
<br />
Второй объет интереса нашей небольшой экспедиции&nbsp;Церковь рождества Пресвятой Богородицы в урочище Курилово. Церковь сейчас пытаются реставрировать. Я тут был году в 2004 - храм стоял зарошенный в лесах и тогда уже был Меккой для джипперов и других любителей приключений. Рядом с храмом построено несколько времянок, выкопали колодец - в общем пытаются обживать эти места. На &nbsp;плакате, установленном рядом с храмом довольно ёмко описана история храма, села Курилово и окресностей. Тут в 1953-м году пытались сделать артиллерийский полигон - выселили деревни и вместе с домами и сараями перевезли в крупные населенные пункты Шатура, Мишеронский, Рошаль и др. Плакат по истории храма и окрестностей я сфотографировал (кликабельно) - почитайте - интересно.</p>

<p><br />
<img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/38197/38197_800.jpg" /></p>

<p><em>Информационный стенд около руин церкви.</em></p>

<p><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/36460/36460_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/36460/36460_800.jpg" /></a><br />
<em>Курилово. Церковь рождества Пресвятой Богородицы.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/36880/36880_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/36880/36880_800.jpg" /></a><br />
<em>Курилово. Церковь рождества Пресвятой Богородицы. Интерьеры.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/37563/37563_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/37563/37563_800.jpg" /></a><br />
<em>Курилово. Церковь рождества Пресвятой Богородицы. Фасад.</em><br />
<br />
В стройный ряд Жениных фото вставлю несколько своих с телефона Xiaomi Readmi note 3<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/38613/38613_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/38613/38613_800.jpg" /></a></p>

<p><em>Курилово. Церковь рождества Пресвятой Богородицы. Вид со стороны кладбища.</em></p>

<p><br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/38738/38738_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/38738/38738_800.jpg" /></a></p>

<p><em>Ур. Курилово, старые могилы.</em></p>

<p><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/39126/39126_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/39126/39126_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/39187/39187_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/39187/39187_800.jpg" /></a></p>

<p><em>Курилово. Церковь рождества Пресвятой Богородицы (постр около 1832-... г.).</em></p>

<p><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/39674/39674_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/39674/39674_800.jpg" /></a><br />
<em>Временное поселение реставраторов храма.</em><br />
<br />
<br />
Ехали мы неспеша, перекусывали, фотографировали, лазили по руинам - а день короткий - осень, поэтому возвращались уже в ночи.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/37684/37684_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/37684/37684_800.jpg" /></a></p>

<p><em>Ужин недалеко от поселка &quot;Северная Грива&quot;.</em></p>

<p><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/38075/38075_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/38075/38075_800.jpg" /></a></p>

<p>&nbsp;</p>

<p>ВИДЕО</p>

<p><iframe frameborder="0" height="315" src="https://www.youtube.com/embed/BdBSU0jZjQM?controls=0" width="560"></iframe></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><br />
Ссылка на трэк:&nbsp;<a href="https://www.gpsies.com/map.do?fileId=ybravjdjvziucfvj">https://www.gpsies.com/map.do?fileId=ybravjdjvziucfvj</a></p>

<p><br />
<br />
<iframe frameborder="0" height="500" name="embed_84837712_1" src="https://l.lj-toys.com/?auth_token=sessionless%3A1540368000%3Aembedcontent%3A84837712%261%26%26%3A251a515a17d9c3ba074c7173ef930c327bb325a1&amp;moduleid=1&amp;preview=&amp;journalid=84837712&amp;noads=" width="800"></iframe></p>
