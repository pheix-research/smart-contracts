<h1>Октябрьский пеший ПВД по Мещере под рук. К. Титкова. Маршр. Войново-Авсютино 20-22 окт. 2017 (67 км)</h1>

<p>Прогулялся с друзьями по Мещере - на этот раз пешком. Маршрут составил и был штурманом Кирилл Титков. Состав группы: Кирилл, Сергей, Анна, Владимир, Максим и&nbsp;я - Павел Ляхов.&nbsp;&nbsp;Поход был задуман как подготовка к ММБ-осень 2017. Этот год можно по праву назвать годом Мещеры - я уже 2 раза в этом году был в велопоходах, один раз ездил на машине (с заездом в заброшенный поселок Моховое&nbsp;<a href="http://drtg.ru/zabroshenniy-poselok-mohovoe-02-07-2017.html" target="_blank">http://drtg.ru/zabroshenniy-poselok-mohovoe-02-07-2017.html&nbsp;</a>), ММБ осень 2017 был в Мещере - я этот ММБ пропустил - рисовал сташные маски на Хеллоуин-2017 (<a href="http://www.pavel-lyakhov.ru/rus/aquagrim.html" target="_blank">http://www.pavel-lyakhov.ru/rus/aquagrim.html</a>&nbsp;). Тенденция и тренд на лицо!</p>

<p>Пользуясь случаем передаю привет всем участникам похода&nbsp;:-)&nbsp;<br />
<br />
На этот раз я взял с собой зеркалку Canon 350D 2004 года. Фотоаапарат оказался оченьудобным и миниатбрным, если использовать kitовый объектив.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/74498/74498_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/74498/74498_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Озеро Горбатое, с того места, где мы к нему подходили имело заболоченный берег. На берегу ростет клюква.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/74890/74890_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/74890/74890_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
Панорама озера Горбатое:<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/86453/86453_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/86453/86453_900.jpg" style="height:284px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/75186/75186_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/75186/75186_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/75343/75343_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/75343/75343_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Мещерские дорожки.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/75700/75700_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/75700/75700_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/75793/75793_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/75793/75793_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Бывшие торфоразработки:<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/76046/76046_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/76046/76046_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Дорожки - бывшие насыпи узкоколеек.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/76378/76378_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/76378/76378_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Первый день похода. Обшее фото:<br />
<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/76750/76750_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/76750/76750_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Остатки бетонных конструкций бывших торфоразработок.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/76857/76857_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/76857/76857_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/77207/77207_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/77207/77207_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/77355/77355_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/77355/77355_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/77712/77712_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/77712/77712_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Неожиданно выглянуло солнце.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/77844/77844_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/77844/77844_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Остатки золотой осени.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/78083/78083_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/78083/78083_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Велотрофей.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/78495/78495_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/78495/78495_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Переправа.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/78780/78780_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/78780/78780_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/79069/79069_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/79069/79069_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Привал.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/79353/79353_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/79353/79353_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Этот мост я в этом году уже видел, проезжая на велосипеде.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/79454/79454_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/79454/79454_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Ночной брод&nbsp; через реку Сеньга.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/79703/79703_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/79703/79703_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/79906/79906_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/79906/79906_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/80300/80300_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/80300/80300_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/80513/80513_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/80513/80513_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/80738/80738_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/80738/80738_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/80971/80971_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/80971/80971_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/81277/81277_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/81277/81277_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/81452/81452_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/81452/81452_900.jpg" style="height:522px; width:800px" /></a><br />
<br />
<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/81720/81720_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/81720/81720_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/81970/81970_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/81970/81970_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/82388/82388_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/82388/82388_900.jpg" style="height:532px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/82548/82548_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/82548/82548_900.jpg" style="height:532px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/82727/82727_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/82727/82727_900.jpg" style="height:532px; width:800px" /></a><br />
<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/83147/83147_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/83147/83147_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
Первый снег.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/83249/83249_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/83249/83249_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/83621/83621_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/83621/83621_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/83811/83811_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/83811/83811_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/83981/83981_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/83981/83981_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/84407/84407_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/84407/84407_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/84512/84512_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/84512/84512_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
Общее фото. 2 день похода.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/84817/84817_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/84817/84817_900.jpg" style="height:453px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/84997/84997_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/84997/84997_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
Узел связи.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/85369/85369_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/85369/85369_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/85752/85752_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/85752/85752_900.jpg" style="height:532px; width:800px" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/85874/85874_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/85874/85874_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
Авсюнино.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg2/83781880/86076/86076_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg2/83781880/86076/86076_900.jpg" style="height:533px; width:800px" /></a><br />
<br />
Ссылка на gps трэк&nbsp;&nbsp;Войново-Авсютино 20-22 окт. 2017 (67 км)<br />
Трэк можно скачать здесь (gpx):&nbsp;<a href="https://yadi.sk/d/qsCHVxm73PpRyn" target="_blank">https://yadi.sk/d/qsCHVxm73PpRyn</a><br />
&nbsp;</p>

<p><strong>МЕТКИ: #</strong>Авсютино, #Войнова гора, #мещера, #пеший поход, #поход</p>

<p>&nbsp;Войново-Авсютино 20-22 окт. 2017 (67 км) Трэк можно скачать здесь (gpx): <a href="https://yadi.sk/d/qsCHVxm73PpRyn" target="_blank">https://yadi.sk/d/qsCHVxm73PpRyn</a></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><iframe frameborder="0" height="800" scrolling="no" src="https://www.strava.com/activities/1242077115/embed/0640b38f7d3b388578d293a1dae4ac2af7fb7b50" width="700"></iframe></p>
