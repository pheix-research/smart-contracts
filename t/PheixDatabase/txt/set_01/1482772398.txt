<h1>Испытание фэтбайка в реальных снежных условиях. Маршрут: пл.Некрасовская - ГАБО - Катуар. Фэтбайк, 35 км (03.12.2016)<br />
&nbsp;</h1>

<p>Запись в инстаграм на второй день после путешествия<em>: &quot;Фига себе! Оказывается вчера был всемирный день фэтбайка. В принципе отметился - прокатился лесным маршрутом&nbsp;</em><em><a href="https://www.instagram.com/explore/tags/%D0%BD%D0%B5%D0%BA%D1%80%D0%B0%D1%81%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F/" target="_self">#Некрасовская</a></em><em>-<a href="https://www.instagram.com/explore/tags/%D0%B3%D0%B0%D0%B1%D0%BE/" target="_self">#ГАБО</a></em><em>&nbsp;-&nbsp;<a href="https://www.instagram.com/explore/tags/%D0%BA%D0%B0%D1%82%D1%83%D0%B0%D1%80/" target="_self">#Катуар</a></em><em>. Праздник удался! 35 км проехал, из них по снегу около 28 км. Адски удолбался :-))))</em><em>.&quot;</em></p>

<p>В принципе можно дальше не писать. Однако есть куча фоток, которые думаю достойны публикации на&nbsp;DRTG.RU</p>

<p>&nbsp;</p>

<p>Снега навалило от 10 до 20 см поверх наста. В принципе неплохо - на обычном веле уже не проехать. Почему я вспомнил про обычный горный велосипед - потому что я стартовал именно в этом месте в феврале этого года на своём&nbsp;Merida&nbsp;TFS500D&nbsp;направляясь в сторону Икши, рассчитывая проехать по насту. В тот раз мне это удалось, так как днем снег таял, а ночью подмерзал. В итоге я почти весь путь смог проехать на велосипеде (отчет по этой поездке можно посмотреть тут:&nbsp;<a href="http://drtg.ru/test-drave-merida-tfs-500d.html" target="_self">http://drtg.ru/test-drave-merida-tfs-500d.html</a>&nbsp;).&nbsp;</p>

<p>В этот раз снеговая обстановка не располагала к езде на обычном велосипеде - но оказалась интересной для попытки проехать на фэтбайке.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/177797/177797_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/177797/177797_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Я ехал вдоль лыжни, не заезжая на неё - на полуспущенных колесах по лесных просеках велосипед шел хорошо, однако усилий требовалось больше чем при прохождении этой дистанции на лыжах. Оно и понятно - внедорожник требует больше ресурсов для своего движения.</p>

<p>&nbsp;</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/178128/178128_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/178128/178128_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p><br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/180358/180358_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/180358/180358_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
В лесу толщина снежного покрова меньше, чем на открытых местах. На болотах глубина снега достигала 30 см - там приходилось уже идти пешком, опираясь на велосипед, чтобы распределить вес и не проваливаться в болотную жижу.</p>

<p>На просеке газопровода толщина снега также была около 30 см, но я вышел на следы от снегохода и с переменным успехом проехал по ним.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/179847/179847_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/179847/179847_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Участок маршрута по газопроводу до кольцевой ЖД оказался самым труднопроходимым из за большого количество сухого не слежавшегося снега. Там не менее с переменным успехом по снегоходному следу удавалось проехать &quot;в седле&quot;. Оказалось что по следу снегоходной лыжи лучше удавалось ехать, а при езде по следу от гусениц велосипед все-таки соскальзывал.</p>

<p>Позже выяснилось, что если бы я сильнее спустил колеса, проходимость была бы выше.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/180991/180991_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/180991/180991_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Еще один лайфхак при езде на фэтбайке по снегу - если велосипед начинает пробуксовывать, необходимо перенести весь вес на заднее колесо, тогда есть вероятность что удастся прогрестись.</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/178560/178560_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/178560/178560_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/178260/178260_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/178260/178260_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Не доезжая до железной дороги, примерно на 10-м километре пути я остановился на привал.</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/180569/180569_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/180569/180569_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Мороз хорошо подсушил деревья, так что проблем с розжигом костра не было.</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/179517/179517_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/179517/179517_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>От кольцевой железной дороги до ГАБО я проехал 2 километра по обочине Рогачевского шоссе.</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/179319/179319_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/179319/179319_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
Доехал до Габровской лыжни, сделал символический круг по трассе.</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/179012/179012_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/179012/179012_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Трасса ГАБО сделана на 5 с плюсов. С удовольствием там прокатился :-)<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/181440/181440_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/181440/181440_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Обратно поехал по обочине Рогачевского шоссе до кольцевой железной дороге, далее по ней до пересечения с дорогой на Катуар.</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/182136/182136_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/182136/182136_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Вдоль кольцевой ЖД, &nbsp;на насыпи проезжается более менее неплохо, так как снег сдувается отсюда ветром, в том числе и от проходящих поездов.<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/181788/181788_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/181788/181788_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Доехал до платформы 120 км кольцевой железной дороги, и в голове скользнула крамольная мысль &quot;слиться в строну Икши на электричке&quot;. Но посмотрев расписание электропоездов я понял что за 3 часа простоя на морозе я быстрее околею. В итоге я потихоньку попедалировал в сторону Катуара по слегка припорошенной грунтовке, идущей вдоль железки.<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/181603/181603_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/181603/181603_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/182405/182405_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/182405/182405_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Через некоторое время я выехал на заснеженную, но все-таки твердую дорогу Кузяево - Катуар. Теперь полуспущенные колеса стали неудобными для езды, я &nbsp;остановился, подкачал колеса и поехал дальше как на обычном велосипеде. Единственно, что на толстых колесах практически не ощущаешь припорошенные снегом выбоины на дороге :-)<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/178904/178904_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/178904/178904_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Дом - аквариум, показался очень необычным :-)</p>

<h2>Видео</h2>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/182571/182571_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/182571/182571_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>Поездка в целом была позитивной, жду наста - буду штурмовать направление Морозки-Поварово, это будет интересным испытанием для меня и велосипеда :-))</p>

<p><iframe frameborder="0" height="450" src="https://www.youtube.com/embed/7g31YEfDbKU" width="800"></iframe></p>

<h2>Ссылка на gps трэки</h2>

<p>Скачать трэк:&nbsp;https://yadi.sk/d/ltXnPuhO358Ki5&nbsp;</p>

<p>&nbsp;</p>

<p><iframe frameborder="0" height="405" scrolling="no" src="https://www.strava.com/activities/792305910/embed/5766a3d0e4d4e9dab670bc41bfb718b6dd31e009" width="590"></iframe></p>
