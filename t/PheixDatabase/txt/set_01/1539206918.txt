<h1><strong>Путешествие из Опухликов в Геленджик. Интересности. Обратная сторона Геленджика. Краткий фото отчет с комментариями. Июль 2018.</strong></h1>

<p>В этой заметке я опишу как можно разнообразить матрасный отдых в Геленджике своими силами, не прибегая к общераспространенным развлечениям. К отдыху в Геленджике я подошёл как исследователь, и помимо хождению на пляж открыл для себя много интересного.</p>

<p>В Геленджик мы поехали спонтанно. Планировалось, что я приеду к Полине в на общественном транспорте Опухлики, и от туда мы поедем на машине в Белоруссию. Но в последний момент решили что поедем в Геленджик. И поехали!</p>

<p>Навигатор показывал что быстрее доехать до Москвы, а от туда по трассе М4-Дон до места назначения. Мы особо не спешили и решили поехать вдоль белорусской границы.</p>

<p>Недалеко от Опухликов мы остановили машину&nbsp; высадились для просмотра&nbsp; заброшенного пионерского лагеря ( возможно военной части).<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/12767/12767_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/12767/12767_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/12841/12841_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/12841/12841_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/13103/13103_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/13103/13103_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/13468/13468_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/13468/13468_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/13735/13735_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/13735/13735_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/13920/13920_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/13920/13920_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/14275/14275_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/14275/14275_800.jpg" /></a><br />
<br />
&nbsp;</p>

<p>Проехали город Велиж, который я проезжал в июньском велопоходе от Опухликов до Смоленска. До Велижа ехали по платной дороге (я так понимаю это единственная дорога с твердым покрытием. Альтернатива - грунтовкаи, по которым я проезжал в велопоходе). Эта платная дорога отличается от платных дорог трассы М4-Дон - обычная асфальтированная двухколейная дорога, вся в заплатках. Машин практически нет, в этом месте карты вообще мало жилых населенных пунктов. На выходе из платного участка стоит шлагбаум с кассой, установленной в офисе-бытовке. Проезд стоил 200 руб.</p>

<p>&nbsp;</p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/14536/14536_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/14536/14536_800.jpg" /></a></p>

<p>Ехали не спеша, так как было много знаков ограничения скорости, и местами дороги были с выбоинами. Ехали в походном режиме с ночёвками в палатке в лесных массивах. Так получилось, что всю дорогу за нами гнались тучи, заряженные проливными дождями, но мы как правило успевали от них уходить и не разу не попали под дождь. Утром дождь заставал нас слегка покрапывал когда мы были уже а машине: весь день мы от него убегали, а на следующее утро он нас нагонял.</p>

<p><br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/14642/14642_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/14642/14642_800.jpg" /></a>&nbsp;<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/15086/15086_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/15086/15086_800.jpg" /></a></p>

<p><strong>Археологический памятник Елагино</strong></p>

<p>Проезжая Орловскую область мы посетили археологический памятник Елагино на территории которого около 2-3 тысячи лет до нашей эры было поселение. Место силы оборудовано беседками, есть родник - чаша. Искупался в речке Любовша, через которую перекинут металлический пешеходный мост, постирал нижнее белье.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/15284/15284_original.jpg" target="_blank"><img alt="image11.jpeg" src="https://ic.pics.livejournal.com/drtg6/84837712/15284/15284_800.jpg" /></a></p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/16137/16137_original.jpg" target="_blank"><img alt="image15.jpeg" src="https://ic.pics.livejournal.com/drtg6/84837712/16137/16137_800.jpg" /></a>&nbsp;<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/15960/15960_original.jpg" target="_blank"><img alt="image14.jpeg" src="https://ic.pics.livejournal.com/drtg6/84837712/15960/15960_800.jpg" /></a>&nbsp;<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/15636/15636_original.jpg" target="_blank"><img alt="image13.jpeg" src="https://ic.pics.livejournal.com/drtg6/84837712/15636/15636_800.jpg" /></a>&nbsp;<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/15556/15556_original.jpg" target="_blank"><img alt="image12.jpeg" src="https://ic.pics.livejournal.com/drtg6/84837712/15556/15556_800.jpg" /></a></p>

<p>В целом остальной путь промелькал за окнами автомобиля.</p>

<p>&nbsp;</p>

<p><strong>Ленин!</strong></p>

<p>орадовало как в Краснодарском крае сохраняют историю, например в селе Краснопартизанском памятник Ленину, который стараются держать в идеальном состоянии, подкрашивая и облагораживая вокруг него территорию.</p>

<p>&nbsp;</p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/28597/28597_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/28597/28597_800.jpg" /></a>&nbsp;<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/28754/28754_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/28754/28754_800.jpg" /></a></p>

<p>&nbsp;</p>

<p><strong>Чебурашка</strong></p>

<p>Полазив по окрестностям, я обнаружил, что помимо дольменов и гор тут есть ещё несколько интересных мест, в том числе которые также попадут в это повествование.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/16718/16718_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/16718/16718_800.jpg" /></a></p>

<p>&nbsp;</p>

<p><strong>Развалины санатория им. Октябрьской революции. Обратная сторона Геленджика.</strong></p>

<p>Заброшенный детский санаторий (ДКТС им. Октябрьской революции).</p>

<p><em>История: в 1913 году состоялось открытие санатория для лечения костного туберкулеза на 25 коек. В 1920 году он был переименован в санаторий им. Октябрьской революции. С 1945 года расширился до 250 коек. Было открыто хирургическое отделение для лечения костнотуберкулезных больных. В 1986 году, в связи с ликвидацией костно-суставного туберкулеза, санаторий перепрофилирован для лечения детей с заболеваниями опорно-двигательного аппарата.(Википедия)</em></p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/17089/17089_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/17089/17089_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/17246/17246_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/17246/17246_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/17612/17612_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/17612/17612_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/17847/17847_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/17847/17847_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/17928/17928_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/17928/17928_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/18391/18391_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/18391/18391_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/18566/18566_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/18566/18566_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/18905/18905_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/18905/18905_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/19149/19149_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/19149/19149_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/19237/19237_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/19237/19237_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/19528/19528_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/19528/19528_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/19756/19756_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/19756/19756_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/20150/20150_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/20150/20150_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/20308/20308_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/20308/20308_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/20586/20586_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/20586/20586_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/20869/20869_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/20869/20869_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/21167/21167_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/21167/21167_800.jpg" /></a></p>

<p>&nbsp;</p>

<p><strong>Развалины старинного дома</strong><br />
Ещё один &nbsp;заброшенный дом. С него кстати хороший вид на море. Пока не знаю его историю, если кто знает напишите - интересно.</p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/21304/21304_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/21304/21304_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/21664/21664_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/21664/21664_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/21843/21843_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/21843/21843_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/22078/22078_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/22078/22078_800.jpg" /></a></p>

<p><br />
<strong>Гипербарической комплекс института океаналогии имени Ширшова.</strong></p>

<p>Сосуд во дворе - похоже на камеру, спускаемся на глубину. Судя по шильдику сделан на военном заводе. Иллюминаторы имеют странный рисунок на стеклах - похоже следствие работы под высоким давлением. Ещё во дворе стоит какая-то установка, заглубления под землю с лебедкой над нею - похоже это одна из барокамер, размещенная вне здания.&nbsp; Окна стоят новые пластиковые, так что возможно объект функционирует. Место интересное, похоже закрыто на лето или на всегда.</p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/22398/22398_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/22398/22398_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/22709/22709_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/22709/22709_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/22800/22800_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/22800/22800_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/23082/23082_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/23082/23082_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/23466/23466_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/23466/23466_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/23648/23648_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/23648/23648_800.jpg" /></a></p>

<p>&nbsp;</p>

<p><strong>Непонятное сооружение в береговой скале. Сделано капитально, забетонировано.</strong></p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/24023/24023_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/24023/24023_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/24103/24103_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/24103/24103_800.jpg" /></a></p>

<p>&nbsp;</p>

<p><strong>Дольмены</strong></p>

<p>Дольмены на высоте 171 западнее села Широкая щель. Забрались туда на Дустере. Дольмены разрушенные но протягиваются по всему хребту высоты. Энергетика колоссальная. На высоту 171 обычно возят на УАЗиках, но погода располагала и мы смогли проехать на Дустере. В дождь дорога превращается в глиняную реку а реки, которые мы пересекали вброд в мощные потоки воды и камней.</p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/24342/24342_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/24342/24342_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/24789/24789_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/24789/24789_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/25052/25052_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/25052/25052_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/25206/25206_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/25206/25206_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/25593/25593_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/25593/25593_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/25772/25772_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/25772/25772_800.jpg" /></a></p>

<p>Дольмен Старец (на карте Локуса обозначен как дольмен Вдохновение). Сохранился. Был найден случайно при строительстве дороги. Находится в селе Широкая щель на правом берегу притока реки Шепс. Круглое окно дольмена выходит на восток (посмотрел по компасу). Место сильное - ощущается легко. Дольмены строились на разломах, на мощных энергетических потоках. В тех местах их тысячи. Если удастся выбраться в пеший рандомный&nbsp; вылаз думаю найду ещё много не отмеченных на карте. Их можно найти &quot;по нюху&quot; - в смысле ощущается энергетика - так можно найти и тех что под землёй. Дольмены, найденные к сему дню в основном были засыпаны землёй - какие-то сами вышли из нее, какие-то нашли в результате строительных и дорожных работ.</p>

<p><strong>Вершина Плоская (Высота 762,4 м)</strong></p>

<p>Одна из интересных вершин в ближайшем окружении - находится на хребте Маркотхский. Если вы хотите разнообразить свой матрасный отдых - это то что вам нужно. Так как в июле достаточно жарко, необходимо взять с собой не менее 2-х литров воды. Несмотря на жару, в горах может быть прохладно, запросто может пойти дождь, поэтому необходимо взять с собой термуху и ветрозащитную куртку. Кстати если в термобелье и куртку завернуть холодное пиво, то несмотря на жарy, вы поднимите его на гору холодным - это будет ваш бонус на вершине :-).</p>

<p>Восхождение я осуществлял по навигационной программе locus map. Очень порадовала детализация карт Краснодарского края (кстати, первые пять оффлайн карт можно скачать бесплатно, остальные стоят не дорого от 5 до 40 рублей).</p>

<p>На вершине сохранились развалины радиолокационной станции. Когда-то там были установлены большие радиопрозрачные&nbsp; белые шары, укрывающие находящееся внутри них радиолокационное оборудование.</p>

<p>&nbsp;</p>

<p><iframe frameborder="0" height="500" src="https://www.youtube.com/embed/57iygUVCH0s" width="800"></iframe></p>

<p>&nbsp;</p>

<p><iframe frameborder="0" height="405" scrolling="no" src="https://www.strava.com/activities/1690962171/embed/36af29eb724d6117f6574180c47e783b164a6444" width="800"></iframe></p>

<p>&nbsp;</p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/26003/26003_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/26003/26003_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/26289/26289_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/26289/26289_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/26526/26526_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/26526/26526_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/26805/26805_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/26805/26805_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/27124/27124_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/27124/27124_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/27204/27204_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/27204/27204_800.jpg" /></a></p>

<p>&nbsp;</p>

<p><strong>Гора Дооб (Высота&nbsp;434м)</strong></p>

<p>Тоже интересное место, но до вершины я не дошел, так как подходы к ней огорожены. Тем не менее на спуске с горы насладился величественным видом выходящей в море береговой плиты, образующей крутой обрыв. Глубина обрыва на глаз определяется на несколько сотен метров. Видны вереницы морских судов, курсирующих в сторону Новороссийска и обратно.Во время предыдущей прогулки с сыном Ярославом мы встретили оленя с олененком.&nbsp;</p>

<p><strong>Дооб</strong>&nbsp;&mdash; мыс на Чёрном море на юго-востоке Цемесской бухты. Является продолжением Навагирского хребта, который ушёл под воду в результате сильного сжатия земной коры у входа в Цемесскую бухту.</p>

<p>К сожалению, у меня небыло достаточно времени, чтобы детально исследовать окресности горы Дооб. Нашел интересную&nbsp;статью в интернете - для затравки к следующим исследованиям этих мест:<a href="http:// https://www.proza.ru/2012/02/27/1924">&nbsp;https://www.proza.ru/2012/02/27/1924</a></p>

<p><a href="https://ic.pics.livejournal.com/drtg6/84837712/27535/27535_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/27535/27535_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/27733/27733_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/27733/27733_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/28037/28037_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/28037/28037_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/28352/28352_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/28352/28352_800.jpg" /></a></p>

<p><iframe frameborder="0" height="500" src="https://www.youtube.com/embed/3_1m6R1dSuQ" width="800"></iframe></p>

<p><iframe frameborder="0" height="405" scrolling="no" src="https://www.strava.com/activities/1700002077/embed/7cef45cea3d6fbc246d33c32390a4f0de45f474a" width="800"></iframe></p>

<p>Вот так...</p>

<p>&nbsp;</p>

<p><em>Павел Ляхов, Август 2018</em></p>
