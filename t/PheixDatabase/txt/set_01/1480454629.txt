<h1>#полезнаяинфа Подбор рамы велосипеда по росту. Как правильно выбрать ростовку велосипеда. (29.02.2016)</h1>

<p><em>В рубрике #полезнаяинфа я буду публиковать полезные статьи по тематике сайта DRTG.RU.</em></p>

<p>&nbsp;</p>

<p>29 февраля 2016, 22:12</p>

<p>Размер рамы велосипеда - важный параметр, от которого зависит удобство езды на велосипеде. Правильно подобранная рама обеспечивает более высокие результаты как в велоспорте, так и в любительском катании. Особенно важен правильный выбор рамы при покупке велосипеда для длительных велозаездов, в том числе для велотуризма. От размера рамы зависят и геометрические характеристики самой рамы (соответственно ее длина и высота). С изменением размера рамы велосипеда, изменяются и размеры верхней, нижней и подседельной труб.</p>

<p><img alt="" src="images/systemgallery/upload/1472048124_13.jpg" /></p>

<p>Сложилось так, что ростовка рамы обозначается в трех стандартах:</p>

<p>- &quot; (дюймы)</p>

<p>- условные единицы (XS, S, M, L, XL)</p>

<p>- сантиметры (cm).</p>

<p>В данной статье представлена таблица размеров велосипедных рам. Зная рост человека с помощью этой таблицы можно подобрать размер рамы, и, следовательно не ошибиться с ростовкой при покупке велосипеда через интернет (эта же таблица поможет вам при выборе велосипеда в оф-лайн магазине)</p>

<p>&nbsp;</p>

<p><strong>Таблица: подбор велосипеда по принципу рост велосипедиста - размер рамы*.</strong></p>

<table border="1" cellpadding="0" cellspacing="0" style="width:100.0%">
	<tbody>
		<tr>
			<td style="width:15.68%">
			<p>Размер рамы, дюймы</p>
			</td>
			<td style="width:11.32%">
			<p>Ваш рост, см.</p>
			</td>
			<td style="width:15.82%">
			<p>Размер рамы, см.</p>
			</td>
			<td style="width:28.14%">
			<p>Размер рамы, условн.ед.</p>
			</td>
			<td style="width:29.06%">
			<p>Обозначение</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>13&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>130 - 145</p>
			</td>
			<td style="width:15.82%">
			<p>33</p>
			</td>
			<td style="width:28.14%">
			<p>XS (XSmall)</p>
			</td>
			<td style="width:29.06%">
			<p>Минимальный</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>14&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>135 - 155</p>
			</td>
			<td style="width:15.82%">
			<p>35,6</p>
			</td>
			<td style="width:28.14%">
			<p>XS (XSmall)</p>
			</td>
			<td style="width:29.06%">
			<p>Минимальный</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>15&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>145 - 160</p>
			</td>
			<td style="width:15.82%">
			<p>38,1</p>
			</td>
			<td style="width:28.14%">
			<p>S (Small)</p>
			</td>
			<td style="width:29.06%">
			<p>Малый</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>16&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>150 - 165</p>
			</td>
			<td style="width:15.82%">
			<p>40,6</p>
			</td>
			<td style="width:28.14%">
			<p>S (Small)</p>
			</td>
			<td style="width:29.06%">
			<p>Малый</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>17&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>156 - 170</p>
			</td>
			<td style="width:15.82%">
			<p>43,2</p>
			</td>
			<td style="width:28.14%">
			<p>M (Meduim)</p>
			</td>
			<td style="width:29.06%">
			<p>Средний</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>18&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>165 - 178</p>
			</td>
			<td style="width:15.82%">
			<p>45,7</p>
			</td>
			<td style="width:28.14%">
			<p>M (Meduim)</p>
			</td>
			<td style="width:29.06%">
			<p>Средний</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>19&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>170 - 180</p>
			</td>
			<td style="width:15.82%">
			<p>48,3</p>
			</td>
			<td style="width:28.14%">
			<p>L (Large)</p>
			</td>
			<td style="width:29.06%">
			<p>Большой</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>20&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>178 - 185</p>
			</td>
			<td style="width:15.82%">
			<p>50,8</p>
			</td>
			<td style="width:28.14%">
			<p>L (Large)</p>
			</td>
			<td style="width:29.06%">
			<p>Большой</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>21&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>180 - 190</p>
			</td>
			<td style="width:15.82%">
			<p>53,3</p>
			</td>
			<td style="width:28.14%">
			<p>XL (XLarge)</p>
			</td>
			<td style="width:29.06%">
			<p>Очень большой</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>22&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>185 - 195</p>
			</td>
			<td style="width:15.82%">
			<p>55,9</p>
			</td>
			<td style="width:28.14%">
			<p>XL (XLarge)</p>
			</td>
			<td style="width:29.06%">
			<p>Очень большой</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>23&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>190 - 200</p>
			</td>
			<td style="width:15.82%">
			<p>58,4</p>
			</td>
			<td style="width:28.14%">
			<p>XXL (XLarge)</p>
			</td>
			<td style="width:29.06%">
			<p>Максимальный</p>
			</td>
		</tr>
		<tr>
			<td style="width:15.68%">
			<p>24&quot;</p>
			</td>
			<td style="width:11.32%">
			<p>195 - 210</p>
			</td>
			<td style="width:15.82%">
			<p>61</p>
			</td>
			<td style="width:28.14%">
			<p>XXL (XLarge)</p>
			</td>
			<td style="width:29.06%">
			<p>Максимальный</p>
			</td>
		</tr>
	</tbody>
</table>

<p>*Таблицы размеров носят рекомендательный характер.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
