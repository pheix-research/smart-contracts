<h1><strong>Прогулка от Коктебеля до Старого Крыма (Тропа Волошина). 22.07.2016</strong></h1>

<p>Находясь &nbsp;в Феодосии я решил непременно прогуляться от Коктебеля до Старого Крыма. Прогулка была запланирована с раннего утра, чтобы пройти этот маршрут до жары.</p>

<p>Около шести утра тихо собрал рюкзак: взял две полторашки воды, краски и картон для живописи, навигатор, крем от загара;&nbsp; надел по два носка на каждую ногу, чтобы не стереть конечности до костей и вышел из дому.</p>

<p>Маршрут по которому я планировал прогуляться частично является заброшенной старой земской дорогой по которой раньше реально ездили от Коктебеля до Старого Крыма. В народе этот маршрут именуют тропой Грина, по которой писатель &nbsp;Александр Грин ходил тусоваться в Коктебель к поэту Максимильяну Волошину.</p>

<p>Вообще кто только по этой тропе не ходил...</p>

<p>&nbsp;Пройду и я, подумал я - и пошел!!!</p>

<p>От Феодосии Коктебеля я добрался на автобусе, отходящем около 7 утра от центрального автовокзала Феодосии за смешные по российским меркам деньги (около 45 рублей). В Коктебеле я был уже в половине восьмого утра, зашел в магазин - купил мороженного, ободрал куст шелковицы, стоящий у дороги, закусил мороженное ягодой, и направился в Старый Крым.</p>

<p>&nbsp;</p>

<h2>Коктебель</h2>

<p>Несмотря на раннее утро, солнце уже хорошо припекало. Коктебель оказался менее раздолбанным, чем Феодосия - по крайней мере, тротуары вполне проходимы без спотыканий об вздыбленную плитку и поднятые корнями деревьев пласты еще советского асфальта.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_01.jpg" style="height:600px; width:800px" /></p>

<p>Поселок окружен горами и сам по себе показался мне небольшим, ограниченным заборами. Вглубь поселка я не проходил, так как этого не было в моих планах.</p>

<h2>&nbsp;</h2>

<h2>Тропа Волошина</h2>

<p>При выходе из Коктебеля, я сделал панорамное фото поселка. Фото оказалось немного засвеченным - все-таки надо возвратиться мне к нормальным фотодевайсам. В этом посте все фото сделаны на планшет Megafon Login 3 - штука удобная и как навигатор и как фотоаппарат с джипиэс-метками, но качество фото, увы, оставляет желать лучшего. Так что, либо придется возвращаться к зеркалке и потом наносить фото на карту с помощью программ - синхронизаторов с джипиэс трэком типа GeoSetter или покупать более совершенный фотоаппарат с gps-метками - без них [gps меток] в наше время уже совсем не интересно.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_02.jpg" style="height:261px; width:800px" /></p>

<p>Двигаясь по дороге, пронизывающей Коктебель, я свернул &nbsp;на тропу и довольно быстро подошел к подножию потухшего палеовулкана Карадаг. Но вскоре выяснилось что тропа по которой я иду вела в Карадагский заповедник, куда шагать я не планировал, так что пришлось по азимуту выходить на нужное направление.</p>

<p>Пройдя немного по высохшей колючей траве, потом немного по шоссе, я нашел фарватер среди колючих зарослей и огороженных заборами виноградников и вылез на грунтовку, идущую вдоль виноградных плантаций.&nbsp; Дорожка привела меня к засохшему болоту переходящему в озерцо, я сделал пару фото пресного водоема, кишащего лягушками.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_03.jpg" style="height:239px; width:800px" /></p>

<p>От озерца полузаросшими тропками я вышел к заброшенному домику.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_04.jpg" style="height:599px; width:800px" /></p>

<p>От домика дорога должна была спускаться в овраг и далее идти через виноградник, но она заросла, и я шел напрямки, ловя своими матерчатыми кроссовками всевозможные колючки, репеи и прочие закорючести, чем богаты местные просторы.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_05.jpg" style="height:599px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_06.jpg" style="height:599px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_07.jpg" style="height:404px; width:800px" />На следующем фото справа гора с острыми краями и есть тот самый Карадаг.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_08.jpg" style="height:249px; width:800px" /></p>

<p>Через некоторое время я вышел на дорогу, идущую между виноградниками и спускающуюся в Армутлукскую долину.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_09.jpg" style="height:599px; width:800px" /></p>

<p>Вот я уже шагаю по Армутлукской долине, слева от меня мертвый поселок.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_10.jpg" style="height:279px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_11.jpg" style="height:267px; width:800px" /></p>

<p>Через некоторое время я вышел к озеру Армутлук.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_12.jpg" style="height:495px; width:800px" /></p>

<p>И прошел по гребню с южной стороны озера. Гребень состоит из осадочных пород, скрепленных глиной.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_13.jpg" style="height:324px; width:800px" /></p>

<p>С юго восточной стороны имеется небольшое хозяйство.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_14.jpg" style="height:599px; width:800px" /></p>

<p>После озера начинается лесная зона, в которой можно будет частично спрятаться от солнца.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_15.jpg" style="height:599px; width:800px" /></p>

<p>Во время дождей дороги превращаются в реки, которые иногда выходят из берегов и переливаются в лес, оставляя после себя множество русел.&nbsp; Я свернул с дороги и расположился в одном из русел на обеденный перекус.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_16.jpg" style="height:599px; width:800px" /></p>

<p>На лесном участке пути, в тени водятся лосиные вши - мухи с жестким панцирем на подобии как у &nbsp;клеща. Твари не особо приятные, но не сравнятся с нашими комарами.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_17.jpg" style="height:599px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_18.jpg" style="height:599px; width:800px" /></p>

<p>Периодически дорога выходила на открытые участки, откуда просматривалось все зеленое царство лесов.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_19.jpg" style="height:599px; width:800px" /></p>

<p>Вот он, зеленый океан - в противовес каменной пустыне предыдущего участка пути.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_20.jpg" style="height:349px; width:800px" /></p>

<p>Вскоре немного и подъем закончился, дальше дорога идет на спуск.</p>

<p>Лес здесь более высокий, мне встретились даже грибы.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_21.jpg" style="height:599px; width:800px" /></p>

<p>Через некоторое время я вышел на поляну с обычной травой, и, пользуясь случаем, присел отдохнуть &nbsp;на травку без шипов и колючек, попил воды и погрыз сникерс.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_22.jpg" style="height:367px; width:800px" /></p>

<p>Дорога спускалась к Старому Крыму</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_23.jpg" style="height:371px; width:800px" /></p>

<p>Дойдя до этой точки я неожиданно вспомнил что взял с собой холст с красками, и тут же огорчился, так как самые красивые места, как оказалось, были в начале пути. Тем не менее , возвращаться с чистым холстом не мой стиль. Решил я сделать этюд со Старого Крыма и присмотрел местечко с хорошим видом под старым деревом, где и расположился на этюды</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_24.jpg" style="height:599px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_25.jpg" style="height:600px; width:800px" /></p>

<p>Спустя полтора часа я выдал такой шедевр. В качестве эксперимента я не использовал белила, поэтому пейзаж получился фовистическим, перенасыщенным ярким красками.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_26.jpg" style="height:599px; width:800px" /></p>

<p>Пока сидел под деревом, я успел поймать клеща, ползущего по ноге, закусив его и выплюнув - направился в Старый Крым.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_27.jpg" style="height:599px; width:800px" /></p>

<p><em>По дороге встретил маковый цветок :-)</em></p>

<h2>Старый Крым</h2>

<p>Перед поселкам п пути мне встретилось две речки-ручейка.</p>

<p>Река Монастырская, впадающая в Старо крымское Водохранилище в это время года &nbsp;легко перешагивается по камешкам.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_28.jpg" style="height:599px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_29.jpg" style="height:599px; width:800px" /></p>

<p><em>Живописная дорога вдоль реки.</em></p>

<p>Еще одна речка - Чуруксу, также&nbsp; впадает в Старокрымское водохранилище &ndash; еще уже и мельче предыдущей.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_30.jpg" style="height:599px; width:800px" /></p>

<p>Улица, подходившая к реке оказалось сильно размыта водой во время дождей, которые прошли накануне. По испещрённой метровыми каньонами дороге обычные автомобили вряд ли проедут. Размыв дороги открыл немного истории: обнажились слои старой брусчатки.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_31.jpg" style="height:599px; width:800px" /></p>

<p>В Старом Крыму я непременно пожелал увидеть мечеть Хана Узбека (Постр. в 1314 г. мечеть сохранилась) и развалины Медресе (Постр. в 1332 г.) о существовании которых повествовали повстречавшиеся мне надписи на коричневых дорожных знаках. Кто и когда разрушил Медресе я так и не узнал &ndash; возможно,<a name="_GoBack"></a> она обрушилась со временем от ветхости.</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_32.jpg" style="height:599px; width:800px" /></p>

<p>Вот я уже в Медресе</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_33.jpg" style="height:599px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_34.jpg" style="height:599px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_35.jpg" style="height:599px; width:800px" /></p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_36.jpg" style="height:599px; width:800px" /></p>

<p>До дома Грина я не пошел, так как время уже поджимало.</p>

<p>Недалеко от Старого Крыма есть еще одна достопримечательность - армянский монастырь Сурб-Хач, который до недавнего времени, если верить рассказам встретившегося мне местного жителя, был местом адского отдыха и суровых возлияний выходного дня. Монастырь я не посетил, поэтому ничего про него сказать не могу.</p>

<p>В целом поселок мне показался тихим и спокойным,&nbsp; в былые времена он пользовался славой у творческой интеллигенции, там проживали или гостили А. Ахматова, К. Паустовский и др.</p>

<p>Да, кстати Старый Крым (Эски-Кырым) был когда-то столицей Крымского ханства.</p>

<p>Это было заключительное пешее путешествие в моей поездки в Крым в 2016 г.</p>

<p>Походил хорошо &ndash; кроссовки затер до дыр :-)</p>

<p>&nbsp;</p>

<p><img alt="Путешествие по тропе Грина из Коктебеля до Старого Крыма" src="http://drtg.ru/images/systemgallery/upload/1473373905_37.jpg" style="height:423px; width:800px" /></p>

<h2>&nbsp;</h2>

<h2>GPS трэк маршрута Тропа Волошина</h2>

<p>Скачать трэк: <a href="https://yadi.sk/d/C5gRFYFAvYzo9">https://yadi.sk/d/C5gRFYFAvYzo9</a></p>

<p>&nbsp;</p>

<p><iframe frameborder="0" height="405" scrolling="no" src="https://www.strava.com/activities/649555762/embed/2e7d0fb1536fcb42f23f2eecd3dab8d6dca551bc" width="800"></iframe></p>
