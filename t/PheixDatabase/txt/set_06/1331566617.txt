<p>
	<b>Классификация нормативных нагрузок и воздействий. Сочетания расчетных нагрузок</b></p>
<p>
	По продолжительности действия нормативные нагрузки и воздействия, устанавливаемые СНиП, делятся на постоянные и временные (длительные, кратковременные и особые).</p>
<p>
	К постоянным нагрузкам и воздействиям относятся собственный вес конструкций, воздействие предварительного напряжения и др.</p>
<p>
	К длительным временным нагрузкам и воздействиям относятся вес и длительное температурное воздействие стационарного оборудования, давление жидкостей, газов и сыпучих материалов в емкостях и трубопроводах, нагрузки на перекрытия складских помещений, холодильников, зерно- и книгохранилищ, архивов, библиотек и подобных зданий.</p>
<p>
	К кратковременным относятся нагрузки от подвижного подъемно-транспортного оборудования (кранов, тельферов и др.), нагрузки на перекрытия жилых и общественных зданий от веса людей, мебели и подобного легкого оборудования, нагрузка от веса людей, деталей и ремонтных материалов в зонах обслуживания оборудования (проходах, проездах и других свободных от оборудования участках), атмосферные нагрузки (ветровая, снеговая, гололедная, волновая, ледовая) и воздействия (температурные, влажностные и т.п.), монтажные и другие нагрузки и воздействия.</p>
<p>
	К особым относятся сейсмические воздействия, аварийные и воздействия от просадок основания вследствие коренного изменения структуры грунта.</p>
<p>
	Обычно сооружение подвергается одновременному действию нескольких различных нагрузок, поэтому в расчете необходимо учитывать возможные (для отдельных элементов, конструкций и соединений или всего сооружения в целом) неблагоприятные сочетания нагрузок и воздействий. Различают следующие сочетания нагрузок:</p>
<ol>
	<li>
		основные, включающие постоянные нагрузки, длительные временные и одну, наиболее существенную, кратковременную нагрузку (например, снеговую для покрытий, ветровую для высотных сооружений);</li>
	<li>
		основные, включающие постоянные, длительные временные и все кратковременные нагрузки, если их две и более;</li>
	<li>
		oсобые, состоящие из постоянных, длительных временных, возможных кратковременных и одной из особых нагрузок.</li>
</ol>
<p>
	Вертикальные и горизонтальные нагрузки от одного или двух мостовых кранов рассматривают как одну кратковременную нагрузку. Совместное действие снеговой нагрузки с одним или двумя мостовыми кранами тяжелого или весьма тяжелого режимов работы учитывают в первом основном сочетании, с кранами легкого или среднего режимов &mdash; во втором.</p>
<p>
	Вероятность указанных сочетаний нагрузок учитывают <em>коэффициентами сочетания n</em>. В первом случае все нагрузки учитывают полностью (n=1); во втором полностью учитывают постоянные и длительные временные нагрузки, а для кратковременных вводят коэффициент n=0,9. Особые сочетания составляют в соответствии с нормами проектирования в сейсмических районах или специальными указаниями. В этом случае кратковременные нагрузки имеют n=0,8. Коэффициенты сочетания обычно вводят в виде множителя к усилиям от расчетных нагрузок.</p>
<p>
	По характеру воздействия различают нагрузки статические и динамические. К статическим относят нагрузки, не изменяющиеся со временем по значению и направлению (например, собственный вес конструкции) или меняющиеся настолько медленно, что вызываемые ими силы инерции пренебрежимо малы (например, снеговая нагрузка). Динамические нагрузки в отличие от статических меняют свое значение, положение или направление в короткие промежутки времени (например, движущаяся нагрузка, ударная нагрузка, сейсмические воздействия), вызывая значительные силы инерции.</p>
