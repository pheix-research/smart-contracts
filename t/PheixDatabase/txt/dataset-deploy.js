var gasqty     = eth.getBlock('latest').gasLimit;
var tab_prefx  = "tst_table";
var tab_start  = !(typeof _TAB_S === 'undefined') ? _TAB_S : 0;
var tab_end    = !(typeof _TAB_E === 'undefined') ? _TAB_E : 0;
var hashes     = new Array;
var debug_log  = true;
var data_len   = 3072;
var trtimeout  = 3600;
var id_disp    = 100000;
var no_el_code = 1;
var gas_cap    = !(typeof _GAS_CAP === 'undefined') ? _GAS_CAP : 25; // percents
var nodepass   = "node0";
var from       = {from: personal.listAccounts[0]};
var sendtx     = {from: personal.listAccounts[0], gas: gasqty};
var initial_id = eth.getBlock(eth.getTransaction(storage.tx).blockNumber).timestamp;

var statistic = {
    gaslimit  : gasqty,
    totalgas  : 0,
    totaltabs : 0,
    totalrows : 0,
    totaltime : 0,
    totallen  : 0,
    maxgas    : { tab_name:"", row:0, rowid:0, gas:0  },
    fattab    : { tab_name:"", rows:0, bytes:0 }
};

function drop_table(tabname) {
    if (storage.tableExists.call(tabname, from)) {
        var hashes = new Array;
        var ids    = new Array;

        for(var i = 0; i < storage.countRows.call(tabname, from); i++) {
            var rowid = storage.getIdByIndex.call(tabname, i, from);

            if (rowid > 0) {
                ids.push(rowid);
            }
        }

        personal.unlockAccount(personal.listAccounts[0], nodepass);

        if (ids.length > 0) {
            for(var j = 0; j < ids.length; j++) {
                hashes.push(storage.remove.sendTransaction(tabname, ids[j], sendtx));
            }
        }
        else {
            hashes.push(storage.remove.sendTransaction(tabname, 0, sendtx));
            console.log("\tdrop blank record for " + tabname + ": " + hashes[0]);
        }

        wait_transactions_byhash_array("drop table " + tabname, hashes, debug_log);

        if (storage.tableExists.call(tabname, from)) {
            console.log("\tFAILURE: drop table " + tabname);

            return false;
        }
    }

    return true;
}

function drop_all_tables() {
    var rc = true;
    var existed_tables_num = storage.countTables.call(from);

    if (existed_tables_num > 0) {
        console.log("DROPDB: dropping " + existed_tables_num + " existed tables");
        var existed_tables = new Array;

        for(var i = 0; i < existed_tables_num; i++) {
            var tabname = storage.getNameByIndex.call(i, from);

            if (storage.tableExists.call(tabname, from)) {
                existed_tables.push(tabname);
            }
        }

        for(var i = 0; i < existed_tables.length; i++) {
            if (!drop_table(existed_tables[i])) {
                rc = false;
            }
        }
    }

    return rc;
}

function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc = true;
    var index = 0;
    var hashes = tr_hash_arr;

    var failed_hashes = new Array;

    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length );
    while(hashes.length > 0) {
        var updated_hashes = new Array;

        for(var i = 0; i < hashes.length; i++) {
            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                var receipt = eth.getTransactionReceipt(hashes[i]);

                if (!(receipt.status === "0x1")) {
                    failed_hashes.push({hash:hashes[i], status:receipt.status});
                }
            }
        }

        hashes = updated_hashes;

        if (index == trtimeout) {
            rc = false;

            break;
        }
        else {
            if (logging && ( index > 0 && index % 15 == 0 )) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length - failed_hashes.length) + "/" + tr_hash_arr.length);
            }

            index++;
            storage.sleep(1);
        }

        if (failed_hashes.length) {
            rc = false;

            for(var i = 0; i < failed_hashes.length; i++) {
                console.log("\tFAILURE: " + failed_hashes[i].hash + " status=" + failed_hashes[i].status);
            }
        }
    }

    return rc;
}

function deploy_dataset(logging) {
    var hashes       = new Array;
    var tabindex     = 0;
    var compression  = false;

    console.log("***HINT: you could set _GAS_CAP to increase estimateGas value for given % (current: +" + gas_cap + "%)");
    console.log("***HINT: you could set _TAB_S and _TAB_E for iters over specific tabs");

    statistic.totaltabs = (tab_end && tab_end < datasets.length) ? tab_end : datasets.length;
    console.log("***INFO: start job for " + (statistic.totaltabs-tab_start) + " tables. Here we go!");

    for(var i = tab_start; i < statistic.totaltabs; i++) {
        var tab_name   = tab_prefx + "_" + i;
        var data_chk   = new String;
        var cdata_str  = new String;
        var cmpcnt     = 0;

        personal.unlockAccount(personal.listAccounts[0], nodepass);

        wait_transactions_byhash_array("create table <" + tab_name + ">", [storage.newTable.sendTransaction(tab_name, "# id; data; compression", false, sendtx)], logging);

        if (!(typeof LZW === 'undefined') && LZW != null) {
            if (!(typeof LZW.compress === 'undefined')) {
                compression = true;
            }
        }

        cdata_str = datasets[i].content;
        var rows  = Math.floor(cdata_str.length / data_len) + 1;

        if (statistic.fattab.bytes < cdata_str.length) {
            statistic.fattab.file_name = datasets[i].file;
            statistic.fattab.tab_name  = tab_name;
            statistic.fattab.rows      = rows;
            statistic.fattab.bytes     = cdata_str.length;
        }

        if (logging) {
            console.log("\t" + i + ": insert text from " + datasets[i].file + ": " + cdata_str.length + " bytes to <" + tab_name + "> with " + rows + " rows");
        }

        for (var j = 0; j < rows; j++) {
            statistic.totalrows++;

            var upd_gas = gasqty;
            var low_gas = false;
            var rowid   = (initial_id < 1 ? (j + 1) : initial_id + i * id_disp + j);
            var data    = "";
            var start   = j*data_len;
            var end     = j*data_len + (data_len);

            if (end > cdata_str.length) {
                 end = cdata_str.length;
            }

            for (var k = start; k < end; k++) {
                data = ( data ? data : "" ) + ( cdata_str[k] ? cdata_str[k] : "?" );
                if ( !cdata_str[k] ) {
                    console.log("\t    corrupted char at pos=" + k + ", replaced to '?'");
                }

                statistic.totallen++;
            }

            data_chk = ( data_chk ? data_chk : "" ) + data;

            if (compression) {
                var compressed = LZW.compress(LZW.encode_utf8(data));

                data   =  compressed;
                cmpcnt += data.length;
            }

            personal.unlockAccount( personal.listAccounts[0], nodepass );

            var signature = personal.sign(web3.fromUtf8(data), personal.listAccounts[0], nodepass);
            var need_gas  = storage.insert.estimateGas(tab_name, web3.fromUtf8(data), rowid, compression, signature, from);

            if (need_gas > upd_gas / 10) {
                upd_gas = need_gas + Math.floor((need_gas/100) * gas_cap);

                if (upd_gas > gasqty) {
                    upd_gas = gasqty;
                }
            }
            else {
                low_gas = true;
                upd_gas = gasqty - need_gas;
            }

            if (need_gas && upd_gas) {
                var trx_hash  = storage.insert.sendTransaction(tab_name, web3.fromUtf8(data), rowid, compression, signature, {from: personal.listAccounts[0], gas: upd_gas});
                var stats_gas = low_gas ? need_gas : upd_gas;

                hashes.push(trx_hash);

                if (logging) {
                    console.log("\t    " + trx_hash + ": insert row " + (j+1) + "/" + rows + " of <" + tab_name + "> tab {start=" + start + ", end=" + end + ", len="+cmpcnt+"/"+data_chk.length+"}: " + ((upd_gas/1000000).toFixed(2)) + " (" + ((need_gas/1000000).toFixed(2)) + ") Mgas, ratio=" + (compression ? (100-Math.round(100*cmpcnt/data_chk.length)) : 0) + "%");
                }

                statistic.totalgas += stats_gas;

                if (statistic.maxgas.gas < stats_gas) {
                    statistic.maxgas.tab_name  = tab_name;
                    statistic.maxgas.file_name = datasets[i].file;
                    statistic.maxgas.row       = (j + 1);
                    statistic.maxgas.rowid     = rowid;
                    statistic.maxgas.gas       = stats_gas;
                }
            }
            else {
                if (logging) {
                    console.log("\tFAILURE: gas error for row "+ j +" of tab <" + tab_name + ">, data len=" + length);
                }
            }
        }

        if (!(data_chk === cdata_str)) {
            console.log("\t\tFAILURE: data is invalid for <" + tab_name + "> with " + rows + " rows: " + data_chk.length + "/" + cdata_str.length);
        }

        tabindex++;
    }
    var res = wait_transactions_byhash_array("deploy_dataset via " + hashes.length + " transactions", hashes, logging);

    if ( !(storage.countTables.call(from) == tabindex) || !res ) {
        console.log("\tFAILURE: deploy_dataset, countTables=" + storage.countTables.call(from));

        return false;
    }

    return true;
}

function check_dataset( logging ) {
    var check_all_datasets = true;
    var compression        = false;
    for (var i = tab_start; i < statistic.totaltabs; i++) {
        personal.unlockAccount( personal.listAccounts[0], nodepass );

        var tab_name = tab_prefx + "_" + i;
        var data     = new Array;

        if (storage.tableExists.call(tab_name, from)) {

            for (var j = 0; j < storage.countRows.call(tab_name, from); j++) {
                var id = storage.getIdByIndex.call(tab_name, j, from);

                if ( id > -1 ) {
                    let [rowdata, rowsignature] = storage.select.call(tab_name, id, from);
                    data.push(web3.toUtf8(rowdata));
                }
                else {
                    if (logging) {
                        console.log("\tFAILURE: invalid id at index=" + j + " at <" + tab_name + ">");
                    }
                }
            }

            if (data.length == storage.countRows.call(tab_name, from)) {
                var _datasets = new Array;
                cdata_str     = data.join('');

                if (!(typeof LZW === 'undefined') && LZW != null) {
                    if (!(typeof LZW.decompress === 'undefined') ) {
                        compression = true;
                    }
                }

                if (compression) {
                    //console.log(data.length + ":" + data[0].length + "," + data[1].length + "," + data[2].length);
                    for(var row=0; row<data.length; row++) {
                        var _ds = LZW.decode_utf8(LZW.decompress(data[row]));
                        //console.log("uncompressed " + row + ": " + ( _ds == null ? 0 : _ds.length )) + " bytes";
                        _datasets.push(_ds);
                    }

                    var dataset = _datasets.join('');

                    if ((dataset === datasets[i].content)) {
                        ;// console.log( "\tcheck dataset[" + i + "] complete" );
                    } else {
                        //console.log( "\tFAILURE: " + cdata_arr.length + "/" + cdata_arr_et.length );
                        console.log( "\tFAILURE: check dataset[" + i + "] fails, len " + ( dataset ? dataset.length : 0 ) +  "/" + datasets[i].length + " (uncompressed/etalon)" );
                        check_all_datasets = false;
                    }
                } else {
                    if (cdata_str.length == datasets[i].length) {
                        var dataset = cdata_str;

                        if ((dataset === datasets[i].content)) {
                            ;// console.log( "\tcheck dataset[" + i + "] complete" );
                        }
                        else {
                            console.log( "\tFAILURE: check dataset[" + i + "] fails, len " + ( dataset ? dataset.length : 0 ) +  "/" + datasets[i].length + " (blockchain/etalon)" );
                            check_all_datasets = false;
                        }
                    }
                }
            }
        }
    }

    if (check_all_datasets) {
        console.log( "\tALL TESTS COMPLETED! Datasets (" + (statistic.totaltabs-tab_start) + ") are successfully checked!" );
    }
    else {
        console.log( "\tTESTS FAILED! Datasets check failure." );
    }
}

// print statistics
/*
gaslimit  : 0,
totalgas  : 0,
totaltabs : 0,
totalrows : 0,
totaltime : 0,
totallen  : 0,
maxgas    : { tab_name:"", rowid:0, gas:0  },
fattab    : { tab_name:"", rows:0, bytes:0 }
*/
function print_stats() {
    console.log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
    console.log("Gas limit      : " + (statistic.gaslimit/1000000).toFixed(2) + " Mgas");
    console.log("Total gas      : " + (statistic.totalgas/1000000).toFixed(2) + " Mgas");
    console.log("Total tables   : " + statistic.totaltabs );
    console.log("Total rows     : " + statistic.totalrows );
    console.log("Total time     : " + statistic.totaltime/1000 + " sec" );
    console.log("Total bytes    : " + statistic.totallen );
    console.log("Max gas at     : " + (statistic.maxgas.gas/1000000).toFixed(2) + " Mgas <" + statistic.maxgas.tab_name + "> (" + statistic.maxgas.file_name + ") row " + statistic.maxgas.row + " [rowid=" + statistic.maxgas.rowid + "]");
    console.log("Fat table      : " + statistic.fattab.bytes + " bytes <" + statistic.fattab.tab_name + "> (" + statistic.fattab.file_name + ") rows=" + statistic.fattab.rows );

    if ( !(typeof debug.metrics === 'undefined') ) {
        if ( !(typeof debug.metrics.chain === 'undefined') ) {
            console.log("Max execution  : " + debug.metrics(false).chain.execution.Maximum );
            console.log("Max inserts    : " + debug.metrics(false).chain.inserts.Maximum );
            console.log("Max validation : " + debug.metrics(false).chain.validation.Maximum );
            console.log("Max write      : " + debug.metrics(false).chain.write.Maximum );
        }
    }
}

personal.unlockAccount(personal.listAccounts[0], nodepass);
drop_all_tables(debug_log);

var start_time  = Date.now();
var deploy_flag = deploy_dataset(debug_log);

statistic.totaltime = Date.now() - start_time;

if (deploy_flag) {
    check_dataset(debug_log);
}

print_stats();
