var nodepass  = "node0";
var trtimeout = 600;
var tr_hash   = "0x0";
var gasqty    = 3000000;
var debug_log = true;
var test_no1  = true;
var test_no2  = true;
var test_no3  = true;
var test_no4  = true;
var t3loop    = 5;
var from      = {from: personal.listAccounts[0]};
var sendtx    = {from: personal.listAccounts[0], gas: gasqty};
var fields    = "# domain;ip;useragent;resolution;referer;country";

// wait for transactions by hash-array
function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc = true;
    var index = 0;
    var hashes = tr_hash_arr;

    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length );

    while(hashes.length > 0) {
        var updated_hashes = new Array;

        for(var i = 0; i < hashes.length; i++) {
            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                if (!(eth.getTransactionReceipt(hashes[i]).status === "0x1")) {
                    rc = false;
                    console.log("\tFAILURE: transaction status=" + eth.getTransactionReceipt(hashes[i]).status);
                }
            }
        }

        hashes = updated_hashes;

        if (index == trtimeout) {
            rc = false;

            break;
        }
        else {
            if (logging && (index > 0 && index % 15 == 0)) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length) + "/" + tr_hash_arr.length);
            }

            index++;
            storage.sleep(1);
        }
    }

    return rc;
}

// create tables
function create_all(comp) {
    var hashes = new Array;

    hashes.push(
        storage.newTable.sendTransaction("table_1", fields, comp, sendtx),
        storage.newTable.sendTransaction("table_2", fields, comp, sendtx),
        storage.newTable.sendTransaction("table_3", fields, comp, sendtx)
    );

    return wait_transactions_byhash_array("create tables", hashes, debug_log);
}

// application level implemented init
function init(comp) {
    var hashes = new Array;

    if (create_all(comp)) {
        console.log("\tinit database: application level");

        var rows = [
            "pheix.org|127.0.0.1|Mozilla|640*480|index.html|RU",
            "twitter.com|127.0.0.11|IE|1900*1280|index2.html|US",
            "ya.ru|127.0.0.12|Opera|1024*768|index3.html|BY",
            "narkhov.pro|127.0.0.111|Safari|100*200|index4.html|UA",
            "foo.bar|127.0.0.254|Netscape|1152*778|index5.html|RO"
        ];

        hashes.push(
            storage.insert.sendTransaction("table_1", web3.fromUtf8(rows[0]), 1, comp, personal.sign(web3.fromUtf8(rows[0]), personal.listAccounts[0], nodepass), sendtx),
            storage.insert.sendTransaction("table_1", web3.fromUtf8(rows[1]), 2, comp, personal.sign(web3.fromUtf8(rows[1]), personal.listAccounts[0], nodepass), sendtx),
            storage.insert.sendTransaction("table_2", web3.fromUtf8(rows[2]), 1, comp, personal.sign(web3.fromUtf8(rows[2]), personal.listAccounts[0], nodepass), sendtx),
            storage.insert.sendTransaction("table_2", web3.fromUtf8(rows[3]), 2, comp, personal.sign(web3.fromUtf8(rows[3]), personal.listAccounts[0], nodepass), sendtx),
            storage.insert.sendTransaction("table_3", web3.fromUtf8(rows[4]), 1, comp, personal.sign(web3.fromUtf8(rows[4]), personal.listAccounts[0], nodepass), sendtx)
        );

        return wait_transactions_byhash_array("init", hashes, debug_log);
    }

    return false;
}

// application level reset compression
function reset_compression( comp ) {
    console.log("\treset compression to " + comp + ": application level");

    var tabs = [ "table_1", "table_2", "table_3" ];

    for(var i = 0; i < tabs.length; i++) {
        tnam = tabs[i];

        if (storage.tableExists.call(tnam, from)) {
            if (storage.isTabCompressed.call(tnam, from)) {
                var rc = wait_transactions_byhash_array(
                    "reset comp " + tnam,
                    [
                        storage.updateTabCompression.sendTransaction(
                           tnam,
                           comp,
                           sendtx
                        )
                    ],
                    debug_log
                );

                if (!rc) {
                    console.log( "\tFAILURE: reset comp " + tnam);

                    return false;
                }
            }

            var rows = storage.countRows.call(tnam, from);
            var hashes = new Array;

            for(var j = 0; j < rows; j++) {
                var rowid = storage.getIdByIndex.call(tnam, j, from);

                if (rowid > 0) {
                    if (storage.isRecCompressed.call(tnam, rowid, from)) {
                        hashes.push(
                            storage.updateRecCompression.sendTransaction(
                                tnam,
                                rowid,
                                comp,
                                sendtx
                            )
                        );
                    }
                }
            }

            var rc = wait_transactions_byhash_array(
                "reset comp " + tnam + " row " + rowid,
                hashes,
                debug_log
            );

            if (!rc) {
                console.log(
                    "\tFAILURE: reset comp " +
                    tnam + " row " + rowid
                );

                return false;
            }
        }
    }

    return true;
}

// drop table
function drop_table(tabname) {
    if (storage.tableExists.call(tabname, from)) {
        var hashes = new Array;
        var ids = new Array;

        for(var i = 0; i < storage.countRows.call(tabname, from); i++) {
            var rowid = storage.getIdByIndex.call(tabname, i, from);

            if (rowid > 0) {
                ids.push(rowid);
            }
        }

        if (ids.length > 0) {
            for(var j = 0; j < ids.length; j++) {
                hashes.push(storage.remove.sendTransaction(tabname, ids[j], sendtx));
            }
        }
        else {
            hashes.push(storage.remove.sendTransaction(tabname, 0, sendtx));
            console.log("\tdrop blank record for " + tabname + ": " + hashes[0]);
        }

        wait_transactions_byhash_array("drop table " + tabname, hashes, debug_log);

        if (storage.tableExists.call(tabname, from)) {
            console.log("\tFAILURE: drop table " + tabname);

            return false;
        }
    }

    return true;
}

// drop all existed tables
function drop_all() {
    var rc = true;
    var existed_tables_num = storage.countTables.call(from);

    if (existed_tables_num > 0) {
        console.log("DROPDB: dropping " + existed_tables_num + " existed tables");
        var existed_tables = new Array;

        for(var i = 0; i < existed_tables_num; i++) {
            var tabname = storage.getNameByIndex.call(i, from);

            if (storage.tableExists.call(tabname, from)) {
                existed_tables.push(tabname);
            }
        }

        for(var i = 0; i < existed_tables.length; i++) {
            if (!drop_table(existed_tables[i])) {
                rc = false;
            }
        }
    }

    return rc;
}

/* test #1: init, get and set fields record */
{
    console.log("TEST#1: init, get and set fields record");

    personal.unlockAccount(personal.listAccounts[0], nodepass);
    drop_all();

    if (init(false)) {
        var flds;
        var tabs = storage.countTables.call(from);
    
        for(var i = 0; i < tabs; i++) {
            var tnam = storage.getNameByIndex.call(i, from);

            flds = storage.getFields.call(tnam, from);

            if (flds !== fields) {
                test_no1 = false;
                console.log("\tFAILURE: get fields for tab <" + tnam + ">");
            }
            else {
                var uf = "# " + tnam + ";url;ipaddr;ua;screen;page;countrycode";
                var rc = wait_transactions_byhash_array(
                    "update " + tnam + " fields",
                    [
                        storage.setFields.sendTransaction(
                            tnam,
                            uf,
                            sendtx
                        )
                    ],
                    debug_log
                );

                if (!rc) {
                    console.log("\tFAILURE: update fields for tab <" + tnam + ">");
                    test_no1 = false;

                    break;
                }
                else {
                    flds = storage.getFields.call(tnam, from);
                    if (flds !== uf) {
                        console.log("\tFAILURE: get fields after set for tab <" + tnam + ">");
                        test_no1 = false;

                        break;
                    }
                }
            }
        }
    }
    else {
        test_no1 = false;
    }

    if (test_no1) {
        console.log("\tDONE: init, get and set fields record");
    }
    else {
        console.log("\tFAILURE: init, get and set fields record");
    }
}

/* test #2: init and check db with false comp flag */
{
    console.log("TEST#2: init and check db with false comp flag");

    personal.unlockAccount(personal.listAccounts[0], nodepass);

    drop_all();
    init(false);

    var tabs = storage.countTables.call(from);

    for(var i = 0; i < tabs; i++) {
        var tnam = storage.getNameByIndex.call(i, from);

        if ( storage.isTabCompressed.call(tnam, from) ) {
            test_no2 = false;

            break;
        }
        else {
            var rows = storage.countRows.call(tnam, from);

            for( var j = 0; j < rows; j++ ) {
                var rowid = storage.getIdByIndex.call(tnam, j, from);

                if (rowid > 0) {
                    if ( storage.isRecCompressed.call(tnam, rowid, from) ) {
                        test_no2 = false;

                        break;
                    }
                }
            }

            if ( !test_no2 ) {
                break;
            }
        }
    }

    if ( test_no2 ) {
        console.log("\tDONE: init and check db with false comp flag");
    }
    else {
        console.log("\tFAILURE: init and check db with false comp flag");
    }
}

/* test #3: init and check db with true comp flag */
{
    console.log( "TEST#3: init and check db with true comp flag" );

    personal.unlockAccount( personal.listAccounts[0], nodepass );

    drop_all();
    init(true);

    var tabs = storage.countTables.call(from);

    for(var i = 0; i < tabs; i++) {
        var tnam = storage.getNameByIndex.call(i, from);

        if ( !storage.isTabCompressed.call(tnam, from) ) {
            test_no3 = false;

            break;
        }
        else {
            var rows = storage.countRows.call(tnam, from);

            for(var j = 0; j < rows; j++) {
                var rowid = storage.getIdByIndex.call(tnam, j, from);

                if (rowid > 0) {
                    if (!storage.isRecCompressed.call(tnam, rowid, from)) {
                        test_no3 = false;

                        break;
                    }
                }
            }

            if (!test_no3) {
                break;
            }
        }
    }

    if (test_no3) {
        console.log("\tDONE: init and check db with true comp flag");
    }
    else {
        console.log("\tFAILURE: init and check db with true comp flag");
    }
}

/* test #4: init and setter-getter loop */
{
    var rc = false;

    console.log("TEST#4: init and setter-getter loop (" + t3loop + " iters)");

    personal.unlockAccount(personal.listAccounts[0], nodepass);

    drop_all();
    init(false);

    for (tinx = 0; tinx < t3loop; tinx++) {
        var comp   = true;
        /* states */
        var states = [
            {
                "t0" : false,
                "r0" : false,
                "r1" : false
            },
            {
                "t1" : false,
                "r0" : false,
                "r1" : false
            },
            {
                "t2" : false,
                "r0" : false,
            }
        ];

        /* setters test */
        var tabs = storage.countTables.call(from);
        var rti  = Math.floor(Math.random() * (tabs));

        for(var i = 0; i < tabs; i++) {
            var tnam = storage.getNameByIndex.call(i, from);

            /* set comp flag for rand tab */
            if (i == rti) {
                rc = wait_transactions_byhash_array(
                    "update " + tnam + " cmp",
                    [
                        storage.updateTabCompression.sendTransaction(
                            tnam,
                            comp,
                            sendtx
                        )
                    ],
                    debug_log
                );

                if (!rc) {
                    console.log("\tFAILURE: update compression flag for tab <" + tnam + ">");
                    test_no4 = false;
                    break;
                }
                else {
                    var tkey = "t" + i;
                    states[i][tkey] = comp;
                }
            }

            var rows = storage.countRows.call(tnam, from);
            var rtri = Math.floor(Math.random() * (rows));

            for(var j = 0; j < rows; j++) {
                var rowid = storage.getIdByIndex.call(tnam, j, from);

                if (rowid > 0 && j == rtri) {
                    rc = wait_transactions_byhash_array(
                        "update " + tnam + " row " + rowid + " cmp",
                        [
                            storage.updateRecCompression.sendTransaction(
                                tnam,
                                rowid,
                                comp,
                                sendtx
                            )
                        ],
                        debug_log
                    );

                    if (!rc) {
                        console.log("\tFAILURE: update compression flag for " + tnam + " row " + rowid);
                        test_no4 = false;

                        break;
                    }
                    else {
                        var rkey = "r" + j;
                        states[i][rkey] = comp;
                    }
                }
            }

            if (!test_no4) {
                break;
            }
        }

        /* getters test */
        for(var i = 0; i < states.length; i++) {
            var tkey  = "t" + i;
            var scomp = states[i][tkey];
            var tnam  = storage.getNameByIndex.call(i, from);
            var tcomp = storage.isTabCompressed.call(tnam, from);

            if (tcomp != scomp) {
                test_no4 = false;

                break;
            }
            else {
                var rows = storage.countRows.call(tnam, from);

                for(var j = 0; j < rows; j++) {
                    var rowid = storage.getIdByIndex.call(tnam, j, from);

                    if (rowid > 0) {
                        var rkey   = "r" + j;
                        var trcomp = storage.isRecCompressed.call(tnam, rowid, from);
                        var srcomp = states[i][rkey];

                        if (trcomp != srcomp) {
                            test_no4 = false;

                            break;
                        }
                    }
                }

                if (!test_no4) {
                    break;
                }
            }
        }

        if (test_no4) {
            if (reset_compression(false)) {
                var hmi_i = tinx + 1;
                console.log("\tPASS: iteration " + hmi_i);
            }
            else {
                break;
            }
        }
        else {
            break;
        }
    }

    if (test_no4) {
        console.log("\tDONE: init and setter-getter loop");
    }
    else {
        console.log("\tFAILURE: init and setter-getter loop");
    }
}

// summary
if (test_no1 && test_no2 && test_no3 && test_no4) {
    console.log("ALL TESTS COMPLETED!");
}
else {
    console.log("TESTS FAILED!");
}
