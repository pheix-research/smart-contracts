var nodepass  = "node0";
var tab_name  = "tst_table";
var trtimeout = 600;
var db_size   = 100;
var rm_rows   = 25;
var gasqty    = 3000000;
var from      = {from: personal.listAccounts[0]};
var sendtx    = {from: personal.listAccounts[0], gas: gasqty};
var db_in_mem = new Array;
var hashes    = new Array;
var debug_log = true;
var domains   = [
    "google.com",
    "facebook.com",
    "doubleclick.net",
    "google-analytics.com",
    "akamaihd.net",
    "googlesyndication.com",
    "googleapis.com",
    "googleadservices.com",
    "facebook.net",
    "youtube.com",
    "twitter.com",
    "scorecardresearch.com",
    "microsoft.com",
    "ytimg.com",
    "googleusercontent.com",
    "apple.com",
    "msftncsi.com",
    "2mdn.net",
    "googletagservices.com",
    "adnxs.com",
    "yahoo.com",
    "serving-sys.com",
    "akadns.net",
    "bluekai.com",
    "ggpht.com",
    "rubiconproject.com",
    "verisign.com",
    "addthis.com",
    "crashlytics.com",
    "amazonaws.com"
];
var ip_addrs  = [
    "5.135.164.72",
    "212.112.124.163",
    "147.135.206.233",
    "177.234.19.50",
    "31.145.50.34",
    "185.136.151.20",
    "191.239.243.156",
    "202.93.128.98",
    "77.95.132.41",
    "193.117.138.126",
    "182.52.51.10",
    "89.249.65.140",
    "190.214.12.82",
    "91.215.195.143",
    "178.128.88.35",
    "118.172.211.155",
    "87.244.182.181",
    "84.52.69.94",
    "1.2.169.4",
    "43.245.131.205",
    "173.249.51.179",
    "180.180.218.153",
    "91.143.54.254",
    "158.69.243.155",
    "35.185.201.225",
    "82.99.228.90",
    "185.81.99.205",
    "186.215.133.170",
    "31.130.91.37",
    "178.128.21.47"
];
var browsers  = [
    "Mozilla/5.0 (Amiga; U; AmigaOS 1.3; en; rv:1.8.1.19) Gecko/20081204 SeaMonkey/1.1.14",
    "Mozilla/5.0 (AmigaOS; U; AmigaOS 1.3; en-US; rv:1.8.1.21) Gecko/20090303 SeaMonkey/1.1.15",
    "Mozilla/5.0 (AmigaOS; U; AmigaOS 1.3; en; rv:1.8.1.19) Gecko/20081204 SeaMonkey/1.1.14",
    "Mozilla/5.0 (Android 2.2; Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4",
    "Mozilla/5.0 (BeOS; U; BeOS BeBox; fr; rv:1.9) Gecko/2008052906 BonEcho/2.0",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.1) Gecko/20061220 BonEcho/2.0.0.1",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.10) Gecko/20071128 BonEcho/2.0.0.10",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.17) Gecko/20080831 BonEcho/2.0.0.17",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.6) Gecko/20070731 BonEcho/2.0.0.6",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.7) Gecko/20070917 BonEcho/2.0.0.7",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1b2) Gecko/20060901 Firefox/2.0b2",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20051002 Firefox/1.6a1",
    "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20060702 SeaMonkey/1.5a",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.10pre) Gecko/20080112 SeaMonkey/1.1.7pre",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.14) Gecko/20080429 BonEcho/2.0.0.14",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.17) Gecko/20080831 BonEcho/2.0.0.17",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.18) Gecko/20081114 BonEcho/2.0.0.18",
    "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.21pre) Gecko/20090218 BonEcho/2.0.0.21pre",
    "Mozilla/5.0 (Darwin; FreeBSD 5.6; en-GB; rv:1.8.1.17pre) Gecko/20080716 K-Meleon/1.5.0",
    "Mozilla/5.0 (Darwin; FreeBSD 5.6; en-GB; rv:1.9.1b3pre)Gecko/20081211 K-Meleon/1.5.2",
    "Mozilla/5.0 (Future Star Technologies Corp.; Star-Blade OS; x86_64; U; en-US) iNet Browser 4.7",
    "Mozilla/5.0 (Linux 2.4.18-18.7.x i686; U) Opera 6.03 [en]",
    "Mozilla/5.0 (Linux 2.4.18-ltsp-1 i686; U) Opera 6.1 [en]",
    "Mozilla/5.0 (Linux 2.4.19-16mdk i686; U) Opera 6.11 [en]",
    "Mozilla/5.0 (Linux 2.4.21-0.13mdk i686; U) Opera 7.11 [en]",
    "Mozilla/5.0 (Linux X86; U; Debian SID; it; rv:1.9.0.1) Gecko/2008070208 Debian IceWeasel/3.0.1",
    "Mozilla/5.0 (Linux i686 ; U; en; rv:1.8.1) Gecko/20061208 Firefox/2.0.0 Opera 9.70",
    "Mozilla/5.0 (Linux i686; U; en; rv:1.8.1) Gecko/20061208 Firefox/2.0.0",
    "Mozilla/5.0 (Linux i686; U; en; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 Opera 10.51",
    "Mozilla/5.0 (Linux) Gecko Iceweasel (Debian) Mnenhy"
];
var resolut   = [
    "960x640",
    "800x480",
    "1152x768",
    "1024x800",
    "1024x640",
    "1280x1024",
    "1280x960",
    "1440x1024",
    "1600x1024",
    "1600x1200",
    "1920x1080",
    "1920x1200",
    "2048x1152",
    "2048x1536"
];
var pages     = [
    "/index.php",
    "/license.txt",
    "/readme.html",
    "/wp-activate.php",
    "/wp-admin/about.php",
    "/wp-admin/admin-ajax.php",
    "/wp-admin/admin-footer.php",
    "/wp-admin/admin-functions.php",
    "/wp-admin/admin-header.php",
    "/wp-admin/admin-post.php",
    "/wp-admin/admin.php",
    "/wp-admin/async-upload.php",
    "/wp-admin/comment.php",
    "/wp-admin/credits.php",
    "/wp-admin/custom-background.php",
    "/wp-admin/custom-header.php",
    "/wp-admin/edit-comments.php",
    "/wp-admin/edit-form-advanced.php",
    "/wp-admin/edit-form-comment.php",
    "/wp-admin/edit-link-form.php",
    "/wp-admin/edit-tag-form.php",
    "/wp-admin/edit-tags.php",
    "/wp-admin/edit.php",
    "/wp-admin/export.php",
    "/wp-admin/freedoms.php",
    "/wp-admin/gears-manifest.php"
];
var countries = [
    "ES",
    "ET",
    "FI",
    "FJ",
    "FK",
    "FM",
    "FO",
    "FR",
    "FX",
    "GA",
    "GB",
    "GD",
    "GE",
    "GF",
    "GG",
    "GH",
    "GI",
    "GL",
    "GM",
    "GN",
    "GP",
    "GQ",
    "GR",
    "GS",
    "GT",
    "GU",
    "GW",
    "GY",
    "HK",
    "HN",
    "HR",
    "HT",
    "HU"
];

//test bools
var tab_create = false;
var tab_insert = false;
var tab_remove = true;
var tab_selct1 = true;
var tab_selct2 = true;

// unlock account at Parity CLI
personal.unlockAccount(personal.listAccounts[0], nodepass);

// drop table
function drop_table(tabname) {
    if (storage.tableExists.call(tabname, from)) {
        var hashes = new Array;
        var ids = new Array;

        for(var i = 0; i < storage.countRows.call(tabname, from); i++) {
            var rowid = storage.getIdByIndex.call(tabname, i, from);
            if (rowid > 0) {
                ids.push(rowid);
            }
        }

        if (ids.length > 0) {
            for(var j = 0; j < ids.length; j++) {
                hashes.push(storage.remove.sendTransaction(tabname, ids[j], sendtx));
            }
        }
        else {
            hashes.push(storage.remove.sendTransaction(tabname, 0, sendtx));
            console.log("\tdrop blank record for " + tabname + ": " + hashes[0]);
        }

        wait_transactions_byhash_array("drop table " + tabname, hashes, debug_log);

        if (storage.tableExists.call(tabname, from)) {
            console.log("\tFAILURE: drop table " + tabname);

            return false;
        }
    }

    return true;
}

// wait for transactions by hash-array
function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc = true;
    var index = 0;
    var hashes = tr_hash_arr;

    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length );

    while(hashes.length > 0) {
        var updated_hashes = new Array;

        for(var i = 0; i < hashes.length; i++) {
            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                if (!(eth.getTransactionReceipt(hashes[i]).status === "0x1")) {
                    rc = false;
                    console.log("\tFAILURE: transaction status=" + eth.getTransactionReceipt(hashes[i]).status);
                }
            }
        }

        hashes = updated_hashes;

        if (index == trtimeout) {
            rc = false;

            break;
        }
        else {
            if (logging && (index > 0 && index % 15 == 0)) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length) + "/" + tr_hash_arr.length);
            }

            index++;
            storage.sleep(1);
        }
    }

    return rc;
}

// generate database function
function generate_db(database_size) {
    for(var i=0; i<database_size; i++) {
        var d_index  = Math.floor(Math.random() * (domains.length));
        var ip_index = Math.floor(Math.random() * (ip_addrs.length));
        var ua_index = Math.floor(Math.random() * (browsers.length));
        var r_index  = Math.floor(Math.random() * (resolut.length));
        var p_index  = Math.floor(Math.random() * (pages.length));
        var c_index  = Math.floor(Math.random() * (countries.length));

        db_in_mem.push(
            domains[d_index] + '|' +
            ip_addrs[ip_index] + '|' +
            browsers[ua_index] + '|' +
            resolut[r_index] + '|' +
            pages[p_index] + '|' +
            countries[c_index]
        );
    }

    console.log("Database is successfully generated! DB size: " + database_size + " rows");
}

function rowids_list(indexes) {
    var rowids = new Array;

    for(var i=0; i<indexes.length; i++) {
        var id = storage.getIdByIndex.call(tab_name, indexes[i], from);

        if (id > -1) {
            rowids.push(id);
        }
    }

    console.log("\tremove plan: " + rowids.length + " rows, ids=[" + rowids + "]");

    return rowids;
}

// generate rowids list function
function indexes_list( list_size, database_size ) {
    var indexes = new Array;

    for( var i=0; i<list_size; i++ ) {
        var index = -1;
        var flag  = true;

        while( flag ) {
            flag  = false;
            index = Math.floor(Math.random() * (database_size - i));

            for( var k=0; k<indexes.length; k++ ) {
                if (indexes[k] == index) {
                    flag = true;

                    break;
                }
            }
        }

        if (index > -1) {
            indexes.push(index);
        }
        else {
            console.log("Failure white generating index for i=" + i);
        }
    }

    console.log("\tremove plan: " + indexes.length + " elements, indexes=[" + indexes + "]");

    return indexes;
}

// remove elements from memory database_size
function remove_from_db( database, index_array ) {
    var updated_db = new Array;

    for( var i=0; i<database.length; i++ ) {
        var flag = true;

        for( var k=0; k<index_array.length; k++ ) {
            if (index_array[k] > -1 && index_array[k] < database.length && index_array[k] == i) {
                flag = false;

                break;
            }
        }

        if (flag) {
            updated_db.push(database[i]);
        }
    }

    return updated_db;
}

generate_db(db_size);

// drop table if existed
drop_table(tab_name);

// table create test
console.log("TEST#1: table create");

wait_transactions_byhash_array(
    "create table " + tab_name,
    [
        storage.newTable.sendTransaction(tab_name, "", false, sendtx)
    ],
    debug_log
);

if (storage.tableExists.call(tab_name, from)) {
    console.log("\tDONE: table create test!");
    tab_create = true;
}
else {
    console.log("\tFAILURE: table create test!");
}

// table insert
console.log("TEST#2: table insert");

hashes = [];

for(var i=0; i<db_size; i++) {
    hashes.push(
        storage.insert.sendTransaction(tab_name, web3.fromUtf8(db_in_mem[i]), 0, false, personal.sign(web3.fromUtf8(db_in_mem[i]), personal.listAccounts[0], nodepass), sendtx)
    );
}

wait_transactions_byhash_array("insert to " + tab_name, hashes, debug_log);

if (storage.countRows.call(tab_name, from) == db_size) {
    console.log("\tDONE: table insert test!");
    tab_insert = true;
}
else {
    console.log("\tFAILURE: table insert test, count_rows=" + storage.countRows.call(tab_name, from));
}

//check database insert constiency
console.log("TEST#3: table constiency select");
var rows_in_tab = storage.countRows.call(tab_name, from);

for(var k=0; k<rows_in_tab; k++) {
    var id = storage.getIdByIndex.call(tab_name, k, from);

    if ( id > -1 ) {
        var [data, signature] = storage.select.call(tab_name, id, from);
        //console.log("["+k+"] {" + id + "}: "+data);

        if (!(web3.toUtf8(data) === db_in_mem[k])) {
            tab_selct1 = false;
            console.log("\tdatabase inconstiency at index " + k);
        }
    }
}
if ( tab_selct1 ) {
    console.log("\tDONE: database insert constiency test!");
}
else {
    console.log("\tFAILURE: database insert constiency test!");
}

// remove from table
console.log("TEST#4: remove from table");

hashes = [];

var indxes_to_remove = indexes_list(rm_rows, db_size);
var rowids_to_remove = rowids_list(indxes_to_remove);

for(var j=0; j<rowids_to_remove.length; j++) {
    var rowid = rowids_to_remove[j];

    if ( rowid > -1 ) {
        hashes.push(
            storage.remove.sendTransaction(tab_name, rowid, sendtx)
        );
    }
    else {
        console.log("\tinvalid id=" + rowid + " for remove");
    }
}

tab_remove = wait_transactions_byhash_array(
    "remove from " + tab_name,
    hashes,
    debug_log
);

if (storage.countRows.call(tab_name, from) == (db_size - rm_rows)) {
    var _chk = true;

    for(var j=0; j<rowids_to_remove.length; j++) {
        var rowid = rowids_to_remove[j];

        if ((storage.idExists.call(tab_name, rowid, from))) {
            _chk = false;
            console.log("\tremove failure: found removed id=" + rowid);
        }
    }

    if (_chk) {
        console.log("\tDONE: remove from table test!");
    }
    else {
        tab_remove = false;
    }
}
else {
    tab_remove = false;
    console.log("\tFAILURE: remove from table test, countRows=" + storage.countRows.call(tab_name, from));
}

var _database = new Array;
_database = remove_from_db(db_in_mem, indxes_to_remove);

// table select
console.log("TEST#5: table select");

for(var k=0; k<storage.countRows.call(tab_name, from); k++) {
    var id = storage.getIdByIndex.call(tab_name, k, from);

    if ( id > -1 ) {
        var [data, signature] = storage.select.call(tab_name, id, from);
        var fnd  = false;

        for(var n=0; n<_database.length; n++) {
            if (web3.toUtf8(data) === _database[n]) {
                fnd = true;

                break;
            }
        }

        if (!fnd) {
            tab_selct2 = false;
            console.log("\tcould not find data for id " + id);
            // break;
        }
    }
}

if ( tab_selct2 ) {
    console.log("\tDONE: table select test!");
}
else {
    console.log("\tFAILURE: table select test!");
}

// summary
if ( tab_create && tab_insert && tab_remove && tab_selct1 && tab_selct2 ) {
    console.log("ALL TESTS COMPLETED!");
}
else {
    console.log("TESTS FAILED!");
}
