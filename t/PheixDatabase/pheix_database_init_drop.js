var nodepass     = "node0";
var trtimeout    = 600;
var debug_log    = true;
var tdmultcreate = false;
var tdmultinit   = false;
var tdrop_t1     = false;
var tdrop_t2     = false;
var tdrop_t3     = false;
var tdrop_t4     = false;
var tdrop_t5     = false;
var tdrop_t6     = false;
var drop_table_1 = false;
var drop_table_2 = false;
var drop_table_3 = false;
var tdblankcrdel = false;
var tr_hash      = "0x0";
var gasqty       = 3000000;
var from         = {from: personal.listAccounts[0]};
var sendtx       = {from: personal.listAccounts[0], gas: gasqty};
var hashes       = new Array;

// application level implemented init
function init(userimpl) {
    var hashes = new Array;

    if (userimpl) {
        console.log("\tinit database: application level");
        var rows = [
            "pheix.org|127.0.0.1|Mozilla|640*480|index.html|RU",
            "twitter.com|127.0.0.11|IE|1900*1280|index2.html|US",
            "ya.ru|127.0.0.12|Opera|1024*768|index3.html|BY",
            "narkhov.pro|127.0.0.111|Safari|100*200|index4.html|UA",
            "foo.bar|127.0.0.254|Netscape|1152*778|index5.html|RO"
        ];
        hashes.push(
            storage.insert.sendTransaction("table_1", web3.fromUtf8(rows[0]), 1, false, personal.sign(web3.fromUtf8(rows[0]), personal.listAccounts[0], nodepass), sendtx),
            storage.insert.sendTransaction("table_1", web3.fromUtf8(rows[1]), 2, false, personal.sign(web3.fromUtf8(rows[1]), personal.listAccounts[0], nodepass), sendtx),
            storage.insert.sendTransaction("table_2", web3.fromUtf8(rows[2]), 1, false, personal.sign(web3.fromUtf8(rows[2]), personal.listAccounts[0], nodepass), sendtx),
            storage.insert.sendTransaction("table_2", web3.fromUtf8(rows[3]), 2, false, personal.sign(web3.fromUtf8(rows[3]), personal.listAccounts[0], nodepass), sendtx),
            storage.insert.sendTransaction("table_3", web3.fromUtf8(rows[4]), 1, false, personal.sign(web3.fromUtf8(rows[4]), personal.listAccounts[0], nodepass), sendtx)
        );
    }
    else {
        console.log("\tinit database: blockchain level");
        hashes.push(storage.init.sendTransaction(sendtx));
    }

    return wait_transactions_byhash_array("init", hashes, debug_log);
}

// create tables
function create_all() {
    var hashes = new Array;

    hashes.push(
        storage.newTable.sendTransaction("table_1", "", false, sendtx),
        storage.newTable.sendTransaction("table_2", "", false, sendtx),
        storage.newTable.sendTransaction("table_3", "", false, sendtx)
    );

    return wait_transactions_byhash_array("create tables", hashes, debug_log);
}

// drop table
function drop_table(tabname) {
    if (storage.tableExists.call(tabname, from)) {
        var hashes = new Array;
        var ids = new Array;

        for(var i = 0; i < storage.countRows.call(tabname, from); i++) {
            var rowid = storage.getIdByIndex.call(tabname, i, from);
            if (rowid > 0) {
                ids.push(rowid);
            }
        }

        if (ids.length > 0) {
            for(var j = 0; j < ids.length; j++) {
                hashes.push(storage.remove.sendTransaction(tabname, ids[j], sendtx));
            }
        }
        else {
            hashes.push(storage.remove.sendTransaction(tabname, 0, sendtx));
            console.log("\tdrop blank record for " + tabname + ": " + hashes[0]);
        }

        wait_transactions_byhash_array("drop table " + tabname, hashes, debug_log);

        if (storage.tableExists.call(tabname, from)) {
            console.log("\tFAILURE: drop table " + tabname);
            return false;
        }
    }
    return true;
}

// drop all existed tables
function drop_all() {
    var rc = true;
    var existed_tables_num = storage.countTables.call(from);

    if (existed_tables_num > 0) {
        console.log("DROPDB: dropping " + existed_tables_num + " existed tables");
        var existed_tables = new Array;

        for(var i = 0; i < existed_tables_num; i++) {
            var tabname = storage.getNameByIndex.call(i, from);
            if (storage.tableExists.call(tabname, from)) {
                existed_tables.push(tabname);
            }
        }

        for(var i = 0; i < existed_tables.length; i++) {
            if (!drop_table(existed_tables[i])) {
                rc = false;
            }
        }
    }
    return rc;
}

// wait for transactions by hash-array
function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc = true;
    var index = 0;
    var hashes = tr_hash_arr;

    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length );

    while(hashes.length > 0) {
        var updated_hashes = new Array;
        for(var i = 0; i < hashes.length; i++) {

            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                if (!(eth.getTransactionReceipt(hashes[i]).status === "0x1")) {
                    rc = false;
                    console.log("\tFAILURE: transaction status=" + eth.getTransactionReceipt(hashes[i]).status);
                }
            }
        }

        hashes = updated_hashes;

        if (index == trtimeout) {
            rc = false;
            break;
        }
        else {
            if (logging && ( index > 0 && index % 15 == 0 )) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length) + "/" + tr_hash_arr.length);
            }

            index++;

            storage.sleep(1);
        }
    }
    return rc;
}

// create blank tables and drop them
console.log("TEST#1: create blank tables and drop them");

personal.unlockAccount(personal.listAccounts[0], nodepass);

drop_all();
create_all();

if (drop_all()) {
    tdblankcrdel = true;
}

if (tdblankcrdel) {
    console.log("\tDONE: create blank tables and drop them");
}
else {
    console.log("\tFAILURE: create blank tables and drop them");
}

// init db and multiple table create with the same name
console.log("TEST#2: init db and multiple table create with the same name");

personal.unlockAccount(personal.listAccounts[0], nodepass);

drop_all();
init(false);

hashes = [
    storage.newTable.sendTransaction("table_1", "", false, sendtx),
    storage.newTable.sendTransaction("table_2", "", false, sendtx)
];

wait_transactions_byhash_array("create multiple tabs", hashes, debug_log);

if (
    storage.countTables.call(from) == 3 &&
    storage.tableExists.call("table_1", from) &&
    storage.tableExists.call("table_2", from) &&
    storage.tableExists.call("table_3", from) &&
    storage.getTableIndex.call("table_1", from) == 0 &&
    storage.getTableIndex.call("table_2", from) == 1 &&
    storage.getTableIndex.call("table_3", from) == 2
) {
    tdmultcreate = true;
}

if (tdmultcreate) {
    console.log("\tDONE: init db and multiple table create with the same name!");
}
else {
    console.log("\tFAILURE: init db and multiple table create with the same name!");
}

// multiple init db - rows with the same ids
console.log("TEST#3: multiple init db - rows with the same ids");

personal.unlockAccount(personal.listAccounts[0], nodepass);

drop_all();

if (create_all()) {
    init(false);
    init(true);
}
else {
    console.log("\tFAILURE: multiple init db - tables are not created!");
}
if (
    storage.countTables.call(from) == 3 &&
    storage.tableExists.call("table_1", from) &&
    storage.tableExists.call("table_2", from) &&
    storage.tableExists.call("table_3", from) &&
    storage.getTableIndex.call("table_1", from) == 0 &&
    storage.getTableIndex.call("table_2", from) == 1 &&
    storage.getTableIndex.call("table_3", from) == 2 &&
    storage.getMaxId.call("table_1", from) == 5 &&
    storage.getMaxId.call("table_2", from) == 5 &&
    storage.getMaxId.call("table_3", from) == 3
) {
    tdmultinit = true;
}

if (tdmultinit) {
    console.log("\tDONE: multiple init db - rows with the same ids!");
}
else {
    console.log("\tFAILURE: multiple init db - rows with the same ids!");
}

// init db and forward table drop
console.log("TEST#4: init db and forward table drop");

personal.unlockAccount(personal.listAccounts[0], nodepass);

if (create_all()) {
    init(true);
}
else {
    console.log("\tFAILURE: forward table drop test - tables are not created!");
}

drop_table_1 = drop_table("table_1");

if (
    storage.getTableIndex.call("table_2", from) != 1 ||
    storage.getTableIndex.call("table_3", from) != 0 ||
    storage.countTables.call(from) != 2 ||
    storage.tableExists.call("table_1", from)
) {
    drop_table_1 = false;
}

drop_table_2 = drop_table("table_2");

if (
    storage.getTableIndex.call("table_3", from) != 0 ||
    storage.countTables.call(from) != 1 ||
    storage.tableExists.call("table_2", from)
) {
    drop_table_2 = false;
}

drop_table_3 = drop_table("table_3");

if (
    storage.countTables.call(from) != 0 ||
    storage.tableExists.call("table_3", from)
) {
    drop_table_3 = false;
}

if (drop_table_1 && drop_table_2 && drop_table_3) {
    console.log("\tDONE: forward table drop test!");
    tdrop_t1 = true;
}
else {
    console.log("\tFAILURE: forward table drop test!");
}

// init db and reverse table drop
console.log("TEST#5: init db and reverse table drop");

personal.unlockAccount(personal.listAccounts[0], nodepass);
init(false);

drop_table_3 = drop_table("table_3");
if (
    storage.getTableIndex.call("table_2", from) != 1 ||
    storage.getTableIndex.call("table_1", from) != 0 ||
    storage.countTables.call(from) != 2 ||
    storage.tableExists.call("table_3", from)
) {
    drop_table_3 = false;
}

drop_table_2 = drop_table("table_2");

if (
    storage.getTableIndex.call("table_1", from) != 0 ||
    storage.countTables.call(from) != 1 ||
    storage.tableExists.call("table_2", from)
) {
    drop_table_2 = false;
}

drop_table_1 = drop_table("table_1");

if (
    storage.countTables.call(from) != 0 ||
    storage.tableExists.call("table_1", from)
) {
    drop_table_1 = false;
}

if (drop_table_1 && drop_table_2 && drop_table_3) {
    console.log("\tDONE: reverse table drop test!");
    tdrop_t2 = true;
}
else {
    console.log("\tFAILURE: reverse table drop test!");
}

// init db and first random table drop
console.log("TEST#6: init db and first random table drop");

personal.unlockAccount(personal.listAccounts[0], nodepass);

if (create_all()) {
    init(true);
}
else {
    console.log("\tFAILURE: init db and first random table drop - tables are not created!");
}

drop_table_2 = drop_table("table_2");

if (
    storage.getTableIndex.call("table_3", from) != 1 ||
    storage.getTableIndex.call("table_1", from) != 0 ||
    storage.countTables.call(from) != 2 ||
    storage.tableExists.call("table_2", from)
) {
    drop_table_2 = false;
}

drop_table_3 = drop_table("table_3");

if (
    storage.getTableIndex.call("table_1", from) != 0 ||
    storage.countTables.call(from) != 1 ||
    storage.tableExists.call("table_3", from)
) {
    drop_table_3 = false;
}

drop_table_1 = drop_table("table_1");

if (
    storage.countTables.call(from) != 0 ||
    storage.tableExists.call("table_1", from)
) {
    drop_table_1 = false;
}

if (drop_table_1 && drop_table_2 && drop_table_3) {
    console.log("\tDONE: init db and first random table drop!");
    tdrop_t3 = true;
}
else {
    console.log("\tFAILURE: init db and first random table drop!");
}

// init db and second random table drop
console.log("TEST#7: init db and second random table drop");

personal.unlockAccount(personal.listAccounts[0], nodepass);
init(false);

drop_table_2 = drop_table("table_2");

if (
    storage.getTableIndex.call("table_3", from) != 1 ||
    storage.getTableIndex.call("table_1", from) != 0 ||
    storage.countTables.call(from) != 2 ||
    storage.tableExists.call("table_2", from)
) {
    drop_table_2 = false;
}

drop_table_1 = drop_table("table_1");

if (
    storage.getTableIndex.call("table_3", from) != 0 ||
    storage.countTables.call(from) != 1 ||
    storage.tableExists.call("table_1", from)
) {
    drop_table_1 = false;
}

drop_table_3 = drop_table("table_3");

if (
    storage.countTables.call(from) != 0 ||
    storage.tableExists.call("table_3", from)
) {
    drop_table_3 = false;
}

if (drop_table_1 && drop_table_2 && drop_table_3) {
    console.log("\tDONE: init db and second random table drop!");
    tdrop_t4 = true;
}
else {
    console.log("\tFAILURE: init db and second random table drop!");
}

// init db and third random table drop
console.log("TEST#8: init db and third random table drop");

personal.unlockAccount(personal.listAccounts[0], nodepass);

if (create_all()) {
    init(true);
}
else {
    console.log("\tFAILURE: init db and third random table drop - tables are not created!");
}

drop_table_1 = drop_table("table_1");

if (
    storage.getTableIndex.call("table_2", from) != 1 ||
    storage.getTableIndex.call("table_3", from) != 0 ||
    storage.countTables.call(from) != 2 ||
    storage.tableExists.call("table_1", from)
) {
    drop_table_1 = false;
}

drop_table_3 = drop_table("table_3");

if (
    storage.getTableIndex.call("table_2", from) != 0 ||
    storage.countTables.call(from) != 1 ||
    storage.tableExists.call("table_3", from)
) {
    drop_table_3 = false;
}

drop_table_2 = drop_table("table_2");

if (
    storage.countTables.call(from) != 0 ||
    storage.tableExists.call("table_2", from)
) {
    drop_table_2 = false;
}

if (drop_table_1 && drop_table_2 && drop_table_3) {
    console.log("\tDONE: init db and third random table drop!");
    tdrop_t5 = true;
}
else {
    console.log("\tFAILURE: init db and third random table drop!");
}

// init db and fourth random table drop
console.log("TEST#9: init db and fourth random table drop");

personal.unlockAccount( personal.listAccounts[0], nodepass );
init(false);

drop_table_3 = drop_table("table_3");

if (
    storage.getTableIndex.call("table_2", from) != 1 ||
    storage.getTableIndex.call("table_1", from) != 0 ||
    storage.countTables.call(from) != 2 ||
    storage.tableExists.call("table_3", from)
) {
    drop_table_3 = false;
}

drop_table_1 = drop_table("table_1");

if (
    storage.getTableIndex.call("table_2", from) != 0 ||
    storage.countTables.call(from) != 1 ||
    storage.tableExists.call("table_1", from)
) {
    drop_table_1 = false;
}

drop_table_2 = drop_table("table_2");

if (
    storage.countTables.call(from) != 0 ||
    storage.tableExists.call("table_2", from)
) {
    drop_table_2 = false;
}

if (drop_table_1 && drop_table_2 && drop_table_3) {
    console.log("\tDONE: init db and fourth random table drop!");
    tdrop_t6 = true;
}
else {
    console.log("\tFAILURE: init db and fourth random table drop!");
}

if (
    tdblankcrdel &&
    tdmultcreate &&
    tdmultinit &&
    tdrop_t1 &&
    tdrop_t2 &&
    tdrop_t3 &&
    tdrop_t4 &&
    tdrop_t5 &&
    tdrop_t6
) {
    console.log("ALL TESTS COMPLETED!");
}
else {
    console.log("TESTS FAILED!");
}
