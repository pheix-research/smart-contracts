var nodepass   = "node0";
var gasqty     = 3000000;
var debug_log  = true;
var trtimeout  = 600;
var fundamount = 0.1

//test bools
var test_receive  = false;
var test_fallback = false;

// unlock account at Parity CLI
personal.unlockAccount(personal.listAccounts[0], nodepass);

// wait for transactions by hash-array
function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc = true;
    var index = 0;
    var hashes = tr_hash_arr;

    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length );

    while(hashes.length > 0) {
        var updated_hashes = new Array;

        for(var i = 0; i < hashes.length; i++) {
            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                if (!(eth.getTransactionReceipt(hashes[i]).status === "0x1")) {
                    rc = false;
                    console.log("\tFAILURE: transaction status=" + eth.getTransactionReceipt(hashes[i]).status);
                }
            }
        }

        hashes = updated_hashes;

        if (index == trtimeout) {
            rc = false;
            break;
        }
        else {
            if (logging && (index > 0 && index % 15 == 0)) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length) + "/" + tr_hash_arr.length);
            }

            index++;
            storage.sleep(1);
        }
    }

    return rc;
}

// receive funds
console.log("TEST#1: receive funds");

var initial_balance = web3.fromWei(eth.getBalance(storage.address));

wait_transactions_byhash_array(
    "receive funds",
    [
        eth.sendTransaction({
            from:  personal.listAccounts[0],
            to:    storage.address,
            gas:   gasqty,
            value: web3.toWei(fundamount),
        })
    ],
    debug_log
);

var current_balance = web3.fromWei(eth.getBalance(storage.address));

if ((current_balance - initial_balance).toFixed(2) == fundamount) {
    var CurrReceiveFilterRes;

    web3.eth.filter({
        fromBlock: 0,
        toBlock: 'latest',
        topics: [
            web3.sha3("PheixAccess(bytes32,uint8,uint256)"),
            web3.padRight(web3.toHex("*"), 66),
            "0x" + web3.padLeft('a', 64)
        ]}).get(function(error, result) {
            if (!error) {
                CurrReceiveFilterRes = result;
            }
            else {
                console.log("\tFAILURE: " + error);
            }
        });

    if (CurrReceiveFilterRes[CurrReceiveFilterRes.length-1] && CurrReceiveFilterRes[CurrReceiveFilterRes.length-1].blockNumber > 0) {
        console.log("\tDONE: funds are received!");
        test_receive = true;
    }
    else {
        console.log("\tFAILURE: no receive event found!");
    }
}
else {
    console.log("\tFAILURE: no funds are received: initial_balance=" + initial_balance + ", current_balance=" + current_balance + ", fundamount=" + fundamount);
}

// fallback
console.log("TEST#2: fallback");

wait_transactions_byhash_array(
    "fallback",
    [
        eth.sendTransaction({
            from: personal.listAccounts[0],
            to:   storage.address,
            gas:  gasqty,
            data: web3.toHex("unknown")
        })
    ],
    debug_log
);

var CurrFallbackFilterRes;

web3.eth.filter({
    fromBlock: 0,
    toBlock: 'latest',
    topics: [
        web3.sha3("PheixAccess(bytes32,uint8,uint256)"),
        web3.padRight(web3.toHex("*"), 66),
        "0x" + web3.padLeft('b', 64)
    ]}).get(function(error, result) {
        if (!error) {
            CurrFallbackFilterRes = result;
        }
        else {
            console.log("\tFAILURE: " + error);
        }
    });

if (CurrFallbackFilterRes[CurrFallbackFilterRes.length-1] && CurrFallbackFilterRes[CurrFallbackFilterRes.length-1].blockNumber > 0) {
    console.log("\tDONE: fallback is called!");
    test_fallback = true;
}
else {
    console.log("\tFAILURE: no fallback event found!");
}

// summary
if (test_receive && test_fallback) {
    console.log("ALL TESTS COMPLETED!");
}
else {
    console.log("TESTS FAILED!");
}
