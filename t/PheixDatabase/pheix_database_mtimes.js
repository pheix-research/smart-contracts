var nodepass  = "node0";
var gasqty    = 3000000;
var trtimeout = 600;
var from      = {from: personal.listAccounts[0]};
var sendtx    = {from: personal.listAccounts[0], gas: gasqty};
var hashes    = new Array;
var debug_log = true;
var tabmtimes = new Array;
var globmtime = 0;

// test tables
var tables = ["table_1", "table_2", "table_3"];

//test bools
var tab_init   = true;
var initmtimes = true;
var tab_insrt1 = false;
var tab_insrt2 = false;
var tab_mtimes = false;
var tab_updmt  = false;
var cmp_w_logs = true;

// unlock account
personal.unlockAccount(personal.listAccounts[0], nodepass);

// drop table
function drop_table(tabname) {
    if (storage.tableExists.call(tabname, from)) {
        var hashes = new Array;
        var ids = new Array;

        for(var i = 0; i < storage.countRows.call(tabname, from); i++) {
            var rowid = storage.getIdByIndex.call(tabname, i, from);

            if (rowid > 0) {
                ids.push(rowid);
            }
        }

        if (ids.length > 0) {
            for(var j = 0; j < ids.length; j++) {
                hashes.push(storage.remove.sendTransaction(tabname, ids[j], sendtx));
            }
        }
        else {
            hashes.push(storage.remove.sendTransaction(tabname, 0, sendtx));
            console.log("\tdrop blank record for " + tabname + ": " + hashes[0]);
        }

        wait_transactions_byhash_array("drop table " + tabname, hashes, debug_log);

        if (storage.tableExists.call(tabname, from)) {
            console.log("\tFAILURE: drop table " + tabname);

            return false;
        }
    }

    return true;
}

// wait for transactions by hash-array
function wait_transactions_byhash_array(tr_name, tr_hash_arr, logging) {
    var rc = true;
    var index = 0;
    var hashes = tr_hash_arr;

    console.log("\twaiting for transaction '" + tr_name + "' by array of hashes, len=" + hashes.length );

    while(hashes.length > 0) {
        var updated_hashes = new Array;

        for(var i = 0; i < hashes.length; i++) {
            if (!eth.getTransactionReceipt(hashes[i])) {
                updated_hashes.push(hashes[i]);
            }
            else {
                if (!(eth.getTransactionReceipt(hashes[i]).status === "0x1")) {
                    rc = false;
                    console.log("\tFAILURE: transaction status=" + eth.getTransactionReceipt(hashes[i]).status);
                }
            }
        }

        hashes = updated_hashes;

        if (index == trtimeout) {
            rc = false;

            break;
        }
        else {
            if (logging && (index > 0 && index % 15 == 0)) {
                console.log("\t    elapsed time: " + index + " sec, mined " + (tr_hash_arr.length - hashes.length) + "/" + tr_hash_arr.length);
            }

            index++;
            storage.sleep(1);
        }
    }

    return rc;
}

// insert row into table
function insert_row(tabname, data) {
    var rowsbefore = parseInt(storage.countRows.call(tabname, from));

    wait_transactions_byhash_array(
        "insert into <" + tabname + ">",
        [
            storage.insert.sendTransaction(tabname, web3.fromUtf8(data), 0, false, personal.sign(web3.fromUtf8(data), personal.listAccounts[0], nodepass), sendtx)
        ],
        debug_log
    );

    var rowsafter = parseInt(storage.countRows.call(tabname, from));

    if (rowsafter == (rowsbefore + 1)) {
        console.log("\tDONE: insert to <" + tabname + ">");

        return true;
    }
    else {
        console.log("\tFAILURE: insert to <" + tabname + ">, initial rows num = " + rowsbefore + ", rows num after insert = " + rowsafter);
    }

    return false;
}

// drop test tables if existed
for(var i = 0; i < tables.length; i++) {
    drop_table(tables[i]);
}

// table init test
{
    console.log("TEST#1: init embedded tables");

    wait_transactions_byhash_array(
        "init embedded tables",
        [
            storage.init.sendTransaction(sendtx)
        ],
        debug_log
    );

    for(var i = 0; i < tables.length; i++) {
        if (!storage.tableExists.call(tables[i], from)) {
            tab_init = false;

            break;
        }
    }

    if (tab_init) {
        console.log("\tDONE: init embedded tables!");
    }
    else {
        console.log("\tFAILURE: init embedded tables!");
    }
}

// save and check initial mod times
{
    console.log("TEST#2: save and check initial mod times");

    globmtime = parseInt(storage.getGlobalModTime.call(from));

    for(var i = 0; i < tables.length; i++) {
        var tm = parseInt(storage.getTableModTime.call(tables[i], from));
        tabmtimes.push(tm);

        if (tm != globmtime) {
            initmtimes = false;
        }
    }

    if (initmtimes) {
        console.log("\tDONE: save and check initial mod times!");
    }
    else {
        console.log("\tFAILURE: save and check initial mod times, globalmtime = " + globmtime + ", mtimes = " + tabmtimes.join(","));
    }
}

// table insert
{
    console.log("TEST#3: insert to <" + tables[1] + ">");

    tab_insrt1 = insert_row(tables[1], "pheix.io|172.0.0.1|Gozilla|1900*1280|/embedded/1598179320|RU")
}

// check modification times
{
    console.log("TEST#4: check modification times");

    var resmtimes = new Array;
    var resglobalmtime = parseInt(storage.getGlobalModTime.call(from));

    for(var i = 0; i < tables.length; i++) {
        resmtimes.push(parseInt(storage.getTableModTime.call(tables[i], from)));
    }

    if ((resglobalmtime == resmtimes[1]) && (resmtimes[0] == resmtimes[2])) {
        console.log("\tDONE: check modification times!");
        tab_mtimes = true;
    }
    else {
        console.log("\tFAILURE: check modification times, globalmtime = " + resglobalmtime + ", mtimes = " + resmtimes.join(","));
    }
}

// update table modification time
{
    console.log("TEST#5: update table modification time");

    var beforemtime = parseInt(storage.getTableModTime.call(tables[0], from));
    var beforeglobalmtime = parseInt(storage.getGlobalModTime.call(from));

    wait_transactions_byhash_array(
        "update <" + tables[0] + "> modtime",
        [
            storage.updateModTimes.sendTransaction(tables[0], sendtx)
        ],
        debug_log
    );

    var aftermtime = parseInt(storage.getTableModTime.call(tables[0], from));
    var afterglobalmtime = parseInt(storage.getGlobalModTime.call(from));

    if ((aftermtime == afterglobalmtime) && (aftermtime > beforemtime) && (afterglobalmtime > beforeglobalmtime)) {
        console.log("\tDONE: update <" + tables[0] + "> modification time!");
        tab_updmt = true;
    }
    else {
        console.log("\tFAILURE: update <" + tables[0] + "> modification time, globalmtime before/after " + beforeglobalmtime + "/" + afterglobalmtime + ", mtime before/after " + beforemtime + "/" + aftermtime);
    }
}

// table insert
{
    console.log("TEST#6: clean up mod times with insert to <" + tables[0] + ">");

    tab_insrt2 = insert_row(tables[0], "pheix.io|192.168.0.1|ZOpera|1900*1280|/embedded/1598179320|SU")
}

// compare modtimes with timestamps from logs
{
    console.log("TEST#7: compare modtimes with timestamps from logs");

    for(var i = 0; i < tables.length; i++) {
        var mtime = parseInt(storage.getTableModTime.call(tables[i], from));

        //read from logs
        var CurrFilterRes;

        web3.eth.filter({
            fromBlock: 'earliest',
            toBlock: 'latest',
            topics: [
                web3.sha3('PheixAccess(bytes32,uint8,uint256)'),
                web3.padRight(web3.toHex(tables[i]),66)
            ]
        }).get(function(error, result){
            if (!error) CurrFilterRes = result;
            else console.log("\tFAILURE: get filters failure - " + error);
        });

        if (CurrFilterRes.length > 0) {
            var latestrec = CurrFilterRes.pop();
            var timestamp = web3.eth.getBlock(latestrec.blockNumber).timestamp;

            if (timestamp != mtime) {
                console.log("\tFAILURE: compare with <" + tables[i] + "> logs, mtime = " + mtime + ", logs timestamp = " + timestamp);
                cmp_w_logs = false;
            }
        }
        else {
            console.log("\tFAILURE: compare with <" + tables[i] + "> logs, no logs found - CurrFilterRes len = " + CurrFilterRes.length);
            cmp_w_logs = false;
        }
    }

    if (cmp_w_logs) {
        console.log("\tDONE: compare modtimes with timestamps from logs!");
    }
    else {
        console.log("\tFAILURE: compare modtimes with timestamps from logs");
    }
}

// summary
if (tab_init && tab_insrt1 && tab_insrt2 && tab_mtimes && initmtimes && tab_updmt && cmp_w_logs) {
    console.log("ALL TESTS COMPLETED!");
}
else {
    console.log("TESTS FAILED!");
}
