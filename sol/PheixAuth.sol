// SPDX-License-Identifier: Artistic-2.0
pragma solidity >=0.4.0 <0.9.0;

contract PheixAuth {
    event PheixAuthCode (
        uint256 indexed code
    );

    uint constant dfltk = 10000000000;
    uint constant dfltdelta = 300;
    uint constant dfltsealperiod = 5;

    uint sealperiod;
    uint delta;
    uint seedmod;
    uint timestamp;
    address owner;
    bytes32 pkey;

    modifier only_owner {
        assert(msg.sender == owner);
        _;
    }

    constructor(uint _sealperiod, uint _delta, uint _seedmod, uint256 _pkey) {
        owner = msg.sender;
        timestamp = block.timestamp;
        seedmod = _seedmod > 0 && _seedmod <= dfltk ? _seedmod : dfltk;
        delta = _delta > 0 && _delta <= dfltdelta ? _delta : dfltdelta;
        sealperiod = _sealperiod > 0 ? _sealperiod : dfltsealperiod;
        pkey = keccak256(abi.encodePacked(_pkey > 0 ? _pkey : getPseudoRand()));

        emit PheixAuthCode(1);
    }

    receive() external payable only_owner {}

    fallback() external payable only_owner {}

    function updateState(uint256 publickey) public only_owner returns (bool success) {
        if (block.timestamp - timestamp <= delta + sealperiod) {
            timestamp = block.timestamp;
            pkey = keccak256(abi.encodePacked(publickey > 0 ? publickey : getPseudoRand()));

            emit PheixAuthCode(2);
        }
        else {
            doExit();
        }

        return true;
    }

    function doExit() public only_owner returns (bool success) {
        selfdestruct(payable(owner));

        return true;
    }

    function getPkey() public view only_owner returns (bytes32 publickey) {
        if (block.timestamp - timestamp <= delta + sealperiod) {
            return pkey;
        }
    }

    function getDiff() public view only_owner returns (int256 diff) {
        return int256(block.timestamp - timestamp) - int256(delta + sealperiod);
    }

    function getPseudoRand() private view returns(uint256) {
        uint ts = block.timestamp;
        uint256 seed = uint256(keccak256(abi.encodePacked(
            ts + block.prevrandao +
            ((uint256(keccak256(abi.encodePacked(block.coinbase)))) / (ts)) +
            block.gaslimit +
            ((uint256(keccak256(abi.encodePacked(msg.sender)))) / (ts)) +
            block.number
        )));

        return (seed - ((seed / seedmod) * seedmod));
    }
}
