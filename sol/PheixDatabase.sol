// SPDX-License-Identifier: Artistic-2.0
pragma solidity >=0.4.0 <0.9.0;

contract PheixDatabase {
    // contract events
    event PheixAccess (
        bytes32 indexed table,
        uint8 indexed mcode,
        uint256 indexed rowid
    );

    // contract types
    struct Record {
        uint index;
        bool comp;
        bytes data;
        string signature;
    }
    struct Table {
        string name;
        string fields;
        uint mtime;
        uint index;
        bool comp;
        uint[] indexes;
        mapping(uint => Record) Records;
    }
    mapping(string => Table) Tables;

    // contract state variables
    string[] tableNames;
    uint globalModTime;
    address owner;

    //modifiers
    modifier only_owner {
        assert(msg.sender == owner);
        _;
    }

    // constructor
    constructor() {
        owner = msg.sender;
        emit PheixAccess(strToB32("*"), 9, 0);
    }

    // contract public functions
    receive() external payable only_owner {
        emit PheixAccess(strToB32("*"), 10, 0);
    }

    fallback() external payable only_owner {
        emit PheixAccess(strToB32("*"), 11, 0);
    }

    function init() public returns (bool success) {
        string memory flds = "#domain;ip;useragent;resolution;referer;country";
        bytes[5] memory rows = [
            bytes("pheix.org|127.0.0.1|Mozilla|640*480|index.html|RU"),
            bytes("twitter.com|127.0.0.11|IE|1900*1280|index2.html|US"),
            bytes("ya.ru|127.0.0.12|Opera|1024*768|index3.html|BY"),
            bytes("narkhov.pro|127.0.0.111|Safari|100*200|index4.html|UA"),
            bytes("foo.bar|127.0.0.254|Netscape|1152*778|index5.html|RO")
        ];
        newTable("table_1", flds, false);
        newTable("table_2", flds, false);
        newTable("table_3", flds, false);
        insert("table_1", rows[0], 0, false, "");
        insert("table_1", rows[1], 0, false, "");
        insert("table_2", rows[2], 0, false, "");
        insert("table_2", rows[3], 0, false, "");
        insert("table_3", rows[4], 0, false, "");
        emit PheixAccess(strToB32("*"), 8, 0);
        return true;
    }

    function insert(
        string memory tabname,
        bytes memory rowdata,
        uint id,
        bool comp,
        string memory signature
    ) public returns (bool success) {
        if (tableExists(tabname)) {
            uint rowid;
            if (id > 0 && !idExists(tabname, id)) {
                rowid = id;
            } else {
                rowid = getMaxId(tabname);
            }
            Tables[tabname].indexes.push(rowid);
            Tables[tabname].Records[rowid].index = Tables[tabname].indexes.length - 1;
            Tables[tabname].Records[rowid].data = rowdata;
            Tables[tabname].Records[rowid].comp = comp;
            Tables[tabname].Records[rowid].signature = signature;
            emit PheixAccess(strToB32(tabname), 5, rowid);
            updateModTimes(tabname);
            return true;
        }
        return false;
    }

    function newTable(
        string memory tabname,
        string memory fields,
        bool comp
    ) public returns (bool success) {
        if (!tableExists(tabname)) {
            tableNames.push(tabname);
            Tables[tabname].name = tabname;
            Tables[tabname].fields = fields;
            Tables[tabname].index = tableNames.length - 1;
            Tables[tabname].comp = comp;
            emit PheixAccess(strToB32(tabname), 3, 0);
            updateModTimes(tabname);
            return true;
        }
    }

    function remove(
        string memory tabname,
        uint rowid
    ) public returns (uint id) {
        uint i = 0;
        id = 0;
        if (tableExists(tabname)) {
            if (idExists(tabname, rowid)) {
                for (i = 0; i < Tables[tabname].indexes.length; i++) {
                    if (Tables[tabname].indexes[i] == rowid) {
                        delete Tables[tabname].Records[Tables[tabname].indexes[i]];
                        Tables[tabname].indexes[i] = Tables[tabname].indexes[Tables[tabname].indexes.length - 1];
                        Tables[tabname].indexes.pop();
                        emit PheixAccess(strToB32(tabname), 6, rowid);
                        updateModTimes(tabname);
                        id = i;
                        break;
                    }
                }
            }
            if (countRows(tabname) == 0) {
                uint dropIndex = Tables[tabname].index;
                uint lastIndex = tableNames.length - 1;
                tableNames[dropIndex] = tableNames[lastIndex];
                Tables[tableNames[dropIndex]].index = dropIndex;
                tableNames.pop();
                if (!tableExists(tabname)) {
                    emit PheixAccess(strToB32(tabname), 7, 0);
                    globalModTime = block.timestamp;
                }
            }
        }
        return id;
    }

    function set(
        string memory tabname,
        uint rowid,
        bytes memory rowdata,
        bool comp,
        string memory signature
    ) public returns (bool success) {
        if (tableExists(tabname) && rowid > 0) {
            if (idExists(tabname, rowid)) {
                bytes memory rdb = bytes(rowdata);
                if (rdb.length > 0) {
                    Tables[tabname].Records[rowid].data = rowdata;
                    Tables[tabname].Records[rowid].comp = comp;
                    Tables[tabname].Records[rowid].signature = signature;
                    emit PheixAccess(strToB32(tabname), 4, rowid);
                    updateModTimes(tabname);
                    return true;
                }
            }
        }
    }

    function setFields(
        string memory tabname,
        string memory fields
    ) public returns (bool success) {
        if (tableExists(tabname)) {
            bytes memory fldsb = bytes(fields);
            if (fldsb.length > 0) {
                Tables[tabname].fields = fields;
                updateModTimes(tabname);
                return true;
            }
        }
    }

    function updateRecCompression(
        string memory tabname,
        uint rowid,
        bool comp
    ) public returns (bool success) {
        if (tableExists(tabname) && rowid > 0) {
            if (idExists(tabname, rowid)) {
                Tables[tabname].Records[rowid].comp = comp;
                emit PheixAccess(strToB32(tabname), 2, 0);
                updateModTimes(tabname);
                return true;
            }
        }
    }

    function updateTabCompression(
        string memory tabname,
        bool comp
    ) public returns (bool success) {
        if (tableExists(tabname)) {
            Tables[tabname].comp = comp;
            emit PheixAccess(strToB32(tabname), 1, 0);
            return true;
        }
    }

    function updateModTimes(
        string memory tabname
    ) public returns (bool success) {
        if (tableExists(tabname)) {
            globalModTime = block.timestamp;
            Tables[tabname].mtime = globalModTime;
            return true;
        }
    }

    // contract public view functions
    function countRows(
        string memory tabname
    ) public view returns (uint count) {
        return Tables[tabname].indexes.length;
    }

    function countTables() public view returns (uint count) {
        return tableNames.length;
    }

    function getDataByIndex(
        string memory tabname,
        uint index
    ) public view returns (bytes memory data, string memory signature) {
        int rowid = getIdByIndex(tabname, index);
        if (rowid > -1) {
            return (Tables[tabname].Records[uint(rowid)].data, Tables[tabname].Records[uint(rowid)].signature);
        }
    }

    function getFields(
        string memory tabname
    ) public view returns (string memory fields) {
        if (tableExists(tabname)) {
            return Tables[tabname].fields;
        }
    }

    function getGlobalModTime() public view returns (uint globmtime) {
        return globalModTime;
    }

    function getTableModTime(
        string memory tabname
    ) public view returns (uint tabmtime) {
        if (tableExists(tabname)) {
            return Tables[tabname].mtime;
        }
    }

    function getIdByIndex(
        string memory tabname,
        uint index
    ) public view returns (int rowid) {
        rowid = -1;
        if (tableExists(tabname)) {
            if (index < Tables[tabname].indexes.length) {
                rowid = int(Tables[tabname].indexes[index]);
            }
        }
        return rowid;
    }

    function getMaxId(
        string memory tabname
    ) public view returns (uint rowid) {
        uint i = 0;
        rowid = 0;
        if (tableExists(tabname)) {
            for (i = 0; i < Tables[tabname].indexes.length; i++) {
                if (Tables[tabname].indexes[i] > rowid) {
                    rowid = Tables[tabname].indexes[i];
                }
            }
            rowid++;
        }
        return rowid;
    }

    function getNameByIndex(
        uint index
    ) public view returns (string memory name) {
        if (index < tableNames.length) {
            return tableNames[index];
        }
    }

    function getTableIndex(
        string memory tabname
    ) public view returns (int index) {
        if (tableExists(tabname)) {
            return int(Tables[tabname].index);
        }
        return -1;
    }

    function idExists(
        string memory tabname,
        uint rowid
    ) public view returns (bool success) {
        uint i = 0;
        if (rowid > 0) {
            if (tableExists(tabname)) {
                for (i = 0; i < Tables[tabname].indexes.length; i++) {
                    if (Tables[tabname].indexes[i] == rowid) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    function isTabCompressed(
        string memory tabname
    ) public view returns (bool comp) {
        if (tableExists(tabname)) {
            return Tables[tabname].comp;
        }
        return false;
    }

    function isRecCompressed(
        string memory tabname,
        uint rowid
    ) public view returns (bool comp) {
        if (idExists(tabname, rowid)) {
            return Tables[tabname].Records[rowid].comp;
        }
        return false;
    }

    function select(
        string memory tabname,
        uint rowid
    ) public view returns (bytes memory data, string memory signature) {
        if (idExists(tabname, rowid)) {
            return (Tables[tabname].Records[rowid].data, Tables[tabname].Records[rowid].signature);
        }
    }

    function tableExists(
        string memory tabname
    ) public view returns (bool success) {
        if (tableNames.length == 0) {
            return false;
        }
        else {
            uint existIndex = Tables[tabname].index;
            if (existIndex <= (tableNames.length - 1)) {
                bytes memory tnb = bytes(tabname);
                bytes memory tneixb = bytes(tableNames[existIndex]);
                if (keccak256(tnb) != keccak256(tneixb)) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    function getVersion() public pure returns (string memory version) {
        return "0.6.19";
    }

    // contract private pure functions
    function strToB32(
        string memory source
    ) private pure returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }
}
