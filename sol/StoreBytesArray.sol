// SPDX-License-Identifier: Artistic-2.0
pragma solidity >=0.4.0 <0.9.0;

contract StoreBytesArray {
    struct Record {
        uint index;
        bytes data;
    }
    uint[] indexes;
    mapping(uint => Record) Records;

    /*
     * Remix arguments:
     * 0x - empty
     * 0x313433303137343139395f747874 - non empty
     */
    function pushRecord(bytes memory data) public returns (bool success) {
        uint index = indexes.length;
        indexes.push(index);
        Records[index].data = data;
        return true;
    }

    function popRecord() public returns (bool success) {
        if (indexes.length > 0) {
            uint lastIndex = indexes.length - 1;
            delete Records[indexes[lastIndex]];
            indexes.pop();
            return true;
        }
        return false;
    }

    function getRecord(uint index) public view returns (bytes memory data) {
        return Records[index].data;
    }

    function getLength() public view returns (uint length) {
        return indexes.length;
    }
}
