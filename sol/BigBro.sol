// SPDX-License-Identifier: Artistic-2.0
pragma solidity >=0.4.0 <0.9.0;

contract BigBro {
    // contract events
    event BigBroAccess (
        bytes32 indexed method,
        uint8 indexed id
    );

    // contract types
    struct Statistics {
        uint id;
        string referer;
        string ip;
        string useragent;
        string resolution;
        string page;
        string country;
    }

    // contract state variables
    Statistics[] BigBroDB;

    // contract public functions
    function init() public returns (bool success) {
        uint rowid = getMaxId();
        insert((rowid + 1), "goo.gl", "127.0.0.1", "Mozilla", "640*480", "index.html", "RU");
        insert((rowid + 2), "twitter.com", "127.0.0.11", "IE", "1900*1280", "index2.html", "US");
        insert((rowid + 3), "ya.ru", "127.0.0.12", "Opera", "1024*768", "index3.html", "BY");
        insert((rowid + 4), "pheix.org", "127.0.0.111", "Safari", "100*200", "index4.html", "UA");
        insert((rowid + 5), "foo.bar", "127.0.0.254", "Netscape", "1152*778", "index5.html", "RO");
        emit BigBroAccess(strToB32("init"), 0);

        return true;
    }

    function insert(
        uint rowid,
        string memory ref,
        string memory ip,
        string memory ua,
        string memory res,
        string memory pg,
        string memory cntry
    ) public returns (bool success) {
        if (rowid > 0) {
            BigBroDB.push(Statistics(rowid, ref, ip, ua, res, pg, cntry));
            emit BigBroAccess(strToB32("insert"), 1);

            return true;
        }

        return false;
    }

    function removeById(uint rowid) public returns (bool success) {
        if (rowid > 0) {
            uint i = 0;
            for (i = 0; i < BigBroDB.length; i++) {
                if (BigBroDB[i].id == rowid) {
                    BigBroDB[i] = BigBroDB[BigBroDB.length - 1];
                    BigBroDB.pop();
                    emit BigBroAccess(strToB32("removeById"), 2);

                    return true;
               }
           }
        }

        return false;
    }

    function setById(
        uint rowid,
        string memory ref,
        string memory ip,
        string memory ua,
        string memory res,
        string memory pg,
        string memory cntry
    ) public returns (bool success) {
        if (rowid > 0) {
            uint i = 0;
            for (i = 0; i < BigBroDB.length; i++) {
                if (BigBroDB[i].id == rowid) {
                    bytes memory refB = bytes(ref);
                    bytes memory ipB = bytes(ip);
                    bytes memory uaB = bytes(ua);
                    bytes memory resB = bytes(res);
                    bytes memory pgB = bytes(pg);
                    bytes memory cntryB = bytes(cntry);

                    if (refB.length > 0)
                        BigBroDB[i].referer = ref;
                    if (ipB.length > 0)
                        BigBroDB[i].ip = ip;
                    if (uaB.length > 0)
                        BigBroDB[i].useragent = ua;
                    if (resB.length > 0)
                        BigBroDB[i].resolution = res;
                    if (pgB.length > 0)
                        BigBroDB[i].page = pg;
                    if (cntryB.length > 0)
                        BigBroDB[i].country = cntry;

                    emit BigBroAccess(strToB32("setById"), 3);

                    return true;
                }
            }
        }
        return false;
    }

    // contract public view functions
    function getById(
        uint rowid
    ) public view returns (
        string memory ref,
        string memory ip,
        string memory ua,
        string memory res,
        string memory pg,
        string memory cntry
    ) {
        if (rowid > 0) {
            uint i = 0;
            for (i = 0; i < BigBroDB.length; i++) {
                if (BigBroDB[i].id == rowid) {
                    return (
                        BigBroDB[i].referer,
                        BigBroDB[i].ip,
                        BigBroDB[i].useragent,
                        BigBroDB[i].resolution,
                        BigBroDB[i].page,
                        BigBroDB[i].country
                    );
                }
            }
        }
    }

    function getByIndex(
        uint index
    ) public view returns (
        uint rowid,
        string memory ref,
        string memory ip,
        string memory ua,
        string memory res,
        string memory pg,
        string memory cntry
    ) {
        if (index >= 0 && index < BigBroDB.length) {
            if (BigBroDB[index].id > 0) {
                return (
                    BigBroDB[index].id,
                    BigBroDB[index].referer,
                    BigBroDB[index].ip,
                    BigBroDB[index].useragent,
                    BigBroDB[index].resolution,
                    BigBroDB[index].page,
                    BigBroDB[index].country
                );
            }
        }
    }

    function getCount() public view returns (uint count) {
        return BigBroDB.length;
    }

    function getMaxId() public view returns (uint rowid) {
        uint ret = 0;
        uint i = 0;
        for (i = 0; i < BigBroDB.length; i++) {
            if (BigBroDB[i].id > ret) {
                ret = BigBroDB[i].id;
           }
        }

        return ret;
    }

    // contract private pure functions
    function strToB32(string memory source) private pure returns (bytes32 res) {
        bytes memory tempStr = bytes(source);
        if (tempStr.length == 0) {
            return 0x0;
        }
        assembly {
            res := mload(add(source, 32))
        }
    }
}
