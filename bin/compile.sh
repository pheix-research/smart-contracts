#!/bin/sh

CONTRACT=$1
SOLPATH=../sol
OUTPATH=./solcoutput
DEPLOYGAS=4700000
PATH=/bin

if test -z "${CONTRACT}"
then
      echo "Please specify correct contact name: ./compile.sh <contract_name>"
else
    if [ -f ${SOLPATH}/${CONTRACT}.sol ]; then
        ${PATH}/rm -rf ${OUTPATH}/*
        /usr/local/bin/solc --overwrite --abi --bin -o ${OUTPATH} dapp-bin=. ${SOLPATH}/${CONTRACT}.sol
        ABI=`${PATH}/cat ./solcoutput/${CONTRACT}.abi`
        BIN=`${PATH}/cat ./solcoutput/${CONTRACT}.bin`
        echo "var contractFactory=eth.contract(${ABI});" > ${OUTPATH}/${CONTRACT}.js
        echo "var contractBinary=\"0x\" + \"${BIN}\"" >> ${OUTPATH}/${CONTRACT}.js
        echo "var ${CONTRACT} = contractFactory.new( {from:eth.accounts[0],data:contractBinary,gas:${DEPLOYGAS}}, function(e, contract){ if(e) {console.error(e);return;} if(!contract.address) {console.log(\"Contract transaction send: TransactionHash: \" + contract.transactionHash + \" waiting to be mined...\");}else{console.log(\"Contract mined! Address: \" + contract.address);}})" >> ${OUTPATH}/${CONTRACT}.js
    else
        echo "contract ${SOLPATH}/${CONTRACT}.sol not found"
    fi
fi
