# Pheix database smart contracts repository

## Overview

This repository contains smart contracts library for storing [Pheix CMS](https://pheix.org) data on Ethereum blockchain private network (Proof-Of-Authority).

### Smart contract «PheixDatabase» deployment

First, you need to generate smart contract deploy file via helper bash-script `./compile-contract.sh`. Usage:

```bash
./compile-contract.sh <contract_name> [--tgz/--notgz] [<node_password>]
```

Sample command line:

```bash
cd /home/<username>/git/pheix-research/smart-contracts/t
./compile-contract.sh PheixDatabase --notgz
```

If `./compile-contract.sh` finished successfully, you will get deploy file `PheixDatabase.js` at `/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/`.

Next, you should attach console to node of your private network with `geth`:

```bash
geth attach rpc:http://127.0.0.1:8540
```

After logon run `PheixDatabase.js` in geth console. It will wait for 60 seconds for contract mining and return transaction hash if contract is successfully mined:

```javascript
loadScript("/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/PheixDatabase.js");
// "0x6c837d3b350a21ca91a161d11994ad392889a62bef4bfaddd0a65161d172456b"
```

Now smart contract methods (init, insert, select, remove, etc...) are available via `storage` object methods.

### Smart contract «PheixDatabase» manual test

Initialize the sample database:

```javascript
personal.unlockAccount(eth.accounts[0]);
var gs = storage.init.estimateGas({from:eth.accounts[0]});
storage.init.sendTransaction({from:eth.accounts[0], gas:(gs + 100000)});
```

Then interact with initialized database:

```javascript
storage.countTables.call({from:eth.accounts[0]});
storage.countRows.call("table_1", {from:eth.accounts[0]});
storage.select.call("table_1", 1, {from:eth.accounts[0]});
storage.select.call("table_1", 2, {from:eth.accounts[0]});
```

Add a new record to database and check that insertion is done:

```javascript
personal.unlockAccount(eth.accounts[0]);
storage.insert.sendTransaction("table_1", "pheix.org|127.120.127.120|Mozilla FireFox|320*480|feedback.html|BY", 0, false, {from:eth.accounts[0], gas:8000000});
storage.countRows.call("table_1", {from:eth.accounts[0]});
storage.select.call("table_1", 3, {from:eth.accounts[0]});
// "pheix.org|127.120.127.120|Mozilla FireFox|320*480|feedback.html|BY"
```

### Smart contract «PheixDatabase» automated test

1. Automated smart contract heavy test:

```javascript
personal.unlockAccount(eth.accounts[0]);
loadScript("/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/pheix_database_heavy_test.js");
```

2. Automated `init()` and drop table via `remove()` test:

```javascript
personal.unlockAccount(eth.accounts[0]);
loadScript("/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/pheix_database_init_drop.js");
```

3. Automated `insert()`, `select()` and `set()` test:

```javascript
personal.unlockAccount(eth.accounts[0]);
loadScript("/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/pheix_database_insert_select_set.js");
```

4. Automated `remove()` test:

```javascript
personal.unlockAccount(eth.accounts[0]);
loadScript("/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/pheix_database_remove.js");
```

5. Automated `select()` test:

```javascript
personal.unlockAccount(eth.accounts[0]);
loadScript("/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/pheix_database_select_all.js");
```

6. Automated `updateTabCompression()`, `updateRecCompression()`, `setFields()`, `isTabCompressed()`, `isRecCompressed()` and `getFields()` test:

```javascript
personal.unlockAccount(eth.accounts[0]);
loadScript("/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/pheix_database_flds_comp_set_get.js");
```

7. Automated `updateModTimes()`, `getGlobalModTime()`, `getTableModTime()` test:

```javascript
personal.unlockAccount(eth.accounts[0]);
loadScript("/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/pheix_database_mtimes.js");
```

Also this test includes **log filtering**, more details: [wiki](https://gitlab.com/pheix-research/smart-contracts/-/wikis/Filter-blockchain-events), #39

### Smart contract «PheixDatabase» crucial datasets test

1. Initialize datasets:

```javascript
loadScript('/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/txt/datasets.js');
```

2. Set up test dependent variables (`_TAB_S` — initial dataset index, `_TAB_E` — final dataset index, `_CONTRACT_TX` — smart contract deployment address):

```javascript
 _TAB_S=1; _TAB_E=10; _CONTRACT_TX="<smart_contract_deployment_addr>";
```

3. Initialize/retrieve smart contract:

```javascript
loadScript('/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/retrieve_PheixDatabase.js');
```

4. Deploy datasets:

```javascript
loadScript('/home/<username>/git/pheix-research/smart-contracts/t/PheixDatabase/txt/dataset-deploy.js');
```

## License information

This is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
