var Web3 = require('web3');

const web3 = new Web3('https://goerli.prylabs.net');
const abi  = '[{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint8","name":"mcode","type":"uint8"},{"indexed":true,"internalType":"bytes32","name":"table","type":"bytes32"},{"indexed":true,"internalType":"uint256","name":"rowid","type":"uint256"}],"name":"PheixAccess","type":"event"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"}],"name":"countRows","outputs":[{"internalType":"uint256","name":"count","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"countTables","outputs":[{"internalType":"uint256","name":"count","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"}],"name":"dropTable","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"uint256","name":"index","type":"uint256"}],"name":"getDataByIndex","outputs":[{"internalType":"string","name":"data","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"}],"name":"getFields","outputs":[{"internalType":"string","name":"fields","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"uint256","name":"index","type":"uint256"}],"name":"getIdByIndex","outputs":[{"internalType":"int256","name":"rowid","type":"int256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"}],"name":"getMaxId","outputs":[{"internalType":"uint256","name":"rowid","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"index","type":"uint256"}],"name":"getNameByIndex","outputs":[{"internalType":"string","name":"name","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"}],"name":"getTableIndex","outputs":[{"internalType":"int256","name":"index","type":"int256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"uint256","name":"rowid","type":"uint256"}],"name":"idExists","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"init","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"string","name":"rowdata","type":"string"},{"internalType":"uint256","name":"id","type":"uint256"},{"internalType":"bool","name":"comp","type":"bool"}],"name":"insert","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"uint256","name":"rowid","type":"uint256"}],"name":"isRecCompressed","outputs":[{"internalType":"bool","name":"comp","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"}],"name":"isTabCompressed","outputs":[{"internalType":"bool","name":"comp","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"string","name":"fields","type":"string"},{"internalType":"bool","name":"comp","type":"bool"}],"name":"newTable","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"uint256","name":"rowid","type":"uint256"}],"name":"remove","outputs":[{"internalType":"uint256","name":"id","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"uint256","name":"rowid","type":"uint256"}],"name":"select","outputs":[{"internalType":"string","name":"data","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"uint256","name":"rowid","type":"uint256"},{"internalType":"string","name":"rowdata","type":"string"},{"internalType":"bool","name":"comp","type":"bool"}],"name":"set","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"string","name":"fields","type":"string"}],"name":"setFields","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"}],"name":"tableExists","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"uint256","name":"rowid","type":"uint256"},{"internalType":"bool","name":"comp","type":"bool"}],"name":"updateRecCompression","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"tabname","type":"string"},{"internalType":"bool","name":"comp","type":"bool"}],"name":"updateTabCompression","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"}]';

const address = '0x6eeb385b1b088d2eda38053e4d090a6cb35c0737';
const addr = "0x38c927fee135d6a73cb87387707a1537b3684db1";
const keythereum = require("keythereum");

var datadir = "./";
var keyObject = keythereum.importFromFile(address, datadir);

const privateKey = "0x" + keythereum.recover("", keyObject).toString('hex');
console.log(privateKey);

const contract = new web3.eth.Contract(JSON.parse(abi), addr);
//const methdata = contract.methods.newTable('tab01', '#1;2;3', false).encodeABI();
const methdata = contract.methods.init().encodeABI();

const sign = require('ethjs-signer').sign;
const BN = require('bn.js');

web3.eth.getBalance(address, function(err, result) {
    if (err) {
        console.log(err);
    }
    else {
        console.log(web3.utils.fromWei(result, "ether") + " ETH")
    }
});

web3.eth.getTransactionCount(address, function(err, result) {
    if (err) {
        console.log(err)
    }
    else {
        console.log("TX count: " + result)
    }
});

web3.eth.getTransactionCount(address).then((nonce) => {
    web3.eth.sendSignedTransaction(
        sign({
            from: address,
            to: addr,
            gas: web3.utils.toHex('6000000'),
        gasLimit: web3.utils.toHex('6000000'),
        gasPrice: web3.utils.toHex(web3.utils.toWei('1.1', 'gwei')),
            value: web3.utils.toHex('0'),
            data: methdata,
        nonce: nonce,
        }, privateKey)
    ).then((txHash) => {
        console.log('Transaction Hash', txHash);
    });
});
