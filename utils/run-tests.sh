#!/bin/bash

ME=`basename "$0"`
NETPATH=$1
WORKPATH=$2
WORKTEST=$3
SAVEHASHDIR=$4
SCHASHFNAME=smart-contract.hash
TIMEOUT=1
WAITTIMEOUT=120
CONNECTEDPEERS=0
CONTRACTNAME=pheix_database
SMVERSTR=`git log -1 --oneline | perl -lne 'print $1 if /\[ ([0-9]+\.[0-9]+\.[0-9]+) \]/i' | tr '[:upper:]' '[:lower:]'`
PUBLICNET=

if ([ ! -z "$ETHEREUMLOCALNODE" ] && [ $ETHEREUMLOCALNODE == "holesky" -o $ETHEREUMLOCALNODE == "sepolia" ]) || ([ ! -z "$PUBLICTESTNET" ] && [ $PUBLICTESTNET == "holesky" -o $PUBLICTESTNET == "sepolia" ]); then
    if [ ! -z $PUBLICTESTNET ]; then
        PUBLICNET=$PUBLICTESTNET
    else
        PUBLICNET="${ETHEREUMLOCALNODE} LOCAL"
    fi

    echo "***INF[$ME]: skipping due to ${PUBLICNET} is used" && exit 0;
fi

if [ ! -d "$WORKPATH" ] || [ -z "$WORKPATH" ]; then
    WORKPATH=/smart-contracts/t/PheixDatabase
else
    CONTRACTNAME=`basename "$WORKPATH"`
fi

if [ "$WORKTEST" != "SKIP" ]; then
    if [ ! -f "$WORKPATH/$WORKTEST" ] || [ -z "$WORKTEST" ]; then
        WORKTEST=${CONTRACTNAME}_heavy_test.js
    fi
fi

if [ ! -d "$NETPATH" ] || [ -z "$NETPATH" ]; then
    NETPATH=/ethereum-local-network/geth.local
else
    if [ ! -f "$NETPATH/attach.node0" ]; then
        NETPATH=/ethereum-local-network/geth.local
    fi
fi

echo "***INF[$ME]: $SMVERSTR"
echo "***INF[$ME]: local PoA network <$NETPATH>"

if [ "$WORKTEST" != "SKIP" ]; then
    echo "***INF[$ME]: running test <$WORKPATH/$WORKTEST>"
else
    echo "***INF[$ME]: skipping test, just deploying"
fi

echo "***INF[$ME]: waiting $WAITTIMEOUT seconds for active ethereum network"

CLI=`curl -s --data '{"method":"web3_clientVersion","params":[],"id":1,"jsonrpc":"2.0"}' -H "Content-Type: application/json" -X POST localhost:8540 | jq '.result' | sed 's/\"//g' | tr '[:upper:]' '[:lower:]'`

if [[ $CLI =~ "geth" ]]; then
	PEERS=admin_peers
	JQCOMM=".result | length"
else
    echo "***FAILURE[$ME]: invalid Ethereum client $CLI"
    exit 5
fi

IPCPATH="${NETPATH}/geth-node1/geth.ipc"

if [ ! -S "${IPCPATH}" ]; then
    echo "***FAILURE[$ME]: IPC socket ${IPCPATH} is not available"
    exit 1
fi

while [ $CONNECTEDPEERS -eq 0 ] && [ $WAITTIMEOUT -gt 0 ]
do
    DATA=('{"method":"'$PEERS'","params":[],"id":1,"jsonrpc":"2.0"}')

    #CONNECTEDPEERS=`curl -s --data ${DATA[0]} -H "Content-Type: application/json" -X POST localhost:8540 | jq "$JQCOMM"`;
    CONNECTEDPEERS=`echo ${DATA[0]} | nc -q 0 -U ${IPCPATH} | jq -r "$JQCOMM"`

    if [ -z "$CONNECTEDPEERS" ] || [ "$CONNECTEDPEERS" = "null" ]; then
        CONNECTEDPEERS=0;
    else
        if [ $CONNECTEDPEERS -gt 0 ]; then
            echo "***INF[$ME]: network is active - found $CONNECTEDPEERS peer(s)"
        fi
    fi
    WAITTIMEOUT=$[ $WAITTIMEOUT - 1 ]
    sleep $TIMEOUT
done

if [ $CONNECTEDPEERS -eq 0 ]; then
    echo "***FAILURE[$ME]: network is down - found $CONNECTEDPEERS peer(s)"
    exit 4
fi

COMMANDOUT=`bash $NETPATH/attach.node0 <<< "loadScript('$WORKPATH/$CONTRACTNAME.js');console.log(storage.tx);"`
#COMMANDOUT=`nice -n 20 geth attach $IPCPATH <<< "loadScript('$WORKPATH/$CONTRACTNAME.js');console.log(storage.tx);"`

if [ $? -eq 0 ]; then
    SCHASH=`echo $COMMANDOUT | perl -lne 'print $1 if /receipt.transactionHash \= (0x[0-9a-f]{64})/i'`
    if [ ! -z "$SCHASH" ]; then
        echo "***INF[$ME]: smart-contract deploy address <$SCHASH>"
        if [ ! -z "$SAVEHASHDIR" ]; then
            if [ -d "$SAVEHASHDIR" ]; then
                echo $SCHASH > $SAVEHASHDIR/$SCHASHFNAME
                if [ $? -eq 0 ]; then
                    echo "***INF[$ME]: smart-contract address is saved to $SAVEHASHDIR/$SCHASHFNAME"
                else
                    echo "***FAILURE[$ME]: error while saving smart-contract address"
                fi
            fi
        fi
    else
        echo "***FAILURE[$ME]: no smart-contract deploy address found"
        echo "***POSTMORTEM[$ME]:"
        printf "$COMMANDOUT"
        exit 3
    fi

    if [ "$WORKTEST" != "SKIP" ]; then
        GETHOUTPUT=`bash $NETPATH/attach.node0 <<< "_CONTRACT_TX=\"$SCHASH\";loadScript('$WORKPATH/retrieve_${CONTRACTNAME}.js');loadScript('$WORKPATH/$WORKTEST');"`
        COMMANDOUT=`echo ${GETHOUTPUT} | perl -lne 'print "***INF['$ME']: TEST $1!" if $_ =~ /(COMPLETED|FAILED)/'`
        if [ $? -ne 0 ] || [ -z "$COMMANDOUT" ]; then
            echo "***FAILURE[$ME]: run test $WORKTEST"
            exit 2
        else
            echo $COMMANDOUT | sed -e 's/ \*/\n\*/g'

            if [[ $COMMANDOUT =~ "FAILED" ]]; then
                printf "${GETHOUTPUT}"
            fi
        fi
    fi
else
    echo "***FAILURE[$ME]: smart-contract deploy"
    exit 1
fi
