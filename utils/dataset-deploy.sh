#!/bin/bash

COMMANDOUT=
SCHASH=
TABLIMIT=
TABLIMITMSG=ALL
ME=`basename "$0"`
NETPATH=$1
WORKPATH=$2
TABS=$3
TABE=$4
SCHASHPATH=/mnt/smart-contract.hash
CONTRACTNAME=PheixDatabase
TESTFNAME=dataset-deploy.js
DATAFNAME=datasets.js
LZWFNAME=lzw.js
SMVERSTR=`git log -1 --oneline | perl -lne 'print $1 if /\[ ([0-9]+\.[0-9]+\.[0-9]+) \]/i' | tr '[:upper:]' '[:lower:]'`
REGEXPR='^[0-9]+$'

if [ ! -d "$WORKPATH" ] || [ -z "$WORKPATH" ] || [ ! -f "$WORKPATH/txt/$TESTFNAME" ]; then
    WORKPATH=/smart-contracts/t/$CONTRACTNAME
fi

if [ ! -d "$NETPATH" ] || [ -z "$NETPATH" ]; then
    NETPATH=/ethereum-local-network/geth.local
else
    if [ ! -f "$NETPATH/attach.node0" ]; then
        NETPATH=/ethereum-local-network/geth.local
    fi
fi

if [[ $TABS =~ $REGEXPR && $TABE =~ $REGEXPR ]] ; then
   TABLIMIT="_TAB_S=${TABS};_TAB_E=${TABE};"
   TABLIMITMSG="[${TABS},${TABE}]"
fi

echo "***INF[$ME]: $SMVERSTR"
echo "***INF[$ME]: local PoA network <$NETPATH>"
echo "***INF[$ME]: running test <$WORKPATH/txt/$TESTFNAME>"
echo "***INF[$ME]: testing tables: $TABLIMITMSG"

# Checking smart contract hash
if [ -f "$SCHASHPATH" ]; then
    SCHASH=`cat $SCHASHPATH`
    if [ -z "$SCHASH" ]; then
        echo "***FAILURE[$ME]: smart contract hash is blank"

        exit 1
    else
        echo "***INF[$ME]: smart contract hash <$SCHASH>"

        COMMANDOUT=`bash $NETPATH/attach.node0 <<< "_CONTRACT_TX=\"$SCHASH\";loadScript('$WORKPATH/retrieve_${CONTRACTNAME}.js');${TABLIMIT}loadScript('$WORKPATH/txt/$DATAFNAME');loadScript('$WORKPATH/txt/$LZWFNAME');loadScript('$WORKPATH/txt/$TESTFNAME');"`

        if [ $? -ne 0 ]; then
            echo "***FAILURE[$ME]: bash error while running test $WORKTEST"

            exit 2
        else
            RES=`printf "${COMMANDOUT//%/%%}" | grep "COMPLETED\|FAILED" | perl -lne 'print "***INF['$ME']: $_"'`

            if [ -z "$RES" ]; then
                echo "***FAILURE[$ME]: something wrong with test $WORKTEST"
                echo "***POSTMORTEM[$ME]:"
                printf "${COMMANDOUT//%/%%}"

                exit 4
            else
                echo $RES | sed -e 's/ \*/\n\*/g'
                printf "${COMMANDOUT//%/%%}" | sed -n '/=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=/,$p' | sed '/undefined/d'

                printf "${COMMANDOUT//%/%%}" > "geth_testing_$(date +"%m_%d_%Y_%T").log"
            fi
        fi
    fi
else
    echo "***INF[$ME]: no smart contract hash found"
    exit 3
fi
